# <h1 align="center">《庄小焱博客》</h1>

<img src="https://komarev.com/ghpvc/?username=Zhuang-XiaoYan&label=Visits" alt="artdong" />

<h3>我是庄小焱。任职于世界100强软件研发中心，PMP项目管理专家、系统架构设计师(高级)、CSDN博文专家.博主在系统架构、容器化技术、大数据、机器学习领域不断学习,同时我将在博客中持续不断分享自己的学习知识和相关技术解决方案,欢迎大家和我交流学习,欢迎大家关注我的博客.</h3>

#  一、庄小焱简历


# 二、庄小焱博客


# 三、实战项目

* https://javaguide.cn/system-design/security/design-of-authority-system.html#rbac-%E6%A8%A1%E5%9E%8B
* [JVM线上问题排查和性能调优案例](https://javaguide.cn/java/jvm/jvm-in-action.html)
* [八股文](https://github.com/h2pl/JavaTutorial)


# 五、面试项目
https://juejin.cn/post/6844904125935665160


# 博文资源

- [庄小焱网站](https://2462612540.github.io/zhuangxiaoyan/)
- [庄小焱github](https://github.com/2462612540)
- [庄小焱gitee](https://gitee.com/xjl2462612540)
- [庄小焱CSDN](https://blog.csdn.net/weixin_41605937?type=blog)

# 联系方式

> 如果你有什么问题想与我沟通交流的话你可以使用邮件或者微信与我联系.
> 我的邮箱是：18279148786@163.com,我的微信号:Mike-Scofield-Love


# 博文参考
- [面试总结视频](https://www.bilibili.com/video/BV1684y1c71W/?spm_id_from=333.337.search-card.all.click&vd_source=1032b663a8d7733cb9dbbbf8a2188d94)
- [八股文总结](https://segmentfault.com/u/magebyte/articles)
- [java全栈博文](https://pdai.tech/md/outline/x-outline.html#java%E8%BF%9B%E9%98%B6-%E5%B9%B6%E5%8F%91%E6%A1%86%E6%9E%B6)
- [虚拟化博客](https://cshihong.github.io/categories/)
- [代码随想录](https://www.programmercarl.com/other/kstar_baguwen.html#%E4%B8%8B%E8%BD%BD%E6%96%B9%E5%BC%8F)
- [javaguide](https://javaguide.cn/system-design/security/jwt-intro.html)
- [跟着Mic学架构](https://segmentfault.com/a/1190000041917963)
- [程序员八股文面试总结](https://github.com/hollischuang)
- https://github.com/zhoubichuan
- https://juejin.cn/post/7109286956076695560
- https://zhoubichuan.github.io/web-vue/base/engine/1.index.html
- https://www.vuepress.cn/config/#palette-styl
- https://blog.csdn.net/xiaoxianer321/article/details/119548202
- [前端博文](https://lq782655835.github.io/blogs/team-standard/0.standard-ai-summary.html)
