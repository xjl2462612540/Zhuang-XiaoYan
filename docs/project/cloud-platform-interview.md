---
lang: zh-CN
sidebarDepth: 2
---

# 三、Cloud-Platform面试总结

## 云计算系统的访问量是多少？

Cloud-Platform包括了的提供测试系统环境与物理与虚拟资源，

1. 集成测试需要VM，一天6次，6*500=3千次.
2. 手动测试需要的VM：一天是500千次.
3. 部署VM的申请量是：500次.
4. cluster的申请量是500次.

**一天的访问量是4千次左右。**
**系统的QPS每一秒测试请求数量:200QPS**

1. 白天的请求数据访问高一点，大概是在100次访问次数。

## Cloud-Platform的背景

在没有这个系统和平台之前是需要用自己去构建相关所需的环境，处置之外还需要配置网络，CPU,存储等资源。
为了节约物理资源的使用，同时为了最大限度的利用物理资源，采用虚拟化技术来构建相关开发、测试、生产环境。
系统提供企业所需要的各种基础设施，帮助用户能够快速获取用户所需VM环境,各种虚拟化产品。同时提供用户对环境的统一管理。

## 什么是虚拟化？

虚拟化技术是云计算的根基，在计算机技术中，虚拟技术是一种资源管理技术，是将计算机的各种实体资源（CPU、内存、磁盘空间、网络适配器等），
予以抽象、转换后呈现出来并可供分割、组合为一个或多个电脑配置环境。由此，打破实体结构间的不可切割的障碍，
使用户可以比原本的配置更好的方式来应用这些电脑硬件资源。这些资源的新虚拟部分是不受现有资源的架设方式，地域或物理配置所限制。
一般所指的虚拟化资源包括计算(CPU+内存），网络，存储。

![img.png](../.vuepress/public/project/cloud/vmware001.png)

![img.png](../.vuepress/public/project/cloud/vmware002.png)

![img.png](../.vuepress/public/project/cloud/vmware028.png)

### CPU资源管理

在Linux虚拟化环境中，可以使用CPU调度器来管理CPU资源的分配和使用情况。
通过设置CPU限制（如CPU份额、CPU周期等），可以控制每个虚拟机可以使用的CPU资源量。
此外，还可以通过CPU亲和性设置和CPU热插拔等技术，实现对CPU资源的动态管理和调整。

### 内存资源管理

在Linux虚拟化环境中，可以使用内存管理器来管理内存资源的分配和使用情况。
通过设置内存限制（如内存配额、内存保证等），可以控制每个虚拟机可以使用的内存资源量。
此外，还可以通过内存回收和内存压缩等技术，实现对内存资源的有效管理和回收。

### 存储资源管理：

在Linux虚拟化环境中，可以使用存储管理器来管理存储资源的分配和使用情况。
通过设置存储配额、存储保证和存储限制等参数，可以控制每个虚拟机可以使用的存储资源量。
此外，还可以通过存储共享和存储快照等技术，实现对存储资源的共享和管理。

### 网络资源管理

在Linux虚拟化环境中，可以使用网络管理器来管理网络资源的分配和使用情况。
通过设置带宽限制、流量控制和访问控制等策略，可以控制每个虚拟机可以使用的网络资源量。
此外，还可以通过虚拟防火墙和虚拟路由器等技术，实现对网络流量的监控和管理。

## 怎么实现资源的隔离？

![img.png](../.vuepress/public/project/cloud/vmware003.png)

![img.png](../.vuepress/public/project/cloud/vmware004.png)

在一个操作系统中运行多个文件，在每一个对应的操作系统运行所需要的数据都保存在对应的文件中，这样来模拟一个VM的运行。

![img.png](../.vuepress/public/project/cloud/vmware005.png)

### CPU资源隔离

在Linux虚拟化环境中，可以通过**CPU调度器（如CFS）实现对CPU资源的隔离和分配**。
为**每个虚拟机配置CPU时间片，并通过调度算法来决定虚拟机之间的CPU资源分配比例**。
这样可以确保每个虚拟机获得公平的CPU时间，并防止某个虚拟机占用过多的CPU资源。

### 内存资源隔离

Linux内核提供了多种内存管理技术，如**内存分页、内存块分配等，这些技术可以用于实现对内存资源的隔离和管理**。
在虚拟化环境中，可以为每个虚拟机配置独立的内存空间，并通过内存分页和交换等技术进行内存管理。
这样可以确保每个虚拟机获得足够的内存，并防止某个虚拟机占用过多的内存资源。

### 存储资源隔离

在Linux虚拟化环境中，可以使用虚拟磁盘镜像或直接访问物理存储设备的方式为每个虚拟机提供独立的存储空间。
通过存储虚拟化技术（如LVM、RAID等），可以将**物理存储资源划分为多个逻辑卷，并为每个虚拟机分配独立的逻辑卷。
这样可以确保每个虚拟机获得独立的存储空间，并防止某个虚拟机占用过多的存储资源**。

### 网络资源隔离

在Linux虚拟化环境中，可以通过**虚拟网络设备和虚拟交换机等技术实现对网络资源的隔离和管理**。
**为每个虚拟机分配独立的IP地址、MAC地址和端口号，并在物理网络设备上配置虚拟交换机或虚拟网桥，
以实现虚拟机之间和虚拟机与物理主机之间的通信隔离**。这样可以确保每个虚拟机获得独立的网络资源，
并防止网络流量混杂和干扰。

## CPU虚拟化技术的原理

### CPU虚拟化是什么

**CPU资源池中的资源是什么:** **算力(用主频表达)**

**什么是主频?**： CPU的主频代表CPU的一个核每秒计算的次数（如:2.9GHz主频的CPU，可以每秒计算2.9G次，即29亿次)

1. **一颗CPU的算力是多少(未开启超线程)**？ CPU核心数*主频=整个CPU的算力
2. **一颗CPU的算力是多少(开启超线程)**？ CPU核心数*2*主频=整个CPU的算力(超线程是将一颗物理CPU通过时分复用的方式变为2个逻辑CPU，操作系统识别到的就是逻辑CPU)
3. **一台服务器的算力是多少**? CPU个数*CPU核心数*2*主频=整个服务器的算力
4. **一个服务器集群的算力是多少**? :服务器1+服务器2的算力=整个集群的算力

![img.png](../.vuepress/public/project/cloud/vmware006.png)

### VM申请VCPU资源方式

1. Vm通过VCPU划分算力。
2. **时分复用实现原理:** 当一台服务器上运行的所有VM的vCPU数量超过线程数时，将进入超分配状态，**超分模式下vCPU通过时分复用的方式**，**按照时间分片轮流使用线程进行计算**

![img.png](../.vuepress/public/project/cloud/vmware007.png)

**超分配方案:VM通过时分复用划分资源**

**vCPU的算力有可能降低**

一个完整的线程如果主频是2GHz，1秒钟可以计算20亿次。当因为超分配有了时间分片轮循时，比如他只能使用0.5s，
对这个VM来说相当于1秒钟可以计算10亿次，相当于VM的算力从2GHz变成了1GHz

**超分情况下vCPU算力(是一个范围值)∶**

1. 口最低算力:总算力/vCPU数(所有VM都繁忙时)
2. 最高算力: vCPU*主频

示例:如下图所示：
1. VM1的最低算力=8/6*2=2.67GHz;
2. 最高算力=2*2GHz=4GHz

**超分配的问题:性能不可预期**

1. 当大多数VM都比较空间时，性能体验与没有超分区别不大。
2. 当大多数VM都负载非常高时，CPU资源争抢严重，CPU处理不过来导致VM性能下降。
3. 如果这里面有1个VM非常重要，他的性能也无法保障。

为了让部分VM在超分时依然有可预期的性能，vsphere提供了CPU的资源控制方案

![img.png](../.vuepress/public/project/cloud/vmware008.png)

**CPU的资源控制方案:**

![img.png](../.vuepress/public/project/cloud/vmware009.png)

**VM的vCPU是不是越多越好?**

![img.png](../.vuepress/public/project/cloud/vmware010.png)

### VCPU用完如何回收资源?

![img.png](../.vuepress/public/project/cloud/vmware011.png)

## 内存虚拟化技术原理

### 内存虚拟化

内存资源池中的资源是什么?内存空间。内存池:内存池的总资源相当于是集群内所有服务器内存资源之和

![img.png](../.vuepress/public/project/cloud/vmware012.png)

### VM申请内存资源方式

1. 按需分配:一个设置为4G内存的VM开机后不会立刻分配完整的4G内存而是按需分配(如左图，VM需要时才会分配内存给VM使用)
2. 内存也可以超分配:∶服务器上运行的所有VM设置的内存总和可大于服务器的物理内存(如: 3个4G内存的VM可以同时在8G内存的服务器上运行)

![img.png](../.vuepress/public/project/cloud/vmware013.png)

### VM释放内存资源

根据服务器剩余的内存执行内存回收策略:
1. 当服务器可用内存≥6%，VMM默认仅使用“TPS”回收内存。
2. 当服务器可用内存≥4%，使用“TPS”+“气球”回收内存。
3. 当服务器可用内存≥2%，使用"TPS"+‘气球’÷“压缩”＋“交换灾件”加速内存回败收。
4. 当服务器可用内存≥1%，使用“TPS”+"气球'＋“压缩”＋*交换文中”加速内存回收，并禁止所有WMN申清更多内存。

![img.png](../.vuepress/public/project/cloud/vmware014.png)

## 存储虚拟化技术原理？

### 虚拟存储资源

存储资源池中的资源是什么?①存储空间②IO性能。

概述:VM所用的存储设备的存储空间，常见的存储类型:**①本地存储②共享存储**（FC SAN、iSCSI、NAS)③分布式存储（vSAN)

![img.png](../.vuepress/public/project/cloud/vmware015.png)

### 申请虚拟存储方式

![img.png](../.vuepress/public/project/cloud/vmware016.png)

**先到先得**

1. 选择对的存储来存VM:VM可以选择集群内的任意一个文件系统为VMFS的存储设备来存VM的文件，存储设备为每个VM在本地生成一个后缀名为vmdk的文件作为虚拟磁盘来存VM的数据(**先到先得**)
2. VMFS的文件系统:想要作为VM的存储设备必须要使用VMFS这种文件系统来格式化磁盘
3. vmdk的虚拟磁盘:当VM想要使用某个存储设备时，给存储设备会在本地划分出一个虚拟的空间给VM用来存储VM产生的数据，这个虚拟的空间会以后缀名为vmdk的文件的形式呈现出来

**一个VM占用多少空间?**

1. 精简置备:按需占用空间，节省存储空间（比如VM设置的硬盘是100G，实际用了10G，那实际占用的物理存储的10G)
   * 优点:极致的空间节省，多个VM可以复用存储资源
   * 缺点:①用时再申请空间，体验最差②空间被写爆(VM显示有空间，物理存储没了)

2. 厚置备延迟置零:预留全部硬盘空间(比如VM设置的硬盘是100G，创建VM时直接从硬盘上划分出100G该VM独占，不管实际有没有用这么多)
   * 优点:不会存储空间被写爆的情况
   * 缺点:①空间占用大②性能较差（不用每次申请空间但每次都是“改写”数据)

3. 厚置备快速置零:预留全部硬盘空间且置零这个空间内所有的数据（空间划分同“厚置备延迟置零”，除此之外有个情况数据的操作)
   * 优点:①不会存储空间被写爆的情况②性能最好（每次都是“全新写”)
   * 缺点:①空间占用大②首次创建速度较慢(因为需要置零)

![img.png](../.vuepress/public/project/cloud/vmware017.png)

### 回收虚拟存储资源

**VM中删除某个文件，存储空间如何回收?**

1. 问题说明:VM中删除一个文件(比如:106)，虽然操作系统显示空间被释放了，实际该空间无法被VMM回收(其他VM无法使用)，看到的效果就是VM只能越来越大，不会变小。
2. 解决方案:通过某种机制来主动回收空间(类似内存气球驱动的方案)，把VM中被删除的空间全部“置零”，VMM周期性回收“置零”的空间。
3. 补充说明:空间回收仅对精简置备有效，厚置备无需回收。

![img.png](../.vuepress/public/project/cloud/vmware018.png)

## 网络虚拟化技术原理？

### 网络虚拟化资源

**网络资源池中的资源是什么?网络带宽**
1. 物理网卡的带宽就是资源:集群内的所有物理网卡的带宽之和形成网络资源池，所有VM共用网络资源池中的所有带宽资源.
2. 不只是VM需要网络资源:需要使用网络资源的除了VM还有系统数据传输时的流量〈包括: vMotion流量、存储流量、FT流量、vSAN流量等)
3. 不需要资源的情况:一台服务器内部的VM相互访问，无需使用网络资源池的资源（因为不经过网卡)

![img.png](../.vuepress/public/project/cloud/vmware019.png)

**问题1:VM如何使用物理网卡访问外部网络?**

1. 方案1:服务器上每个物理网卡分给一个VM用（比如:3个VM用3张网卡，每人一个)
2. 方案2(实际选择的方案)︰让多个VM连接一个“虚拟交换机”上，从而连入物理网络

**问题2:一个服务器里面怎么就可以做出来“交换机”?**

1. 本质上就是文件复制:每个VM本质上就是文件模拟的，VM之间的数据传输本质上就是将数据从一个“文件”复制到另一个“文件; 
**VM和外部物理网络的访问本质上就是把文件中的部分数据根据VM的网络配置封装成一个数据包通过服务器的物理网卡传输出去**
2. 从而实现类似“交换机的效果”

![img.png](../.vuepress/public/project/cloud/vmware020.png)

![img.png](../.vuepress/public/project/cloud/vmware021.png)

![img.png](../.vuepress/public/project/cloud/vmware022.png)

![img.png](../.vuepress/public/project/cloud/vmware023.png)

关于VMkernel

VMkernel有什么用?**虚拟交换机上的虚拟接口，需要配置IP地址，用于ESXI之间的系统流量传输**
具体可以跑哪些流量?**管理流量、Motion流量、FT流量、vSAN流量、置备流量(VM冷迁移、克隆、快照)**
等补充说明:VMkernel对应服务器如果没有激活，对应的功能就无法使用(比如:没有激活vMotion该功能将无法使用)

### 如何使用虚拟网络资源

1. 基于VDS划分资源池:网络资源是基于VDS设计的，即VDS的上行链路连接的物理端口的带宽之和就是总带宽（而不是所有服务器物理接口的总带宽)
2. 需要使用网络资源的流量包括: ①VM流量②系统流量(管理流量、vMotion流量、FT流量.iSCSI流量、vSAN流量等)
3. 为每种流量设置资源抢占能力:基于份额、预留、限制来定义不同流量的资源抢占能力。

![img.png](../.vuepress/public/project/cloud/vmware024.png)

![img.png](../.vuepress/public/project/cloud/vmware025.png)

![img.png](../.vuepress/public/project/cloud/vmware026.png)

### 回收虚拟网络资源
1. 回收的目的:是暂时不用的资源可以被其他VM使用，实现物理资源的最大化利用
2. 网络资源无需回收:与CPU类似，只要该VM发送的流量不发了，就相当于网络资源被释放了，此时其他VM就可以使用了

![img.png](../.vuepress/public/project/cloud/vmware027.png)

## 怎么构建Vxrail虚拟化产品?

![img.png](../.vuepress/public/project/cloud/Vxrail01.png)

![img.png](../.vuepress/public/project/cloud/Vxrail02.png)

## Clone原理与实现？

虚拟机克隆是指完整的将虚拟机再复制一份。

**克隆产生的与原虚拟机相同的部分：1、虚拟机的配置信息.2、vmdk 文件的信息。**
**克隆产生的与原虚拟机不同的部分：1、虚拟机文件的文件名。2、网络适配器的MAC地址。**

克隆时要求虚拟机处于关机状态，无法为处于开启或挂起状态的虚拟机或快照创建克隆，
因为克隆时需要复制 vmdk 文件，而虚拟机处于挂起或者开启状态时 vmdk 文件是锁定的，
无法进行复制。此外，当虚拟机处于开机状态时，可以完成对快照的克隆，但依然要求，
此快照必须是关机状态下制作的快照。

克隆的方式：
1. **链接克隆**: 克隆是制作一个新的虚拟机，克隆出来的虚拟机 vmdk 文件、配置文件等文件中占内存空间最大的即 vmdk 文件，
此时选择链接克隆，直接调用现有虚拟机的 vmdk 文件而不复制，只是创建一个配置文件，
将自己的虚拟磁盘指向原本的 vmdk，这样就只需要拷贝其他的占内存空间小的文件，提高速度，节省磁盘空间。

2. **完整克隆**: 把虚拟机整个完整的复制一份，消耗时间长、占用内存空间大。

## 利用clone实现产品快速构建

### 测试环境构建

![img.png](../.vuepress/public/project/cloud/clone-test-env.png)

### 用户产品使用
![img.png](../.vuepress/public/project/cloud/user-cluster.png)

## Vmotion的原理与实现

VMware vMotion允许虚拟机在不停机的情况下更改其运行位置。例如，我可以将虚拟机从一个ESXi主机移动到另一个ESXi主机，
而正在使用此虚拟机的用户并不会感觉到这个迁移的发生。通过vMotion可以实现零停机时间和连续可用的服务，并能全面保证业务的完整性和连续性。

Vmotion操作需要在不同ESXi主机之间传输虚拟机状态信息。如下图，一个可以运行vMotion的环境需要包括**共享存储、共享网络和
用于转移虚拟机状态的网络**。这三个要素是vMotion运行必须具备的先决条件。

1. **虚拟机的状态信息是指内存中的内容(虚拟机正在做什么)，** 以及关于虚拟机的一些唯一标识信息。
2. 这些信息包括**BIOS、设备、CPU、以太网卡的MAC地址、芯片组状态、寄存器**等等。

![img.png](../.vuepress/public/project/cloud/Vmotion01.png)

## Vmware HA的原理与实现

为集群中的VM提供高可用，群集中的主机均会受到监控，如果发生故障，故障主机上的虚拟机将在备用主机上重新启动。
vSphere HA会持续监控资源池中的所有物理服务器，并重启受服务器故障影响的虚拟机。

1. 监控和检测虚拟机的“客户操作系统”故障，并在用户指定的时间间隔后自动启动虚拟机。
2. 使用服务器上的“心跳信号”来自动检测服务器故障。
3. 无需人工干预立即在同一资源池中的其它物理主机上重启虚拟机。
4. 与DRS配合使用，选择要在其上重启虚拟机的资源池中的最佳物理主机。

![img.png](../.vuepress/public/project/cloud/HA01.png)

**HA不间断地监控集群中所有ESXi服务器，并检测故障。放置在每台主机上的代理程序不断向集群中的其它主机发出“心跳信号”。
如果“心跳信号”终止,将触发所有受影响的虚拟机在其它主机上的重启过程。**

## Vmware DRS原理

VMware DRS代表分布式资源调度程序∶**是VMware vSphere集群的一部分，它查看集群中的主机，并平衡它们之间的CPU和内存利用率**。
如果一台主机超载，它将分散工作负载，以确保性能不受影响。这是对VMware DRS功能的一个非常简单的解释。
VMware DRS跨聚合到逻辑资源池中的硬件资源集合来动态的分配和平衡计算容量，DRS会不间断的监控资源利用率，
并根据需要和不断变化的优先级的预定义规则，在多台虚拟机之间智能地分配可用资源，当虚拟机负载增大时，
DRS会通过在资源池中的物理主机之间重新分发虚拟机来自动分配额外的资源。

![img.png](../.vuepress/public/project/cloud/DRS01.png)

VMware DRS的前提条件:与VMware vMotion一样，VMware DRS也有一些要求，因为它是vSphere集群的一部分，所以必须有一个有效的ESXi集群才能打开DRS。
其中一些要求是大家熟悉的，例如:
1. 共享存储
2. ESXi主机上兼容的处理器
3. vMotion已经启用(Vmotion不依赖DRS,但是DKRS依赖vMotion)

## Vmware FT原理与实现

主虚拟机位于一台主机之上，备虚拟机位于另一台主机之上。VMware vLockstep技术确保虚拟机处于同步状态。
如果主虚拟机发生故障，那么备虚拟机将会实时接管业务。用户不会感觉到中断或者连接丢失。

1. 在不同的主机上同步运行相同的虚拟机。
2. 出现硬件故障时，所有虚拟机均可实现零宕机时间、零数据损失故障切换。
3. 零宕机时间、零数据损失无需复杂的集群或专用硬件。
4. 所有应用程序和操作系统通用的单—机制。

## 基于本地消息表分布式事务原理是实现

本地消息表，作为可靠消息最终一致性的一种典型实现方案。最初由eBay提出，其亦是对BASE理论的体现、实践。其基本原理、思路很简单。
这里以订单服务、库存服务为例展开说明。当客户下单后，需要先通过订单服务在订单表中插入一条订单记录，再通过库存服务实现对库存表中库存记录的扣减。
但这里即会存在一个问题，由于订单表、库存表分别位于订单服务、库存服务的数据库。传统的本地事务显然无法解决这种跨服务、跨数据库的场景。
而基于本地消息表的分布式事务方案则可以在对业务改动尽可能小的前提下保障数据的最终一致性

**具体地，在事务发起方即这里订单服务的数据库中再增加一张本地消息表。向订单表中插入订单记录的同时，
在本地消息表中也插入一条表示订单创建成功的记录。由于此时订单表、本地消息表位于同一数据库当中，
可以直接通过一个本地事务来保证对这两张表操作的原子性。**

与此同时，**在订单服务中添加一个定时任务，不停轮询、处理本地消息表**。具体地，**将消息表中未被成功处理的记录通过MQ投递至库存服务**。
而库存服务在从MQ中接收到订单创建成功的消息后，对库存表进行库存扣减操作。在库存服务完成扣减后，
通过某种方式告诉订单服务该条消息已经被成功消费、处理。这样订单服务即可将本地消息表中相关记录标记为成功处理的状态，
以避免定时任务重复投递。这里库存服务确认消息消费成功的实现方式，**可以直接通过MQ的Ack消息确认机制实现**，
**也可以让库存服务再向订单服务发送一个处理完毕的消息来完成**。整个方案的示意图如下所示

![img_2.png](../.vuepress/public/project/cloud/cloud01.png)

可以看到，基于本地消息表的可靠消息最终一致性方案非常简单。但在具体业务实践过程还是有一些需要的地方：
1. **库存服务的库存扣减需要保证幂等性**：一方面由于MQ存在自动重试机制，另一方面，当订单服务未收到库存服务对本次消息的消费确认时，则可能会导致定时任务下一次继续投递该消息至库存服务
2. 根据实际业务需要，**本地消息表中记录还应该设置一个合理的最大处理等待时间**，**以及时发现长时间无法得到有效处理的本地消息记录**。

**优点：**
数据最终一致性。
在业务应用中实现了消息的可靠性，减少了对消息中间件的依赖。
**缺点：**
1. 绑定了具体的业务场景，耦合性太高，不可公用和扩展。
2. 消息数据与业务数据在同一数据库，
3. 占用了业务系统的扩展。消息数据可能会受到数据库并发性的影响

## 基于RocketMQ的分布式事务原理是实现

通过基于本地消息表的可靠消息最终一致性方案可以看出，其本质上是通过引入本地消息表来保证本地事务与发送消息的原子性。
那如果说MQ本身能够直接保证消息发送与本地事务的原子性岂不是更方便了,为此在RocketMQ中提供了所谓的事务消息。
这里我们来介绍下其基本机制，流程图如下所示：

![img.png](../.vuepress/public/project/cloud/cloud02.png)

![img.png](../.vuepress/public/project/cloud/cloud03.png)

![img.png](../.vuepress/public/project/cloud/cloud04.png)

事务发起方首先会将事务消息发送到RocketMQ当中，**但此时该条消息并不会对消费者可见，即所谓的半消息**。
当RocketMQ确定消息已经发送成功后，事务发起方即会开始执行本地事务。同时根据本地事务的执行结果，告知给RocketMQ相应的状态信息——commit、rollback。
具体地，**当RocketMQ得到commit状态，则会将之前的事务消息转为对消费者可见、并开始投递**；当RocketMQ得到rollback状态，则会相应的删除之前的事务消息，
保证了本地事务回滚的同时消息也不会投递到消费者侧，保障了二者的原子性。**进一步地，如果RocketMQ未收到本地事务的执行状态时，**
**则会通过事务回查机制定时检查本地事务的状态。**

**优点：** 
1. 数据最终一致性。
2. 消息数据能够独立存储，与具体的业务数据库解耦。
3. 消息的并发性和吞吐量优于本地消息表方案。

**缺点：**
1. 发送一次消息需要完成两次网络交互：1.消息的发送 ; 2. 消息的提交或回滚。
2. 需要实现消息的回查接口，增加了开发成本。

## 最终一致性分布式事务注意的问题

1. 事务发送方本地事务与消息发送的原子性问题：
原因：执行本地事务和发送消息，要么都成功，要么都失败。
解决方案：通过消息确认服务本地事务执行成功。

2. 事务参与方接收消息的可靠性问题：
原因：由于服务器宕机、服务崩溃或网络异常等原因，导致事务参与方不能正常接收消息; 或者接收消息后处理事务的过程中发生异常，无法将结果正确回传到消息库中。
解决方案：通过消息恢复服务保证事务参与方的可靠性。

3. 事务参与方接收消息的幂等性问题：
原因：可靠消息服务可能会多次向事务参与方发送消息
解决方案：**需要具有幂等性，只要参数相同，无论调用多少次接口或方法，结果都相同**。

**库存服务锁定成功后发给消息队列消息（当前库存工作单），过段时间自动解锁，解锁时先查询订单的支付状态。解锁成功修改库存工作单详情项状态为已解锁。**

## redis分布式锁解决资源超分配

多个用户同时申请VM虚拟环境来，在大量请求下可能造成VM的分配了超过一个用户，导致用户环境错误。

![img.png](../.vuepress/public/interview/others/redis-lock.png)

![img.png](../.vuepress/public/interview/others/redis-lock02.png)

![img.png](../.vuepress/public/interview/others/zk-lock.png)

## 系统资源分配最大化是怎么实现？



## 电商产品上架下架业务

![img_2.png](../.vuepress/public/project/cloud/上架流程.png)

![img.png](../.vuepress/public/project/cloud/下架流程.png)

## 项目中主要的工作，主要的亮点？

云计算产品(vxrail、raven产品)，涉及到池化技术和逐级缓存技术。
用户申请资源(背包问题的算法)、涉及订单服务、库存服务、支付服务。
产品的更新服务、软件的更新任务、固件的升级任务、驱动升级。
产品的监控服务、cpu、内存、网络、存储等（数据采集，每分钟采集，每一个小时采集，每一天采集一次）。
产品的删除服务、资源的关闭。

## 系统资源配置管理

vCenter Server是一个64位Windows应用程序，极大程度地提高了可扩展性。 一个vCenter Server实例可以管理多达 1,000 台主机和10,000个运行的虚拟机，
并且在链接模式下，您可以跨10个vCenter Server 实例管理多达30,000个虚拟机。 VMwareHA和VMwareDRS集群可以支持多达32台主机和3,000个虚拟机。

公司一共有的500个集群，1一个集群是1000物理台机器，为了保证集群的安全性质，我们需要对资源限制为总资源的85%，有监控服务对每一个集群进行更新。

## JDK1.8的Full GC实战过程

![img_6.png](../.vuepress/public/interview/jvm/JVM-GC.png)

## 能否对JVM调优，让其几乎不发生Full GC

与普遍看法相反，Minor GC 确实会触发 stop-the-world暂停，暂停应用程序线程。对于大多数应用程序，如果Eden中的大多数对象都可以被视为垃圾并且永远不会复制到Survivor/Old 空间，那么暂停的长度在延迟方面可以忽略不计。

调整内存的大小比例（让生命周期比较短的对象尽可能在min GC的时候GC，避免子啊Full GC的时候来GC生命周期比较短的对象）

## 项目中加了内存缓存，防止穿透到redis的missCache，导致大量的GC解决方案

**最近项目有两个问题**

1. 加了内存缓存，防止穿透到redis的missCache，导致大量的GC。
2. 项目在每次发布的时候GC时间很长达到2s，导致大量的超时。

就针对这两个问题进行了分析和优化。

**问题分析**

首先我们系统是内存32G，使用了大量的内存做为内存缓存数据。结合业务和GC日志以及业务日志得出：

1. 第一个启动的问题和第二个问题是一致的，都是因为内存缓存数据，导致每一次新来一个请求在redis查到数据后存入内存中去，导致存活对象在新生代不停的复制导致超时。
2. 再者我们使用的是CMS垃回收器，新生代使用的是复制清除的垃圾回收机制，通过查看GC日志，每次存活的对象太多，以致于复制数据量很大。导致GC耗时和停顿时间较长，大概在200-300ms，又因为我们对外的借口客户端超时时间时200ms这就导致，我们服务不断出现超时请求。
3. 继续从垃圾回收器的日志中得出垃圾回收的频率也高，大概13秒每次。

**问题本质 ：每次新生代存活对象太多，**

**解决方案**

1. 减小新生代的大小，让每次复制的对象小一点，但是会引发另一个问题GC的频率会提高，虽然停顿时间短了但是停顿的频率会飙高。
2. 调整进入老年代的年龄，默认的15次，调整为6-7次。让提前进入老年代。
3. 更换新的垃圾回收器，使用G1
4. 优化业务逻辑，调整内存缓存key的时间

这四个方案是有先后顺序的，这些方案提出顺序也意味着我当时在执行实验的顺序，可以看出这是一个糟糕的方案顺序，不记得谁说的了应该是鲁迅吧：“调参JVM是迫不得已的选择！”。

G1在jdk6的时候是已经出现了，JDK7u9 或更高版本可以使用，在jdk9的时候成为默认的垃圾回收器。因为我们是jdk8所以是需要设置参数指定的。

```shell
//最大堆内存24G 使用G1 GC，设置预期停顿时间是95ms
-Xms24g -Xmx24g -XX:+UseG1GC -XX:MaxGCPauseMillis=95
```

**JDK8 升级G1**

1. 使用G1的主要原因是：G1的Stop The World(STW)更可控，G1在停顿时间上添加了预测机制，用户可以指定期望停顿时间。目标很明确，可控的GC时间。
2. 升级启用了G1后解惑不尽人意，甚至比CMS的结果还差，最后还是在业务层面思考，优化业务逻辑，调整内存缓存key的时间。这个才根本上解决了GC的问题。

## 你们生产环境的JVM 的环境和配置是什么？？

![img.png](../.vuepress/public/project/cloud/生产环境JVM.png)

* 4核8G的内存VM
* JDK1.8
* **JVM 堆区分配了4G 堆区**
* **eden 区：1.2G**
* **s0 区：150M**
* **s1 区：150M**
* **old 区：2.5G**
* **方法区: 512M**
* **虚拟机栈区1M**
* young 代：Parallel Scavenge 回收器(复制算法)
* old 代：CMS垃圾回收器(标记清除)
    *  初始标记(CMS initial mark) -stop the world
    *  并发标记(CMS concurrent mark)
    *  重新标记(CMS remark) -stop the world
    *  并发清除(CMS concurrent sweep)
* JVM 每秒能够处理300单。即300单/秒 一个地区一秒能够承受1000单/秒
* 公司一共有12台VM 一共有三个地区日本有3台(和1台备用VM) 印度有3台(和1台备用VM) 香港有印度有3台(和1台备用VM)

## Hashmap使用不当导致的内存泄露，导致频繁GC或者是OOM异常

```java
public class Test {
public static void main(String[] args) {
Map<Person, Integer> map = new HashMap<>();
Person p = new Person("0", 10);

        for (int i = 0; i < 50000; i++) {
            p.setName(String.valueOf(i));
            map.put(p, 1);
            System.out.println(map.size());
        }

        System.out.println("end.");
    }
}

class Person {
private String name;
private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Person) {
            Person personValue = (Person) obj;
            if (personValue.getName() == null && name == null) {
                return true;
            }
            return personValue.getName() != null && personValue.getName().equals(name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
```

直接点击运行，查看结果，发现当put第49153个对象时，报了java.lang.OutOfMemoryError: Java heap space

结果分析：本来，HashMap put同一个对象，理论上是会覆盖的，不会导致内存泄露，这里之所以出现这种情况，
主要是因为我们put的并不是同一个对象（重写了hashcode和equals方法，且hashcode发生了改变），然后一直put，
就导致对象越来越多，最终触发OutOfMemoryError。


```java
public class Test {
public static void main(String[] args) {
Map<Person, Integer> map = new HashMap<>();
for (int i = 0; i < 500000; i++) {
map.put(new Person("0", 10), 1);
System.out.println(map.size());
}

        System.out.println("end.");
    }
}

class Person {
private String name;
private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

同样，直接点击运行，查看结果，发现也报了java.lang.OutOfMemoryError: Java heap space
结果分析：这个没啥好说，Object默认的hashcode对于new出来的对象都是不同的，然后一直put，就导致对象越来越多，最终触发OutOfMemoryError。

**当使用HashMap执行put操作的时候，如果你期望的结果是覆盖这个key，那么你要再三确认put的时候，key对象的hashcode有没有发生变化，否则可能会有意想不到的结果；
建议，当想要使用对象作为HashMap的key时，可以考虑使用不可变对象作为HashMap的key，如常用的String类型，或者确保使用不可变的成员属性来生成hashcode；**

## rocketmq解决延迟消息是什么版本？解决的原理是什么？4.0 和5.0的区别是什么？

实现任意时间的定时的要点在于知道在某一时刻需要投递哪些消息，以及破除一个队列只能保存同一个延迟等级的消息的限制。

联想 Rocketmq 的索引文件 IndexFile，可以通过索引文件来辅助定时消息的查询。需要建立这样的一个索引结构：
Key 是时间戳，Value 表示这个时间要投递的所有定时消息。类似如下的结构：

`Map<Long /* 投递时间戳 */, List<Message /* 被定时的消息 */>>`

把这个索引结构以文件的形式实现，其中的 Message 可以仅保存消息的存储位置，投递的时候再查出来。

RIP-43 中就引入了这样的两个存储文件：TimerWheel 和 TimerLog，存储结构如下图所示：

![img.png](../.vuepress/public/project/cloud/rocketmq5.0-延迟原理.png)

TimerWheel 是时间轮的抽象，表示投递时间，它保存了 2 天（默认）内的所有时间窗。每个槽位表示一个对应的投递时间窗，
并且可以调整槽位对应的时间窗长度来控制定时的精确度。

采用时间轮的好处是它可以复用，在 2 天之后无需新建时间轮文件，而是只要将当前的时间轮直接覆盖即可。

**TimerLog 是定时消息文件，保存定时消息的索引（在CommitLog 中存储的位置）。它的存储结构类似 CommitLog，是 Append-only Log。**

TimerWheel 中的每个槽位都可以保存一个指向 TimerLog 中某个元素的索引，TimerLog 中的元素又保存它前一个元素的索引。
也就是说，TimerLog 呈链表结构，存储着 TimerWheel 对应槽位时间窗所要投递的所有定时消息。

**定时消息轮转：避免定时消息被老化删除**

为了防止定时消息在投递之前就被老化删除，能想到的办法主要是两个：

1. 用单独的文件存储，不受 Rocketmq 老化时间限制：方法 1 需要引入新的存储文件，占用磁盘空间； 
2. 在定时消息被老化之前，重新将他放入 CommitLog：方法 2 则需要在消息被老化前重新将其放入 CommitLog，增加了处理逻辑的复杂性。

RIP-43 中选择了第二种方案，在定时消息放入时间轮前进行判断，如果在 2 天内要投递（在时间轮的时间窗口之内），则放入时间轮，否则重新放入 CommitLog 进行轮转。

**定时任务划分和解耦**

RIP-43 中，将定时消息的保存和投递分为多个步骤。为每个步骤单独定义了一个服务线程来处理。

保存：从定时消息 Topic 中扫描定时消息 将定时消息（偏移量）放入 TimerLog 和 TimeWheel 保存 

投递：从时间轮中扫描到期的定时消息（偏移量） 根据定时消息偏移量，到 CommitLog 中查询完整的消息体 将查到的消息投递到 CommitLog 的目标 Topic 

每两个步骤之间都使用了生产-消费模式，用一个有界的 BlockingQueue 作为任务的缓冲区，通过缓冲区实现每个步骤的流量控制。当队列满时，新的任务需要等待，无法直接执行。