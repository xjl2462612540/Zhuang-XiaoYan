---
lang: zh-CN
sidebarDepth: 2
---

# 四、DevOps-Platform服务可观测性

监控是微服务治理的一个重要环节，监控系统的完善程度直接影响到我们微服务质量的好坏，我们的微服务在线上运行时，
有没有一套完善的监控体系能去了解到它的健康情况，这对整个系统的可靠性和稳定性非常重要。

## 4.1 服务可观测性背景

一个比较完善的微服务监控体系需要涉及到哪些层级？如下图所示，大致可以划分为五个层级的监控：

<img :src="$withBase('/project/devops/monitor01.png')" alt="monitor01">

**基础设施监控**：这层一般由运维人员负责，涉及到的方面比较接近硬件体系，例如网络，交换机，路由器等低层设备，这些设备的可靠性稳定性就直接影响到上层服务应用的稳定性，所以需要对网络的流量，丢包情况、错包情况，连接数等等这些基础设施的核心指标进行监控。

**系统层监控**：这层涵盖了物理机、虚拟机、操作系统等，这些都是属于系统级别监控的方面，主要对几个核心指标进行监控，如cpu使用率、内存占用率，磁盘IO和网络带宽情况。

**应用层监控**：这层涉及到方面和服务紧密相关，例如对url访问的性能，访问的调用数，访问的延迟，还有对服务提供性能进行监控，服务的错误率等，同时对sql也需要进行监控，查看是否有慢sql。对于cache来说，需要监控缓存的命中率和性能，每个服务的响应时间和qps等等。

**业务监控**：业务监控具体指什么？举个例子，比如说一个典型的交易网站，需要关注它的用户登录情况、注册情况、下单情况、支付情况等等，这些直接影响到实际触发的业务交易情况，这层监控可以提供给运营和公司高管们，提供他们需要关注的数据，直接以数据支撑公司在战略层面的决策和方向。

**端用户体验监控**：一个应用程序可能通过app、h5、pc端的方式交付到用户的手上，用户通过浏览器，客户端打开连到我们的服务，那么在用户端，用户的体验是怎么样？用户端的性能是怎么样？以及有没有产生错误等等……这些信息都需要进行监控并记录下来，如果没有监控，有可能因为某些BUG或者性能问题，造成用户体验非常差，而我们并没有感知。其中包括监控用户端的使用性能、返回码，在哪些城市地区，他们的使用情况是怎么样，还有运营商的情况，包括三大运营商不同用户的连接情况。我们需要进一步知道，是否有哪些渠道哪些用户接入的时候存在着问题，我们还需要知道客户端使用的操作系统浏览器的版本。

总结：这就是我们体系化的监控分层，每一个层级都非常重要。一般情况下，当一个问题出现时，较大概率会先暴露在用户端或业务层，比如说，我们的订单量下降了，业务人员和开发人员会先从上到下去逐层检查是在哪里出现了问题，先确定是否哪个接口调用比较慢，哪个服务调用出现延时，再看是否哪个机器负载过高了，然后再进一步往下一个层去看，是否是网络调用不稳定导致。所以，一个好的监控体系，在每个层级都非常重要。

## 4.2 监控系统要点

<img :src="$withBase('/project/devops/monitor02.png')" alt="monitor02">

微服务监控系统设计要点可以分为以下六个点：
1. 日志监控
2. Metrics监控
3. 调用链监控
4. 报警系统
5. 健康检查
6. 异常处理服务

在微服务运行的体系下，我们一般把监控的agent分散到各个服务身边，agent分别是收集机器和服务的metrics，发送到后台监控系统，
一般来说，我们的服务量非常大，在收集的过程中，会加入队列。一般来说用kafka作为MQ消息中间件有个好处，两边可以进行解耦，
可以起到庞大的日志进行一个缓存的地带，并且可以做到高可用，保证消息不会丢失。

日志收集目前比较流行的是ELK的一套解决方案（Elasticsearch，Logstash，Kibana），Elasticsearch 分布式搜索引擎，
Logstash是一个日志收集的agent，Kibana 是一个查询的日志界面。metrice会采用一个时间序列的数据库，influxDB是最近比较主流时间数据库。

微服务的agent例如springboot也提供了健康检查的端点，可以检查cpu使用情况、内存使用情况、jvm使用情况，
这些需要一个健康检查机制，能够定期对服务的健康和机器的健康进行check，比较常见的是nagios、zabbix等，
这些开源平台能够定期去检查到各个微服务的检查程序并能够进行告警给相关人员，在服务未崩溃之前就可以进行提前的预先接入。

## 4.3 基础设施监控

### 4.3.1 Prometheus存储数据

Promethes是一款2012年开源的监控框架，其本质是时间序列数据库，由Google前员工所开发。
Promethes采用拉的模式（Pull）从应用中拉取数据，并还支持 Alert 模块可以实现监控预警。
它的性能非常强劲，单机可以消费百万级时间序列。架构如下：

<img :src="$withBase('/project/devops/Promethes.png')" alt="Promethes">

**Prometheus主要组件**

1. Prometheus Server:用于拉取 metrics 信息并将数据存储在时间序列数据库。
2. Jobs/exporters:用于暴露已有的第三方服务的 metrics 给 Prometheus Server，比如StatsD、Graphite 等，负责数据收集。
3. Pushgateway:主要用于短期 jobs，由于这类 jobs 存在时间短，可能在 PrometheusServer 来拉取 metrics 信息之前就消失了，所以这类的 jobs 可以直接向 PrometheusServer 推送它们的 metrics 信息。
4. Alertmanager:用于数据报警。
5. Prometheus web UI:负责数据展示。

### 4.3.2 exporter数据采集

### 4.3.3 Grafana数据可视化

### 4.3.4 Alertmanager告警

### 4.3.4 异常自动处理 



