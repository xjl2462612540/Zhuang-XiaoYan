---
lang: zh-CN
sidebarDepth: 2
---

# Netty面试问题

## IO和NIO有什么区别？

1. 首先，I/O ，指的是IO流， 它可以实现数据从磁盘中的读取以及写入。实际上，除了磁盘以外，内存、网络都可以作为I/O流的数据来源和目的地。在Java里面，提供了字符流和字节流两种方式来实现数据流的操作。

2. 其次，当程序是面向网络进行数据的IO操作的时候，Java里面提供了Socket的方式来实现。通过这种方式可以实现数据的网络传输。

3. 基于Socket的IO通信，它是属于阻塞式IO，也就是说，在连接以及IO事件未就绪的情况下，当前的连接会处于阻塞等待的状态。

![img.png](../.vuepress/public/interview/netty/IO01.png)

如果一旦某个连接处于阻塞状态，那么后续的连接都得等待。所以服务端能够处理的连接数量非常有限。

NIO，是JDK1.4里面新增的一种NEW IO机制，相比于传统的IO，NIO在效率上做了很大的优化，并且新增了几个核心组件。Channel、Buffer、Selectors。
另外，还提供了非阻塞的特性，所以，对于网络IO来说，NIO通常也称为No-Block IO，非阻塞IO。

也就是说，通过NIO进行网络数据传输的时候，如果连接未就绪或者IO事件未就绪的情况下，服务端不会阻塞当前连接，而是继续去轮询后续的连接来处理。
所以在NIO里面，服务端能够并行处理的链接数量更多。

![img.png](../.vuepress/public/interview/netty/IO02.png)

因此，总的来说，IO和NIO的区别，站在网络IO的视角来说，前者是阻塞IO，后者是非阻塞IO。

## BIO、NIO、AIO分别是什么?

1. BIO:同步阻塞lO，使用BIO读取数据时，线程会阻塞住，并且需要线程主动去查询是否有数据可读，并且需要处理完一个Socket之后才能处理下一个Socket
2. NIO:同步非阻塞IO，使用NIO读取数据时，线程不会阻塞，但需要线程主动的去查询是否有IO事件
3. AlO:也叫做NIO 2.0，异步非阻塞IO，使用AIO读取数据时，线程不会阻塞，并且当有数据可读时会通知给线程，不需要线程主动去查询。

## 为什么Netty使用NIO而不是AIO？

1. Netty不看重Windows上的使用，在Linux系统上，AIO的底层实现仍使用EPOLL，没有很好实现AIO，因此在性能上没有明显的优势，而且被JDK封装了一层不容易深度优化。
2. Netty整体架构是reactor模型, 而AIO是proactor模型, 混合在一起会非常混乱,把AIO也改造成reactor模型看起来是把epoll绕个弯又绕回来。
3. AIO还有个缺点是接收数据需要预先分配缓存, 而不是NIO那种需要接收时才需要分配缓存, 所以对连接数量非常大但流量小的情况, 内存浪费很多。
4. Linux上AIO不够成熟，处理回调结果速度跟不上处理需求，比如外卖员太少，顾客太多，供不应求，造成处理速度有瓶颈。

## Netty是什么?

* Netty是一个基于NIO的异步网络通信框架，性能高，封装了原生NIO编码的复杂度，开发者可以直接使用Netty来开发高效率的各种网络服务器，并且编码简单。
* Tomcat是一个Web服务器，是一个Servlet容器，基本上Tomcat内部只会运行Servlet程序，并处理HTTP请求，而Netty封装的是底层IO模型，
关注的是网络数据的传输，而不关心具体的协议，可定制性更高。

**Netty的特点:**
1. 异步、NIO的网络通信框架,netty是对NIO模型一个封装，提供了一个简单易用的API。
2. 零拷贝机制（socket的读写、ByteBuf合并为一个逻辑上的ByteBuf），高性能无锁队列(多路复用就是一种无锁机制，ChannelPipeline中的Handler，期间只要用户不主动切换线程，该用户的Handler就会一直被同一个NioEventLoop调用)，内存池。
3. Netty支持多种通信协议，例如http、websocket等
4. Netty的数据的拆包和粘的问题，内置了很多的拆包的策略。有四种拆包策略。

## Netty的高性能体现?

1. 基于异步、非阻塞的I/O模型。Netty 中的I/O操作都是异步的，比如`bind、write、connect`等我们可以通过事件监听机制，方便的获取操作结果;此外，Netty非阻塞I/О另外一个实现的关键就是基于NIO的多路复用技术。
2. 基于主从 Reactor的线程模型。
3. 采用了零拷贝技术。
4. ByteBuf 内存池的设计。随着JVM虚拟机和JIT即时编译技术的发展，对象的分配和回收是个非常轻量级的工作。但是对于缓冲区Buffer (相当于一个连续的内存区域)，情况却稍有不同，特别是对于堆外直接内存的分配和回收，是一件耗时的操作。为了尽量重用缓冲区，Netty 提供了基于ByteBuf内存池的缓冲区重用机制。需要的时候直接从内存池中获取ByteBuf使用即可，使用完毕之后就重新放回到内存池中去。
5. 采用无锁化的串行设计思想。
6. 支持多种高性能的序列化协议。
7. 高效的并发编程。比如Volatile、CAS、线程安全容器、读写锁的使用等。
8. 灵活的TCP参数配置能力。我们可以通过ChannelOption来配置多个TCP参数，合理的TCP参数可以很好的提高应用程序的性能。

## Netty的核心组件

![img.png](../.vuepress/public/interview/netty/netty-核心组件图.png)

![img.png](../.vuepress/public/interview/netty/netty-分层核心组件.png)

**Bootstrap & ServerBootStrap**

基于Netty 开发的应用程序通常是由创建一个`Bootstrap`开始，`Bootstrap`的主要作用是配置整个Netty程序，串联各个组件。
Netty中`Bootstrap类是客户端程序的启动引导类`，**主要负责客户端启动并连接远程的Netty Server**;

```java
Bootstrap bootstrap = new Bootstrap( );
bootstrap.group( .. . ) .channel( . . . ).handler( ... );bootstrap.connect( "127.0.0.1",8888).sync( );
```

而`ServerBootstrap`是服务端的启动引导类，主要用来监听指定的端口。

```java
ServerBootstrap bootstrap = new ServerBootstrap( );
bootstrap.group(.. . ).channel(.. . ).option(... ).childHandler(...);bootstrap. bind ( 8888).sync( );
```

**NioEventLoop & NioEventLoopGroup**

**NioEventLoop**中维护了一个线程和一个任务队列。它支持异步提交执行任务，线程启动时会调用NioEventLoop的run方法，执行I/O任务和非I/O任务:

* I/O任务，即selectionKey中ready的事件，如accept、connect、read、write等，由processSelectedKeys方法触发。
* 非IO任务，添加到taskQueue中的任务，如register0、bind0等任务，由runAllTasks方法触发。

1. EventLoop 定义了Netty的核心抽象，用来处理连接的生命周期中所发生的事件，在内部，将会为每个Channel分配一个EventLoop。
2. EventLoopGroup 是一个 EventLoop 池，包含很多的 EventLoop。
3. Netty 为每个 Channel 分配了一个 EventLoop，用于处理用户连接请求、对用户请求的处理等所有事件。EventLoop 本身只是一个线程驱动，在其生命周期内只会绑定一个线程，让该线程处理一个 Channel 的所有 IO 事件。
4. 一个 Channel 一旦与一个 EventLoop 相绑定，那么在 Channel 的整个生命周期内是不能改变的。一个 EventLoop 可以与多个 Channel 绑定。即 Channel 与 EventLoop 的关系是 n:1，而 EventLoop 与线程的关系是 1:1。

**EventLoop的run方法在一个for死循环中，周而复始的做着三件事：**

1. 从已注册的Channel监听IO事件；
2. 处理IO事件；
3. 从任务队列取任务执行。

**NioEventLoopGroup**，可以理解为一个线程池，内部维护了一组线程，每个线程(NioEventLoop )负责处理多个 Channel上的事件，而一个Channel只对应于一个线程。

```java
EventLoopGroup bossGroup = new NioEventLoopGroup(1);
EventLoopGroup workerGroup = new NioEventLoopGroup( );
```

**Channel**

Channel相当于完成网络通信的载体，能够用于执行网络I/O操作。下面是一些常用的Channel类型:

1. `LocalServerChannel`：用于本地传输的ServerChannel ，允许 VM 通信。
2. `EmbeddedChannel`：以嵌入式方式使用的 Channel 实现的基类。
3. `NioSocketChannel`：异步的客户端  TCP 、Socket 连接。
4. `NioServerSocketChannel`：异步的服务器端  TCP、Socket 连接。
5. `NioDatagramChannel`： 异步的  UDP 连接。
6. `NioSctpChannel`：异步的客户端 Sctp 连接,它使用非阻塞模式并允许将 SctpMessage 读/写到底层 SctpChannel。
7. `NioSctpServerChannel`：异步的 Sctp 服务器端连接，这些通道涵盖了 UDP 和 TCP 网络 IO 以及文件 IO。

**Selector**

Netty 是基于Selector对象实现的I/O多路复用，通过Selector一个线程可以监听多个连接的Channel事件。

当向一个Selector中注册Channel后，Selector可以自动不断地查询(Select)这些注册的Channel 是否有已就绪的I/O事件，这样程序就可以很简单地使用一个线程高效地管理多个Channel 。

**ChannelHandler & ChannelPipeline & ChannelHandlerContext**

ChannelHandler主要负责处理输入输出数据的逻辑，它可以接收入站事件和出站事件，并执行相应的处理逻辑。
ChannelHandler被添加到ChannelPipeline中，形成处理链。入站和出站事件的操作可以通过实现如下的接口或者继承如下的适配器来完成:

```java
ChannelInboundHandler用于处理入站I/0事件的接口。
ChannelOutboundHandler用于处理出站I/0操作的接口。
ChannelInboundHandlerAdapter 用于处理入站I/0事件的适配器。
ChannelOutboundHandlerAdapter用于处理出站I/0操作的适配器。
```

ChannelPipeline是一个双向链表，当数据通过Channel时，它会依次经过每个ChanneHandler进行处理。

ChanneHandlerContext是ChannelHandler的上下文环境。包含与ChannelHandler相关联的各种信息，如Channel、EventLop、ChannelPipeline等。另外，ChannelHandlerContext 还提供了丰富的方法，以便于ChannelHandler可以与其他组件进行交互。

![img.png](../.vuepress/public/interview/netty/netty-channel.png)

一个Channel包含了一个ChannelPipeline，而ChannelPipeline 中又维护了一个由ChannelHandlerContext组成的双向链表，并且每一个ChannelHandlerContext关联这一个ChannelHandler。

**ChannelFuture**

在Netty中所有的IO操作都是异步的，因此，我们并不能立刻得到操作是否执行成功。但是，你可以通过ChannelFuture接口的addListener()方法注册一个ChannelFutureListener，
当操作执行成功或者失败时，监听就会自动触发返回结果。另外，我们还可以通过ChannelFuture 接口的sync()方法让异步的操作变成同步的。
```java
ChannelFuture cf = bootstrap.bind (8888);
cf.addListener(new ChannelFutureListener() {
    @override
    public void operationComplete(ChannelFuture future)throws Exception {
        if (cf.isSuccess()) {
            system.out.println("监听端口8888成功");}
        else {
            System.out.println("监听端口8888失败");
        }
    }
});

//或者通过调用sync()方法让异步的操作变成同步的
bootstrap.bind (8888).sync();
```

## Netty的线程模型

![img.png](../.vuepress/public/interview/netty/netty的线程模型.png)

1. 创建服务端的时候实例化了 2 个 EventLoopGroup。`bossGroup` 线程组实际就**是Acceptor线程池，负责处理客户端的TCP连接请求**。 `workerGroup` 是**真正负责I/O读写操作的线程组**。通过这里能够知道Netty是多Reactor模型。
2. `BossGroup`和 `WorkerGroup `都是`NioEventLoopGroup`类的实例;
3. NioEventLoopGroup相当于一个事件循环线程组,它包含多个事件循环线程，每一个事件循环线程都是一个NioEventLoop;
4. 每个NioEventLoop都有一个Selector ,用于监听注册在其上的socketChannel的网络通信;
5. BossGroup 下的NioEventLoop 线程内部循环执行的步骤如下:
   * 轮询accept事件;
   * 处理 accept事件，与client建立连接﹐生成NioScocketChannel，并将其注册WorkerGroup上的某个NIOEventLoop上的selector;
   * 处理任务队列的任务，即runAllTasks;
6. 每个Worker Group下的NIOEventLoop 线程内部循环执行的步骤如下:
   * 轮询注册到自己Selector 上的所有NioSocketChannel上的read/write事件;
   * 在对应的NioScocketChannel上处理I/О事件，即read/write事件;
   * 处理任务队列的任务，即runAllTasks;

## 说说Netty中的编解码器(序列化和反序列化)？

* 编码（(Encode)，也称为序列化，它主要是将对象转化为字节数组，以便于网络传输或者数据持久化;
* 解码(Decode)，也称为反序列化，它的主要作用是将网络或者磁盘中读取的字节数组转化为原始对象。

我们知道，网络通信中数据都是以二进制字节流的形式进行传输的，因此我们需要使用编解码器来对数据进行处理。编码器的主要作用是将对象转化为二进制字节流，而解码器的主要作用是将二进制字节流转化为原始对象。
Netty中提供了一系列的编解码器来供我们使用，**它们都是ChannelHandlerAdapter类的子类**，因此也是一种特殊的**ChannelHandler**。

**编码器**： 对于编码器，Netty 中提供了两个抽象基类:`MessageToByteEncoder``与MessageToMessageEncoder`。
其中MessageToByteEncoder会将消息编码为字节，MessageToMessageEncode会将消息编码为另一种消息。

我们可以通过继承这两个基类的其中一个来自定义我们自己的编码器。Netty内置的基于`MessageToByteEncoder`的常见子类主要是编码对象的`ObjectEncoder`。
而基于`MessageToMessageEncoder`的常见子类包括: `StringEncoder`、`ProtobufEncoder`、`LineEncoder`、`Base64Encoder等`。

**解码器**：对于解码器，Netty同样也提供了两个抽象基类:`ByteToMessageDecoder`与`MessageToMessageDecoder`。其中
ByteToMessageDecoder会将字节解码为消息，MessageToMessageDecoder 会将消息解码为另一种消息。我们可以通过继承这两个基类的其中一个来自定义我们自己的解码器。

Netty内置的基于ByteToMessageDecoder 的常见子类有:`ObjectDecoder`、`FixedLengthFrameDecoder`、`LineBasedFrameDecoder`、`DelimiterBasedFrameDecoder`、`LengthFieldBasedFrameDecoder等`。
而基于MessageToMessageDecoder的常见子类包括: `StringDecoder`、`ProtobufDecoder`、`Base64Decoder`等。

**编解码器**： Netty中还提供了一种同时具有编码与解码功能的编解码器，它同样有2个抽象基类:`ByteToMessageCodec`和`MessageToMessageCode`，
它们继承自适配器类ChannelDuplexHandler。

## 说说Netty中Handler的执行顺序

我们知道，一个 Channel包含了一个ChannelPipeline，而ChannelPipeline 中又维护了一个由ChannelHandlerContext 组成的双向链表，并且每个ChannelHandlerContext中又关联着一个ChannelHandler。如下图:

![img.png](../.vuepress/public/interview/netty/netty-channel.png)

另外，ChannelHandler 又分为入站Handler和出站Handler两种，很显然，**入站Handler的执行顺序都是顺序执行的。**

而出站Handler又分为两种情况∶
1. 调用的是Channel或者ChannelPipeline上的write()方法，则会从队尾向前遍历出站Handle依次执行; 
2. 调用的是Context上的write()方法，则从当前的Handler处向前遍历出站Handler依次执行

## Netty内置的那些序列化的协议

1. **Java原生序列化**。Netty中内置的ObjectEncoder/ObjectDecoder就是使用JDK序列化协议编解码的。Java 原生的序列化协议，可以序列化所有实现了Serializable 接口的对象。Java序列化虽然简单易用，但是序列化后的字节数较大，而且性能较差，且不具备跨语言的能力，因此不推荐使用。
2. **JSON序列化**。Netty中内置的JsonObjectDecoder就是对JSON格式数据的解码器。JSON是一种轻量级的数据交换格式，易于阅读和编写，同时也具备跨语言的能力，因此在分布式系统中被广泛使用。
3. **XML序列化**。Netty中内置的 XmlDecoder就是对XML格式数据的解码器。XML 也是早期常用的数据交换格式，它也具备跨语言的能力。
4. **Protobuf序列化**。Netty中内置的ProtobufEncoder/ProtobufDecoder就是针对Protobuf 序列化协议的编解码器。Protobuf是Google开源的一种高效灵活的二进制序列化协议，具备良好的跨语言能力和高效的序列化性能。它同样是分布式系统中使用较多的一种序列化协议。

当然，因为 Netty本身是支持自定义编解码器的，因此市面上其他常见的序列化协议它同样也是支持的，包括但不局限于以下几种:

1. **Protostuff序列化**。我们知道，Google的 Protobuf因其序列化后体积小，性能高，同时具备良好的跨语言能力而被广泛使用。但是它需要编写.proto 文件，再通过Protobuf 转换成对应的Java 代码，非常不好维护。而Protostuff 正是为了解决这个痛点而产生的，它是基于Protobuf 实现的，但是不需要编写.proto文件，只需要编写普通的Java bean就可以实现序列化和反序列化。
2. **Thrift序列化**。Thrift 是由 Facebook 实现的一种高效的、支持多语言的远程服务调用框架，即RPC。后来被开源到了Apache。Thrift 虽说是一个RPC框架，但是由于它提供了多语言之间的 RPC 服务，所以很多时候也被用于序列化中。
3. **MessagePack序列化**。它是一种高效的二进制序列化协议，类似于JSON。MessagePack将数据压缩为二进制格式，具有较小的数据体积和高速的编码解码能力。它支持多种编程语言，并且可以与JSON相互转换。
4. **Avro(读音类似于[aevre])序列化**。它是一种由Apache 开发的基于Schema的二进制序列化协议。Avro使用JSON来定义数据结构，并将数据编码为紧凑的二进制格式。它支持动态类型、架构演化和跨语言等特性。

## Netty的心跳检测机制

所谓心跳，即在TCP长连接中,客户端和服务器之间定期发送的一种特殊的数据包,通知对方自己还在线,以确保TCP连接的有效性。

为什么需要心跳检测机制呢?其实也很简单，在TCP长连接过程中，可能会出现网络异常等情况此时如果TCP连接已经断开，但是服务端并没有正常的关闭Socket，
就会出现"连接假死"的现象。假死的连接会继续耗费CPU和内存资源，造成资源的浪费。而心跳检测机制正是解决这一问题的有效手段。

Netty 中内置了一个空闲状态处理器ldleStateHandler来实现心跳的检测。首先让我们看下它的构造方法:

```java
public IdleStateHandler(int readerIdleTimeSeconds，int writerIdleTimeSeconds,int allIdleTimeSeconds){
    this(readerIdleTimeSeconds,writerIdleTimeSeconds,allIdleTimeSecond，TimeUnit.SECONDS);
}
```

该构造方法的三个参数解释如下︰
1. readerldleTimeSeconds:在指定的时间内没有从 Channel中读取到数据时，就会触发一个读空闲事件。
2. writerldleTimeSeconds:在指定的时间内没有写入数据到Channel时，就会触发一个写空闲事件
3. allldleTimeSeconds:在指定的时间内没有读或写操作时，就会触发一个读写空闲事件。

要实现Netty服务端心跳检测机制,需要完成2步:

1. 需要在服务器端的pipeline 中添加ldleStateHandler:
2. 另外需要实现一个自定义的 Handler，并重写userEventTriggered()方法，并将该Handler添加到 pipeline 中，顺序为ldleStateHandler的下一个。

## Netty中如何实现网络断线的自动重连？

断线重连一般分为两种情况︰
1. 客户端启动连接服务端时，如果网络或服务端出现问题，**客户端连接失败而发起的自动重连**;
2. 系统运行过程中网络故障或服务端故障，导致客户端与服务端断开了连接，此时也需要发起自动重连;

针对以上两种情况，Netty中的一般处理方案是︰
1. 针对第一种情况，一般会在客户端发起连接后，添加一个监听操作完成事件，操作完成后，判断当前连接是否成功，如果不成功，则继续发起重连;
2. 针对第二种情况，一般会在客户端的 Handler中重写channellnactive方法中进行处理。

具体的重连处理，一般不建议以固定频率一直尝试。我们可以参考Nacos中的方案∶
1. 设置一个重连间隔时间基数，每重连一次，该值左移一位，连接服务器成功后在完成值的复位; 举个例子:设置重连间隔时间基数为2s，每尝试一次，左移一位(2<<1)，连接成功后基数复位为2s。具体就是第一次重连是2s.
第二次重连是4s，第三次是8s,以此类推...
2. 设置—个最大重试次数和最长间隔时间;

## Netty的buffer有那些改进

我们知道，Java NIO中的ByteBuffer APIl使用复杂，而且无法动态扩容。为此，Netty重新封装了自己的ByteBuf，无论是在性能上还是使用上都做了很大的改进。ByteBuf 的改进主要有以下几个方面︰

1. **读写索引替代flip()**。否者存在数据读取不到的情况。
2. **动态扩缩容**。判断的是否大于4M。
3. **提供了多种ByteBuf的实现，使用上更加灵活**。

**读写索引替代flip()**： **这样我们就可以读取到数据了，但是如果忘记了调用flip()，程序就会出现错误!**

![img.png](../.vuepress/public/interview/netty/java-bytebuffer.png)

**Netty ByteBuf的读写，而Netty 中的 ByteBuf通过读写索引避免了上述的问题，下面的示意图很清楚的展示了一个ByteBuf 是如何被这两个索引分割成可丢弃、可读、可写三部分的︰**

![img.png](../.vuepress/public/interview/netty/netty-bytebuffer.png)

**动态扩缩容**: Netty 中的ByteBuf在写入数据时，会根据写入数据的字节数量，自动扩容。扩容规则总结如下:

1. 写入数据后需要的容量=4M，扩容的容量直接取4M;
2. 小于4M，则会从64byte开始扩容，每次增加一倍，直到计算出来的容量满足最小容量要求。假如当前容量大小是100字节，已经使用80字节，再次写入50字节，则需要的最小容量是80 +50 = 130字节，那么扩容后的容量为64*2*2 = 256字节;
3. 大于4M，扩容计算方法为:最小容量/4*4+4(M)，假如当前容量大小是3M，已经写入2M，在写入5M，则最小容量是2＋5=7M，则扩容后的容量为7/4*4+4 = 8M。

**多种 ByteBuf的实现**: Netty根据不同的场景，有不同的ByteBuf 的实现，比如:PooledDirectByteBuf、PooledHeapByteBuf、UnpooledDirectByteBuf、UnpooledHeapByteBuf 等。
此外，Netty还提供了静态工具类Unpooled 来供我们操作 ByteBuf，比如∶

```java
1 //创建一个容量为10的堆缓冲区
2 ByteBuf buf = Unpooled.buffer(10);
3 //创建一个容量为10的直接内存缓冲区
4 ByteBuf buf = Unpooled.directBuffer(10);
```

## Netty的零拷贝的原理？

最初，狭义的零拷贝是指在操作系统中，避免在用户态和内核态之间拷贝数据;而现在广义的零拷贝是指在操作数据时，
**不需要将数据从一个内存区域拷贝到另外一个内存区域。少了一次内存拷贝，CPU的效率自然也就得到了提升。**

Netty的零拷贝主要体现在以下5个方面:
1. **在网络通信上**，使用直接内存。**Netty接收和发送ByteBuf采用堆外直接内存进行Socket 读写不需要进行字节缓冲区的二次拷贝**。
2. **在缓存操作上**，Netty提供了CompositeByteBuf类。它可以将多个ByteBuf合并为一个逻辑上的ByteBuf，合并的过程其实仅仅是多个ByteBuf对象指针的映射，而不存在各个ByteBuf之间的拷贝。
3. **在缓存操作上**，Netty的静态工具类Unpooled提供了wrappedBuffer方法。它可以将byte[]、ByteBuf、 ByteBuffer 等包装成一个Netty ByteBuf 对象，包装过程中不会产生内存拷贝。
4. **在缓存操作上**，Netty 的ByteBuf还提供了slice()方法。它可以将ByteBuf分解为多个共享同一个存储区域的ByteBuf，从而避免了内存间的拷贝。
5. **在文件传输上**，Netty通过FileRegion实现文件传输。而FileRegion其实正是基于Java NIO中的FileChannel.transferTo()方法实现的，该方法可以将文件缓冲区的数据直接传输到目标Channel，避免了内核缓存区和用户缓冲区之间的数据拷贝。另外，FileChannel.transferTo()方法底层是基于Linux的sendFile函数实现的零拷贝。

## Netty的无锁化设计

在大多数场景下，采用多线程处理可以提升系统的并发性能，对于随之而来的线程安全问题，我们通常的处理方案就是加锁。但是,加锁本身也是一种开销很大的方案，而且还有可能出现死锁。

为了尽可能的避免锁竞争带来的性能开销，我们可以采用`串行化的无锁设计理念`，即消息的处理尽可能在同一个线程内完成，期间不进行线程切换，这样就避免了多线程竞争和线程安全的问题。

Netty中就采用了串行无锁化设计，在I/O线程内部进行串行操作，避免多线程竞争导致的性能下降。表面上看，串行化设计似乎CPU利用率不高。
但是，我们完全可以通过调整NIO线程池的线程参数，同时启动多个串行化的线程并行运行，这种局部无锁化的串行线程设计相比一个队列-多个工作线程的模型性能更优。

具体来说，Netty 中的串行无锁化设计体现下如下2个方面:

1. **Netty是基于Reactor模型通过多路复用器接收并处理用户请求的。NIO中的多路复用技术本身就是一种无锁串行化的设计思想**。
2. **Netty中的NioEventLoop读取到消息之后，会执行ChannelPipeline中的Handler**，期间只要用户不主动切换线程，
该用户的Handler就会一直被同一个NioEventLoop调用，即同一个线程执行，这种串行化的处理方式避免了多线程操作而导致的锁的竞争，从性能角度来看是最优的。

## Netty的内存池策略？

## websocket原理？

## Netty的数据的拆包和粘的问题

针对TCP的粘包和拆包问题，**Netty提供了完善的解决方案。它通过预先指定的数据流解码器，按照特定的规则进行数据的解析**，从而解决粘包和拆包问题。

Netty主要提供了以下四种解码器:
1. **按照换行符分割报文的解码器**:`LineBasedFrameDecoder`;
2. **按照自定义分隔符分割报文的解码器**:`DelimiterBasedFrameDecoder`;
3. **按照固定长度分割报文的解码器**:`FixedLengthFrameDecoder`;
4. **按照数据包长度分割报文的解码器**:`LengthFieldBasedFrameDecoder`;

## Netty中Reactor模式的有几种？

Reactor其实是在NIO多路复用的基础上提出的一个高性能IO设计模式。它的核心思想是把响应IO事件和业务处理进行分离，通过一个或者多个线程来处理IO事件。
然后把就绪的事件分发给业务线程进行异步处理。

**Reactor模型有三个重要的组件:**

* Reactor ：把I/O事件分发给对应的Handler
* Acceptor ：处理客户端连接请求
* Handlers ：执行非阻塞读/写，也就是针对收到的消息进行业务处理。

在Reactor的这种设计中，有三种模型分别是
* **单线程Reactor模型**。
* **多线程Reactor模型**。
* **主从多Reactor多线程模型**。

单线程Reactor模型，就是由同一个线程来负责处理IO事件以及业务逻辑。

这种方式的缺点在于handler的执行过程是串行，如果有任何一个handler处理线程阻塞，就会影响整个服务的吞吐量。

![img.png](../.vuepress/public/interview/netty/Reactor01.png)

所以，就有了多线程Reactor模型。也就是把处理IO就绪事件的线程和处理Handler业务逻辑的线程进行分离，每个Handler由一个独立线程来处理，
在这种设计下，即便是存在Handler线程阻塞问题，也不会对IO线程造成影响。

![img.png](../.vuepress/public/interview/netty/Reactor02.png)

在多线程Reactor模型下，所有的IO操作都是由一个Reactor来完成的，而且Reactor运行在单个线程里面。对于并发较高的场景下，Reactor就成为了性能瓶颈，所以在这个基础上做了更进一步优化。
提出了多Reactor多线程模型，这种模式也叫Master-Workers模式。它把原本单个Reactor拆分成了Main Reactor和多个SubReactor，Main Reactor负责接收客户端的链接，然后把接收到的连接请求随机分配到多个subReactor里面。
SubReactor负责IO事件的处理。这种方式另外一个好处就是可以对subReactor做灵活扩展，从而适应不同的并发量，解决了单个Reactor模式的性能瓶颈问题。

![img.png](../.vuepress/public/interview/netty/Reactor03.png)

## Netty中时间轮的理解？

时间轮，简单理解就是一种用来存储一系列定时任务的环状数组，它的整个工作原理和我们的钟表的表盘类似。它由两个部分组成， 一个是环状数组，另一个是遍历环状数组的指针。
首先，定义一个固定长度的环状数组，然后数组的每一个元素代表一个时间刻度，假设是1s，那么如果是长度为8的数组，就代表8秒钟。
然后，有一个指针，这个指针按照顺时针无线循环这个数组，每隔最小时间单位前进一个数组索引。
这个指针转一圈代表8秒钟，转两圈表示16秒，假设从0点0分0秒开始，转一圈以后就到了0点0分9秒钟。

![img.png](../.vuepress/public/interview/others/timewheel.png)

当指针指向某个数组的时候，就会把这个数组中存储的任务取出来，然后遍历这个链表逐个运行里面的任务。如果某个定时任务的执行时间大于环形数组所表示的长度，一般可以使用一个圈数来表示该任务的延迟执行时间。
也就是说，如果是第16秒要执行的任务，那意味着这个任务应该是在第二圈的数组下标0位置执行。

使用时间轮的方式来管理多个定时任务的好处有很多，我认为有两个核心原因：
1. 减少定时任务添加和删除的时间复杂度，提升性能。
2. 可保证每次执行定时器任务都是O（1）复杂度，在定时器任务密集的情况下，性能优势非常明显。

当然，它也有缺点，对于执行时间非常严格的任务，时间轮不是很适合，因为时间轮算法的精度取决于最小时间单元的粒度。假设以1s为一个时间刻度，那小于1s的任务就无法被时间轮调度。时间轮算法在很多地方都有用到，比如Dubbo、Netty、Kafka等。

## Netty是如何解决JDK中空轮询BUG的?

JDK中的空轮询BUG是NIO中Selector的 BUG，它会导致Selector空轮询，最终造成CPU100%。JDK官方声称在JDK1.6版本的update18修复了该问题，
但是直到JDK1.7版本该问题仍旧存在，只不过该BUG发生概率降低了一些而已，但是它并没有被根本解决，甚至JDK1.8的131版本中依然存在。

**其实，这个问题的本质原因是︰在部分Linux 2.6内核中，当出现客户端的突然中断时，epoll会将该socket返回的eventSet事件集合置为POLLHUP或 POLLERR，
eventSet事件集合发生了变化，就可能导致Selector被唤醒。但是这个时候Selector的select方法返回的numKeys是O，
所以接下来本该对key进行遍历的处理事件就根本执行不了，此时就会回到while循环，循环执行、不断的轮询，直到CPU 100%而最终导致程序崩溃**

**Netty针对这个BUG进行了修复，其实修复的办法也谈不上高级。简单来说就是使用一个计数器统计Selector空轮询的次数，
当超过设定的阈值(默认是512)时，则会重新创建一个Selector，并将原来注册在老的Selector 上的 Channel，重新注册到新的Selector上，同时关闭老的Selector。**

![img.png](../.vuepress/public/interview/netty/netty-空轮询.png)

## Netty中的Channel和jdk nio包下的Channel是什么关系呢?

Netty是一个基于JDK NIO之上的一个封装框架，它的所有功能还是要基于JDK NIO包的这些基础组件来完成的，
所以**Netty的Channel内部包含着一个JDK NIO包的一个Channel对象，并且会将内部NIO Channel对象设置为非阻塞模式**。

基于Java Nio的，那么与Java中的SocketChannel什么关系呢？是一对一的关系。

```shell
NioSocketChannel          --------->     SocketChannel           （1:1）
NioServerSocketChannel    --------->     ServerSocketChannel     （1:1）
```

## Netty客户端Bootstrap.connect()的过程，详细的说一下?

```java
EventLoopGroup bossGroup = new NioEventLoopGroup(1);
EventLoopGroup workerGroup = new NioEventLoopGroup();
ServerBootstrap serverBootstrap = new ServerBootstrap();-+` 
serverBootstrap.group(bossGroup, workerGroup)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new StringEncoder());
                pipeline.addLast(new StringDecoder());
                //业务处理器
                pipeline.addLast(new NettyServerHandler());
            }
        });
ChannelFuture channelFuture = serverBootstrap.bind(hostname, port).sync();

System.out.println("服务提供方开始提供服务~~");

channelFuture.channel().closeFuture().sync();
```

1. boostrap设置bossGroup和workerGroup
2. 用反射的方式的方式创建NioChannel(这时还没创建)
3. 添加channel里 pipeline的编码、解码、自定义Handler.
4. 执行bind()方法注意在serverBootstrap的init方法是在bind方法里进行的，之前的都是配置赋值。
5. 创建channel: initAndRegister 将channel注册到EventLoop中去。
6. 绑定端口，开启Socket服务都是异步执行的，返回的都是Future ,所以要sync()。

## Channel注册到NioEventLoop的过程，详细的说下?

NIO API最核心的一个类就是Selector选择器，它调用的操作系统函数完成一对多监听，在Java中使用的是Selector这个对象完成的调用，
也就是说 NioEventLoop内一定是持有一个Selector对象的，用来监听socket的。

**Channel注册到线程NioEventLoop时，它底层就做了一件事，就是将Netty Channel对象内部维护的JDK NIO Channel注册到NioEventLoop#Selector对象内**。

## Promise是什么？

1. promise接口全异步处理(类似ajax)
2. hannelPromise里有Channel，channel里有eventLoop
3. promise最终由eventLoop来执行

## NioEventLoop组件的工作过程？

NioEventLoop内部的线程创建后，就去执行它自身的run方法，这里面是个for(;;)死循环，循环内它首先需要计算一个IO的选择策略，
这个策略表示调用selector.selectNow(…)还是selector.select(…)方法，selectNow(…)是非阻塞的，select是阻塞直到有监听的socket就绪，
**使用哪种策略主要是看NioEventLoop的任务队列内是否有本地任务需要执行，如果有本地任务需要执行，Netty会调用selector.selectNow()非阻塞方法**，
接下来处理selectKeys了，SelectKeys集合表示的是本次选择器筛选出来的就绪状态的Channel，通过迭代SelectKeys集合，处理每一个SelectKey，
从Key中拿到关联的Channel，然后检查就绪的事件是读就绪，还是写就绪。

如果是读事件，那么就会把这个socket缓冲区内的数据load到一个ByteBuf中，调用当前Channel#Pipeline处理管道的传播读（事件的）接口方法，
就是Pipeline#fireChannelRead()方法。就这样从Socket中收到的数据，就进入到Channel的Pipeline中。
在Pipeline中再依次调用InBound接口的Handler处理器，进行读就绪事件的处理。

它不仅可以处理IO事件，也可以处理普通事件。

## 你对Pipeline编解码操作？

Tomcat与Netty一样也在使用，Pipeline是管道的意思，我们可以在这里面创建任意数量的处理单元，当一个事件要进行处理时，它会从第一个处理单元Handler中开始处理，依次向后传递。

**In事件处理，当服务端向客户端发数据**

1. 第一个处理单元就是解码，将数据按照相应的（传输）协议进行解析，如Http协议，将二进制数据转为字符串后，处理Http中的Data数据。
2. 第二个处理单元就是转换数据结构，比如将JSON数据转成业务传输层面的POJO对象。
3. 第三个处理单元就是对加密字段进行解密；
4. 第四个处理单元就是真正业务层面上的BusinessHandler处理了。

**Out事件处理，当客户端向服务端发数据**

1. 第一就是加密Handler，对POJO它需要加密的字段进行加密，然后向外传递到下一个处理器单元。
2. 第二就是转成json字符串。
3. 第三就是编码器，根据传输的协议，将数据按照指定协议格式化；
4. 最后写入到Channel中，将数据刷到Socket写缓冲区内，操作系统会根据TCP连接将数据传输到服务端。

## Netty使用Pipeline模式时做了一些优化，就是向后传递Handler时的优化，你知道做了什么优化么?

netty 5  里多了mask 标识inboud/outboud handler 基类方法有无overwrite,有,mask=1,无,mask=0 ,跳过无响应时间的handler.

## Netty是全异步的框架，你知道它是怎么做到异步处理的吗？

主要是Promise接口，**它是Future接口的增强**，Future接口都很熟悉了比如在使用线程池时，
使用submit提交任务就会返回一个Future句柄，外部线程可以使用Future.get()去获取任务的执行结果，这个方法会阻塞调用线程，直到任务执行完毕或异常。

**原生Channel的缺点不支持异步，原因是它内部没有线程资源，所以没法异步。**

Netty中，作为Promise接口实现ChannelPromise，它内部有一个Channel对象，Netty的Channel对象会注册到一个NioEventLoop上，
同时这个Channel也会持有NioEventLoop对象，EventLoop对象是有线程资源的，所以EventLoop作为ChannelPromise内部对象，
也间接的表示ChannelPromise有线程资源，可以注册一些Listener对象。就表示这个Promise关联任务完成后，接下来要执行的事情。

## 谈谈你对NioEventLoopGroup的理解?

默认情况下Netty线程池最多可以有**当前CPU核心数*2的线程数量**，NioEventLoopGroup和NioEventLoop是池和单个线程的关系，
Group线程池有个接口方法是next()方法，这个方法会反回一个EventLoop线程对象，看源码会发现NioEventLoopGroup这个线程池内部真正的Tread线程，都是延迟创建的。

虽然NioEventLoopGroup会将NioEventLoop创建，但NioEventLoop内部的线程对象不会创建，等到EventLoop对象接收到第一个需要执行任务时，才创建Thread对象。

如果你看NioEventLoop对象的继承关系（SingleThreadEventExecutor 接口），就会发现EventLoop根本不是个单线程，
它更倾向于一个单线程的线程池，EventLoop内有自己的队列，它即处理Selector工作，也处理任务队列内的普通任务。

## Netty服务端boss启2个线程，worker也启2个线程，这时共有几个线程？

在JDK中启动一个JVM查看工具，可以查看到共3个线程，1个boss，2个worker。初始化了1个，第2个EventLoop 在数组中（堆里面的对象）线程未创建。

## NioEventLoop内部的任务队列做过优化，你知道是什么优化么?

1. 将LinkBlockingQueue换成JCTools里的Queue.
2. MpscArrayQueue多生产者单消费者.
3. MpmcArrayQueue多生产者多消费者.
4. 特性CAS锁 ,对customerIndex消费者角标上锁。

## Netty中涉及到的软件涉及模式

Netty的源码中，主要使用了**建造者模式、模版模式、单例模式、工厂模式、责任链模式、观察者模式、策略模式、装饰者模式等设计模式**。

1. **建造者模式**：建造者模式非常简单，通过链式调用来设置对象的属性，在对象属性繁多的场景下非常有用。**在Netty中该模式最直接的使用就是Bootstrap 和ServerBootstrap 两个启动类的链式调用:**

![img.png](../.vuepress/public/interview/netty/netty-建造者模式.png)

2. **模板模式**：所谓模板模式，简单来说，就是抽象父类提供一套定义好的方法供子类调用，而其中的某些方法子类会根据自己的情况而进行定制。就好像我们平时用模板写一些东西，但是内容却各不相同。**Netty中模板模式使用的地方很多，其中最典型的就是各种编解码器的实现**。

3. **单例模式**：单例模式是最常见的设计模式，它可以保证全局只有一个实例，避免线程安全问题。Netty中，NioEventLoop通过核心方法select()不断轮询注册的I/O事件，Netty提供了选择策略SelectStrategy对象，它用于控制select的循环行为，包含CONTINUE、SELECT、BUSY_WAIT三种策略。SelectStrategy对象的默认实现就是使用的饿汉式单例，源码如下:

![img.png](../.vuepress/public/interview/netty/netty-单例设计模式.png)

此外Netty中还有不少饿汉方式实现单例的实践，例如MqttEncoder、ReadTimeoutException等。

4. **工厂模式**：工厂模式封装了对象创建的过程，使用者不需要关心对象创建的细节。在需要生成复杂对象的场景下，都可以使用工厂模式实现。工厂模式分为三种:`简单工厂模式、工厂方法模式和抽象工厂模式`。

Netty在创建Channel的时候使用的就是工厂方法模式，因为服务端和客户端的Channel是不一样的。Netty将反射和工厂方法模式结合在一起，只使用一个工厂类，然后根据传入的Class 参数来构建出对应的Channel，不需要再为每一种Channel 类型创建一个工厂类。具体源码实现如下:

5. **责任链模式**： 提到责任链模式，Netty中我们很容易联想到ChannelPipeline和ChannelHandler，ChannelPipeline内部是由一组ChannelHandler实例组成的，内部通过双向链表将不同的ChannelHandler 实例连接在一起。如下图:

![img.png](../.vuepress/public/interview/netty/netty-channel.png)

6. **观察者模式**： 观察者模式有两个角色:观察者和被观察者。被观察者发布消息，观察者订阅消息，没有订阅的观察者是收不到消息的。

Netty中的ChannelFuture就是观察者模式，为了获取异步操作结果，我们一般会添加监听器到ChannelFuture中，在ChannelFuture执行完毕时会立即通知已经注册的监听器。

7. **策略模式**： 策略模式就是针对同一个问题提供多种策略的处理方式，这些策略之间可以相互替换，在一定程度上提高了系统的灵活性。策略模式非常符合开闭原则，使用者在不修改现有系统的情况下选择不同的策略，而且便于扩展增加新的策略。

Netty在多处地方使用了策略模式，例如EventExecutorChooser提供了不同的策略选择NioEventLoop，newChooser()方法会根据线程池的大小是否是2的幂次，以此来动态的选择取模运算的方式，从而提高性能。EventExecutorChooser源码实现如下所示:

![img.png](../.vuepress/public/interview/netty/netty-策略模式.png)

8. **装饰者模式**：装饰器模式是对被装饰类的功能增强，在不修改被装饰类的前提下，能够为被装饰类添加新的功能特性。当我们需要为一个类扩展功能时会使用装饰器模式，但是该模式的缺点是需要增加额外的代码。
Netty中WrappedByteBuf就是用来装饰ByteBuf的，源码如下:

