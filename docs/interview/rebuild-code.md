---
lang: zh-CN
sidebarDepth: 2
---

# 策略模式和工厂模式重构代码

## 一、问题背景

重构大量的if else代码肯定是要用策略模式的，这个问题主要想知道你是否了解策略模式。可能有一部分人觉得策略模式并不好好，还不如if else看着清晰。
用设计模式重构与否的关键是，每个分支的行数是否过多。如果一个分支的代码行数在几十行，确实没有必要重构。如果一个分支的代码行数在几百行，
上千行（不用和我杠怎么能写出一个分支几百行的代码，是不是能力的问题，因为比较复杂的系统确实有这种情况），

以某个项目对接微信聊天信息为例，有一部分业务逻辑是这样的：

```java
String msgType=mssage.getMsgType();
if("文本".equals(msgType)){
    //do something
}else if("图片".equals(msgType)){
    //do something
}else if("视频".equals(msgType)){
    //do something
}else{
    //do something
}
```

就是根据消息的不同类型有不同的处理策略，每种消息的处理策略代码都很长，如果都放在这种if/else里使得代码逻辑非常臃肿。

## 二、策略模式

### 2.1 策略模式

策略（Strategy）模式的定义：该模式定义了一系列算法，并将每个算法封装起来，使它们可以相互替换，且算法的变化不会影响使用算法的客户。
策略模式属于对象行为模式，它通过对算法进行封装，把使用算法的责任和算法的实现分割开来，并委派给不同的对象对这些算法进行管理。

策略模式的主要优点如下。
* 多重条件语句不易维护，而使用策略模式可以避免使用多重条件语句。
* 策略模式提供了一系列的可供重用的算法族，恰当使用继承可以把算法族的公共代码转移到父类里面，从而避免重复的代码。
* 策略模式可以提供相同行为的不同实现，客户可以根据不同时间或空间要求选择不同的。
* 策略模式提供了对开闭原则的完美支持，可以在不修改原代码的情况下，灵活增加新算法。
* 策略模式把算法的使用放到环境类中，而算法的实现移到具体策略类中，实现了二者的分离。

其主要缺点如下。
* 客户端必须理解所有策略算法的区别，以便适时选择恰当的算法类。
* 策略模式造成很多的策略类。

## 三、重构代码

### 3.1 定义处理接口

其中MessageContext的定义如下：

```java
package com.tang.learn.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageContext {
    private MessageTypeEnum messageType;
    private String messageContent;
}
```

```java
import com.tang.learn.domain.MessageContext;

public interface MessageHandler {
    void handle(MessageContext context);
}
```

MessageTypeEnum的定义如下：

```java
package com.tang.learn.domain;

public enum MessageTypeEnum {
    TEXT,
    IMAGE,
    VIDIO
}
```

### 3.2 编写接口实现类

先编写文本类型消息的处理实现类：

```java
import com.tang.learn.domain.MessageContext;
import com.tang.learn.domain.MessageTypeEnum;
import com.tang.learn.domain.annotation.MessageTypeAnnotation;
import com.tang.learn.handler.MessageHandler;
import org.springframework.stereotype.Component;

@Component
@MessageTypeAnnotation(MessageTypeEnum.TEXT)
public class TextMessageHandler implements MessageHandler {

    @Override
    public void handle(MessageContext context) {
        System.out.println("handle text message :  " + context);
    }
}
```

```java
import com.tang.learn.domain.MessageContext;
import com.tang.learn.domain.MessageTypeEnum;
import com.tang.learn.domain.annotation.MessageTypeAnnotation;
import com.tang.learn.handler.MessageHandler;
import org.springframework.stereotype.Component;

@Component
@MessageTypeAnnotation(MessageTypeEnum.IMAGE)
public class ImageMessageHandler implements MessageHandler {

    @Override
    public void handle(MessageContext context) {
        System.out.println("handle image message :  " + context);
    }
}
```

```java
import com.tang.learn.domain.MessageContext;
import com.tang.learn.domain.MessageTypeEnum;
import com.tang.learn.domain.annotation.MessageTypeAnnotation;
import com.tang.learn.handler.MessageHandler;
import org.springframework.stereotype.Component;

@Component
@MessageTypeAnnotation(MessageTypeEnum.VIDIO)
public class VideoMessageHandler implements MessageHandler {

    @Override
    public void handle(MessageContext context) {
        System.out.println("handle video message :  " + context);
    }
}
```

**(3) 定义（2）中所使用的MessageTypeAnnotation注解**

```java
import com.tang.learn.domain.MessageTypeEnum;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MessageTypeAnnotation {

    /** message type */
    MessageTypeEnum value() default MessageTypeEnum.TEXT;
}
```

**编写消息处理器工厂类**

```java
import com.tang.learn.domain.MessageTypeEnum;
import com.tang.learn.domain.annotation.MessageTypeAnnotation;
import com.tang.learn.handler.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class MessageHandlerFactory {

    private Map<MessageTypeEnum, MessageHandler> handlerMap = new ConcurrentHashMap<>();

    @Autowired
    public MessageHandlerFactory(Map<String, MessageHandler> handlerMaps) {
        handlerMap.clear();

        handlerMaps.keySet().stream().forEach(beanName -> {
                    Class<? extends MessageHandler> clazz = handlerMaps.get(beanName).getClass();
                    MessageTypeAnnotation annotation = clazz.getAnnotation(MessageTypeAnnotation.class);
                    if (null != annotation) {
                        handlerMap.put(annotation.value(), handlerMaps.get(beanName));
                    }
                }
        );
    }

    public MessageHandler getMessageHandler(MessageTypeEnum typeEnum) {
        return handlerMap.get(typeEnum);
    }
}
```

### 3.3 代码测试

```java
package com.tang.learn;

import com.tang.learn.domain.MessageContext;
import com.tang.learn.domain.MessageTypeEnum;
import com.tang.learn.factory.MessageHandlerFactory;
import com.tang.learn.handler.MessageHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LearnSpringApplicationTests {

    @Autowired
    private MessageHandlerFactory handlerFactory;

    @Test
    void testFactory(){
        MessageContext context=new MessageContext(MessageTypeEnum.TEXT,"this is text message");
        MessageHandler handler=handlerFactory.getMessageHandler(context.getMessageType());
        handler.handle(context);
    }
}
```

## 三、策略模式和工厂模式重构代码

策略模式的作用：就是把具体的算法实现从业务逻辑中剥离出来，成为一系列独立算法类，使得它们可以相互替换。
其实这是一种通用的解决方案，当你 if-else/switch-case 的分支超过 3 个、且分支代码相似且冗长的情况下就应该考虑这种模式。