---
lang: zh-CN
sidebarDepth: 2
---

# Zookeeper面试问题

## Zookeeper是什么?

Zookeeper是一个分布式服务框架，是Apache Hadoop的一个子项目，**它主要用来解决分布式应用中经常遇到的一些数据管理问题**，
如：**统一命名服务、状态同步服务、集群管理、分布式应用配置项的管理等**。
或者说Zookeeper是一个拥有`文件系统特点的、运行在内存的分布式数据库`。

1. Zookeeper 作为一个分布式的服务框架，主要用来解决分布式集群中应用系统的一致性问题。ZooKeeper提供的服务包括：分布式消息同步和协调机制、服务器节点动态上下线、统一配置管理、负载均衡、集群管理等
2. ZooKeeper提供基于类似于Linux文件系统的目录节点树方式的数据存储，即分层命名空间。Zookeeper 并不是用来专门存储数据的，它的作用主要是用来维护和监控你存储的数据的状态变化，通过监控这些数据状态的变化，从而可以达到基于数据的集群管理，ZooKeeper节点的数据上限是1MB
3. 我们可以认为Zookeeper=文件系统+通知机制，对于ZooKeeper的数据结构，每个子目录项如 NameService 都被称作为 znode，这个 znode 是被它所在的路径唯一标识，如 Server1 这个 znode 的标识为 /NameService/Server1
4. znode 可以有子节点目录，并且每个 znode 可以存储数据，注意 EPHEMERAL 类型的目录节点不能有子节点目录(因为它是临时节点)
5. znode 是有版本的，每个 znode 中存储的数据可以有多个版本，也就是一个访问路径中可以存储多份数据
6. znode 可以是临时节点，一旦创建这个 znode 的客户端与服务器失去联系，这个 znode 也将自动删除，Zookeeper 的客户端和服务器通信采用长连接方式，每个客户端和服务器通过心跳来保持连接，这个连接状态称为 session，如果 znode 是临时节点，这个 session 失效，znode 也就删除了
7. znode 的目录名可以自动编号，如 App1 已经存在，再创建的话，将会自动命名为 App2
8. znode 可以被监控，包括这个目录节点中存储的数据的修改，子节点目录的变化等，一旦变化可以通知设置监控的客户端，这个是 Zookeeper 的核心特性，Zookeeper 的很多功能都是基于这个特性实现的，后面在典型的应用场景中会有实例介绍

## Zab协议是什么?

Zab协议 的全称是 Zookeeper Atomic Broadcast （Zookeeper原子广播）。Zookeeper 是通过 Zab 协议来保证分布式事务的最终一致性。

Zab协议是为分布式协调服务Zookeeper专门**设计的一种支持崩溃恢复的原子广播协议** ，是Zookeeper保证数据一致性的核心算法。Zab借鉴了Paxos算法，但又不像Paxos那样，是一种通用的分布式一致性算法。它是特别为Zookeeper设计的支持崩溃恢复的原子广播协议。

在Zookeeper中主要依赖Zab协议来实现数据一致性，基于该协议，zk实现了一种主备模型（即Leader和Follower模型）的系统架构来保证集群中各个副本之间数据的一致性。
这里的主备系统架构模型，就是指只有一台客户端（Leader）负责处理外部的写事务请求，然后Leader客户端将数据同步到其他Follower节点。

Zookeeper 客户端会随机的链接到 zookeeper 集群中的一个节点，如果是读请求，就直接从当前节点中读取数据；如果是写请求，那么节点就会向 Leader 提交事务，Leader 接收到事务提交，会广播该事务，只要超过半数节点写入成功，该事务就会被提交。

Zab 协议包括两种基本的模式：**崩溃恢复 和 消息广播**

## Zk的消息广播

1. 在zookeeper集群中，数据副本的传递策略就是采用消息广播模式。zookeeper中农数据副本的同步方式与二段提交相似，但是却又不同。二段提交要求协调者必须等到所有的参与者全部反馈ACK确认消息后，再发送commit消息。要求所有的参与者要么全部成功，要么全部失败。二段提交会产生严重的阻塞问题。
2. Zab协议中 Leader 等待 Follower 的ACK反馈消息是指“只要半数以上的Follower成功反馈即可，不需要收到全部Follower反馈”

**消息广播具体步骤**

1. 客户端发起一个写操作请求。
2. Leader 服务器将客户端的请求转化为事务 Proposal 提案，同时为每个 Proposal 分配一个全局的ID，即zxid。
3. Leader 服务器为每个 Follower 服务器分配一个单独的队列，然后将需要广播的 Proposal 依次放到队列中取，并且根据 FIFO 策略进行消息发送。
4. Follower 接收到 Proposal 后，会首先将其以事务日志的方式写入本地磁盘中，写入成功后向 Leader 反馈一个 Ack 响应消息。
5. Leader 接收到超过半数以上 Follower 的 Ack 响应消息后，即认为消息发送成功，可以发送 commit 消息。
6. Leader 向所有 Follower 广播 commit 消息，同时自身也会完成事务提交。Follower 接收到 commit 消息后，会将上一条事务提交。

zookeeper 采用 Zab 协议的核心，就是只要有一台服务器提交了 Proposal，就要确保所有的服务器最终都能正确提交 Proposal。这也是 CAP/BASE 实现最终一致性的一个体现。
Leader 服务器与每一个 Follower 服务器之间都维护了一个单独的 FIFO 消息队列进行收发消息，使用队列消息可以做到异步解耦。 Leader 和 Follower 之间只需要往队列中发消息即可。如果使用同步的方式会引起阻塞，性能要下降很多。

## Zk崩溃恢复

一旦 Leader 服务器出现崩溃或者由于网络原因导致 Leader 服务器失去了与过半 Follower 的联系，那么就会进入崩溃恢复模式。

在 Zab 协议中，为了保证程序的正确运行，整个恢复过程结束后需要选举出一个新的 Leader 服务器。因此 Zab 协议需要一个高效且可靠的 Leader 选举算法，从而确保能够快速选举出新的 Leader 。

Leader 选举算法不仅仅需要让 Leader 自己知道自己已经被选举为 Leader ，同时还需要让集群中的所有其他机器也能够快速感知到选举产生的新 Leader 服务器。

崩溃恢复主要包括两部分：Leader选举 和 数据恢复

## Zab协议如何保证数据一致性

假设两种异常情况：
1. 一个事务在 Leader 上提交了，并且过半的 Folower 都响应 Ack 了，但是 Leader 在 Commit 消息发出之前挂了。
2. 假设一个事务在 Leader 提出之后，Leader 挂了。

要确保如果发生上述两种情况，数据还能保持一致性，那么 Zab 协议选举算法必须满足以下要求：

Zab 协议崩溃恢复要求满足以下两个要求：
1. 确保已经被 Leader 提交的 Proposal 必须最终被所有的 Follower 服务器提交。
2. 确保丢弃已经被 Leader 提出的但是没有被提交的 Proposal。

根据上述要求

**Zab协议需要保证选举出来的Leader需要满足以下条件：**
1. 新选举出来的 Leader 不能包含未提交的 Proposal 。即新选举的 Leader 必须都是已经提交了 Proposal 的 Follower 服务器节点。
2. 新选举的 Leader 节点中含有最大的 zxid 。这样做的好处是可以避免 Leader 服务器检查 Proposal 的提交和丢弃工作。

**Zab 如何数据同步**

1. 完成 Leader 选举后（新的 Leader 具有最高的zxid），在正式开始工作之前（接收事务请求，然后提出新的 Proposal），Leader 服务器会首先确认事务日志中的所有的 Proposal 是否已经被集群中过半的服务器 Commit。
2. Leader 服务器需要确保所有的 Follower 服务器能够接收到每一条事务的 Proposal ，并且能将所有已经提交的事务 Proposal 应用到内存数据中。等到 Follower 将所有尚未同步的事务 Proposal 都从 Leader 服务器上同步过啦并且应用到内存数据中以后，Leader 才会把该 Follower 加入到真正可用的 Follower 列表中。

Zab 数据同步过程中，如何处理需要丢弃的 Proposal, 在 Zab 的事务编号 zxid 设计中，zxid是一个64位的数字。
其中低32位可以看成一个简单的单增计数器，针对客户端每一个事务请求，Leader 在产生新的 Proposal 事务时，都会对该计数器加1。而高32位则代表了 Leader 周期的 epoch 编号。
epoch 编号可以理解为当前集群所处的年代，或者周期。每次Leader变更之后都会在 epoch 的基础上加1，这样旧的 Leader 崩溃恢复之后，其他Follower 也不会听它的了，因为 Follower 只服从epoch最高的 Leader 命令。
每当选举产生一个新的 Leader ，就会从这个 Leader 服务器上取出本地事务日志充最大编号 Proposal 的 zxid，并从 zxid 中解析得到对应的 epoch 编号，然后再对其加1，之后该编号就作为新的 epoch 值，并将低32位数字归零，由0开始重新生成zxid。
Zab 协议通过 epoch 编号来区分 Leader 变化周期，能够有效避免不同的 Leader 错误的使用了相同的 zxid 编号提出了不一样的 Proposal 的异常情况。

**基于以上策略**

当一个包含了上一个 Leader 周期中尚未提交过的事务 Proposal 的服务器启动时，当这台机器加入集群中，
以 Follower 角色连上 Leader 服务器后，Leader 服务器会根据自己服务器上最后提交的 Proposal 来和 Follower 服务器的 Proposal 进行比对，
比对的结果肯定是 Leader 要求 Follower 进行一个回退操作，回退到一个确实已经被集群中过半机器 Commit 的最新 Proposal。

## Zookeeper保证的是什么一致性?

zookeeper 采用了全局递增的事务 Id 来标识，所有的 proposal（提议）都在被提出的时候加上了 zxid，zxid 实际上是一个 64 位的数字，
高 32 位是 epoch（ 时期; 纪元; 世; 新时代）用来标识 leader 周期，如果有新的 leader 产生出来，epoch会自增，低 32 位用来递增计数。
当新产生 proposal 的时候，会依据数据库的两阶段过程，首先会向其他的 server 发出事务执行请求，
如果超过半数的机器都能执行并且能够成功，那么就会开始执行。

## Zk的两阶段提交?

Zab协议的核心：定义了事务请求的处理方式

1. 所有的事务请求必须由一个全局唯一的服务器来协调处理，这样的服务器被叫做 Leader服务器。其他剩余的服务器则是 Follower服务器。
2. Leader服务器 负责将一个客户端事务请求，转换成一个 事务Proposal，并将该 Proposal 分发给集群中所有的 Follower 服务器，也就是向所有 Follower 节点发送数据广播请求（或数据复制）
3. 分发之后Leader服务器需要等待所有Follower服务器的反馈（Ack请求），在Zab协议中，只要超过半数的Follower服务器进行了正确的反馈后（也就是收到半数以上的Follower的Ack请求），那么 Leader 就会再次向所有的 Follower服务器发送 Commit 消息，要求其将上一个 事务proposal 进行提交。

![img.png](../.vuepress/public/interview/zookeeper/Zk的两阶段.png)

## 什么是会话Session?

指的是客户端会话，客户端启动时，会与服务器建议TCP链接，连接成功后，客户端的生命周期开始，
客户端和服务器通过心跳检测保持有效的的会话以及发请求并响应、监听Watch事件等。

## 在sessionTimeout之内的会话，因服务器压力大、网络故障或客户端主动断开情况下，之前的会话还有效吗？

有效。

## Zookeeper中的快速领导者选举是怎么做的?

**投票信息**
1. logicalclock（electionEpoch）：本地选举周期，每次投票都会自增
2. epoch（peerEpoch）：选举周期，每次选举最终确定完leader结束选举流程时会自增(真正zxid的前32位)
3. zxid：数据ID，每次数据变动都会自增（真正zxid的后32位，zxid一共64位）前32位是epoch，后32位递增序列。
4. sid：该投票信息所属的serverId
5. leader：提议的leader（被提议的server的serverId，即sid）

![img.png](../.vuepress/public/interview/zookeeper/Zookeeper选举.png)

## Zookeeper数据同步原理?

根据这三个参数的大小对比结果，选择对应的数据同步方式。
1. `peerLastZxid`: Learner服务器(Follower或observer)最后处理的zxid。
2. `minCommittedLog`: Leader服务器proposal缓存队列committedLog中的最小的zxid。
3. `maxCommittedLog`: Leader服务器proposal缓存队列committedLog中的最大的zxid。

Zookeeper中数据同步一共有四类，如下：

1. **DIFF:直接差异化同步**： peerlastZxid介于minCommittedLog和maxCommittedLog之间
2. **TRUNC+DIFF:先回滚再差异化同步**： 当Leader服务器发现某个Learner包含了一条自己没有的事务记录,那么就需要让该Learner进行事务回滚到Leader服务器上存在的
3. **TRUNC:仅回滚同步**： peerlastZxid大于maxCommittedLog,Leader会要求Learner回滚到ZXID值为maxCommitedLog对应的事务操作
4. **SNAP:全量同步**： peerLastzxid 小于minCommittedLog，在初始化阶段，Leader服务器会优先初始化以全量同步方式来同步数据learner先向leader注册，上报peerLastZxid

## 什么是脑裂?

简单点来说，脑裂(Split-Brain) 就是比如当你的 cluster 里面有两个节点，它们都知道在这个 cluster 里需要选举出一个 master。
那么当它们两个之间的通信完全没有问题的时候，就会达成共识，选出其中一个作为 master。但是如果它们之间的通信出了问题，
那么两个结点都会觉得现在没有 master，所以每个都把自己选举成 master，于是 cluster 里面就会有两个 master。

1. 假死：由于心跳超时（网络原因导致的）认为leader死了，但其实leader还存活着。
2. 脑裂：由于假死会发起新的leader选举，选举出一个新的leader，但旧的leader网络又通了，导致出现了两个leader ，有的客户端连接到老的leader，而有的客户端则连接到新的leader。

## zookeeper是如何解决"脑裂"问题的？

1. **zooKeeper默认采用了Quorums这种方式来防止"脑裂"现象**。**即只有集群中超过半数节点投票才能选举出Leader**。这样的方式可以确保leader的唯一性,
要么选出唯一的一个leader,要么选举失败。在zookeeper中Quorums作用如下：
    * 集群中最少的节点数用来选举leader保证集群可用。
    * 通知客户端数据已经安全保存前集群中最少数量的节点数已经保存了该数据。一旦这些节点保存了该数据，客户端将被通知已经安全保存了，可以继续其他任务。而集群中剩余的节点将会最终也保存了该数据。
zookeeper除了可以采用上面默认的Quorums方式来避免出现"脑裂"，还可以可采用下面的预防措施：
   
2. **添加冗余的心跳线，例如双线条线，尽量减少“裂脑”发生机会。**
3. 启用磁盘锁。正在服务一方锁住共享磁盘，"裂脑"发生时，让对方完全"抢不走"共享磁盘资源。但使用锁磁盘也会有一个不小的问题，如果占用共享盘的一方不主动"解锁"，另一方就永远得不到共享磁盘。现实中假如服务节点突然死机或崩溃，就不可能执行解锁命令。后备节点也就接管不了共享资源和应用服务。于是有人在HA中设计了"智能"锁。即正在服务的一方只在发现心跳线全部断开（察觉不到对端）时才启用磁盘锁。平时就不上锁了。
4. 设置仲裁机制。例如设置参考IP（如网关IP），当心跳线完全断开时，2个节点都各自ping一下 参考IP，不通则表明断点就出在本端，不仅"心跳"、还兼对外"服务"的本端网络链路断了，即使启动（或继续）应用服务也没有用了，那就主动放弃竞争，让能够ping通参考IP的一端去起服务。更保险一些，ping不通参考IP的一方干脆就自我重启，以彻底释放有可能还占用着的那些共享资源。

## ZooKeeper是否会自动进行日志清理？

如何进行日志清理？zk自己不会进行日志清理，需要运维人员进行日志清理

## 创建的临时节点什么时候会被删除，是连接一断就删除吗？

延时是多少？连接断了之后，ZK不会马上移除临时数据，只有当SESSIONEXPIRED之后，才会把这个会话建立的临时数据移除。
因此，用户需要谨慎设置`Session_TimeOut`

## ZooKeeper是否支持动态进行机器扩容？如果目前不支持，那么要如何扩容呢？

ZooKeeper中的动态扩容其实就是水平扩容，Zookeeper对这方面的支持不太好，目前有两种方式：

1. 全部重启：关闭所有Zookeeper服务，修改配置之后启动，不影响之前客户端的会话
2. 逐个重启：这是比较常用的方式

## ZooKeeper定义了几种权限？
1. CREATE
2. READ
3. WRITE
4. DELETE

## Zookeeper如何实现Leader选举

首先，Zookeeper集群节点由三种角色组成，分别是

1. Leader，负责所有事务请求的处理，以及过半提交的投票发起和决策。
2. Follower，负责接收客户端的非事务请求，而事务请求会转发给Leader节点来处理，另外，Follower节点还会参与Leader选举的投票。
3. Observer，负责接收客户端的非事务请求，事务请求会转发给Leader节点来处理，另外Observer节点不参与任何投票，只是为了扩展Zookeeper集群来分担读操作的压力。

![img.png](../.vuepress/public/interview/others/zookeeper-election.png)

其次，**Zookeeper集群是一种典型的中心化架构**，也就是会有一个Leader作为决策节点，专门负责事务请求的处理和数据的同步。
这种架构的好处是可以减少集群架构里面数据同步的复杂度，集群管理会更加简单和稳定。
但是，会带来Leader选举的一个问题，也就是说，（如图）如果Leader节点宕机了，为了保证集群继续提供可靠的服务，
Zookeeper需要从剩下的Follower节点里面去选举一个新的节点作为Leader，也就是所谓的Leader选举！

![img.png](../.vuepress/public/interview/others/zookeeper-election02.png)

每一个节点都会向集群里面的其他节点发送一个票据Vote，这个票据包括三个属性。

* epoch， 逻辑时钟，用来表示当前票据是否过期。(因为网络通信延迟的可能性，有可能在新一轮的投票里面收到上一轮投票的票据，这种数据应该丢弃，否则会影响投票的结果和效率)
* zxid，事务id，表示当前节点最新存储的数据的事务编号(因为网络通信延迟的可能性，有可能在新一轮的投票里面收到上一轮投票的票据，这种数据应该丢弃，否则会影响投票的结果和效率。)
* myid，服务器id，在myid文件里面填写的数字。(因为网络通信延迟的可能性，有可能在新一轮的投票里面收到上一轮投票的票据，这种数据应该丢弃，否则会影响投票的结果和效率)

每个节点都会选自己当Leader，所以第一次投票的时候携带的是当前节点的信息。接下来每个节点用收到的票据和自己节点的票据做比较，根据epoch、zxid、myid的顺序逐一比较，
以值最大的一方获胜。比较结束以后这个节点下次再投票的时候，发送的投票请求就是获胜的Vote信息。

![img.png](../.vuepress/public/interview/others/zookeeper-election03.png)

然后通过多轮投票以后，每个节点都会去统计当前达成一致的票据，以少数服从多数的方式，最终获得票据最多的节点成为Leader。

## Zookeeper中的Watch机制的原理？

Zookeeper是一个分布式协调组件，为分布式架构下的多个应用组件提供了顺序访问控制能力。它的数据存储采用了类似于文件系统的树形结构，以节点的方式来管理存储在Zookeeper上的数据.

![img.png](../.vuepress/public/interview/others/watch01.png)

Zookeeper提供了一个Watch机制，可以让客户端感知到Zookeeper Server上存储的数据变化，这样一种机制可以让Zookeeper实现很多的场景，比如配置中心、注册中心等。

![img.png](../.vuepress/public/interview/others/watch02.png)

Watch机制采用了Push的方式来实现，**也就是说客户端和Zookeeper Server会建立一个长连接，一旦监听的指定节点发生了变化，就会通过这个长连接把变化的事件推送给客户端。**

Watch的具体流程分为几个部分：

* 首先，是客户端通过指定命令比如exists、get，对特定路径增加watch
* 然后服务端收到请求以后，用HashMap保存这个客户端会话以及对应关注的节点路径，同时客户端也会使用HashMap存储指定节点和事件回调函数的对应关系。
* 当服务端指定被watch的节点发生变化后，就会找到这个节点对应的会话，把变化的事件和节点信息发给这个客户端。
* 客户端收到请求以后，从ZkWatcherManager里面对应的回调方法进行调用，完成事件变更的通知。

![img.png](../.vuepress/public/interview/others/watch03.png)

## Dubbo是如何动态感知服务下线的？

首先，Dubbo默认采用Zookeeper实现服务的注册与服务发现，简单来说啊，就是多个Dubbo服务之间的通信地址，是使用Zookeeper来维护的。
在Zookeeper上，会采用树形结构的方式来维护Dubbo服务提供端的协议地址。

![img.png](../.vuepress/public/interview/others/Dubbo01.png)

Dubbo服务消费端会从Zookeeper Server上去查找目标服务的地址列表，从而完成服务的注册和消费的功能。
Zookeeper会通过心跳检测机制，来判断Dubbo服务提供端的运行状态，来决定是否应该把这个服务从地址列表剔除。

![img.png](../.vuepress/public/interview/others/Dubbo02.png)

当Dubbo服务提供方出现故障导致Zookeeper剔除了这个服务的地址，
那么Dubbo服务消费端需要感知到地址的变化，从而避免后续的请求发送到故障节点，导致请求失败。
也就是说Dubbo要提供服务下线的动态感知能力。这个能力是通过Zookeeper里面提供的Watch机制来实现的

![img.png](../.vuepress/public/interview/others/Dubbo03.png)

简单来说呢，Dubbo服务消费端会使用Zookeeper里面的Watch来针对Zookeeper Server端的/providers节点注册监听，
一旦这个节点下的子节点发生变化，Zookeeper Server就会发送一个事件通知Dubbo Client端.
Dubbo Client端收到事件以后，就会把本地缓存的这个服务地址删除，这样后续就不会把请求发送到失败的节点上，完成服务下线感知。

## Watch机制的实现

正如我们可以通过点击视频网站上的”收藏“按钮来订阅我们喜欢的内容，ZooKeeper 的客户端也可以通过 Watch 机制来订阅当服务器上某一节点的数据或状态发生变化时收到相应的通知，我们可以通过向 ZooKeeper 客户端的构造方法中传递 Watcher 参数的方式实现：
ZooKeeper 客户端也可以通过` getData、exists 和 getChildren `三个接口来向ZooKeeper 服务器注册 Watcher，从而方便地在不同的情况下添加 Watch 事件：


大体上讲 ZooKeeper 实现的方式是通过客服端和服务端分别创建有观察者的信息列表。客户端调用 getData、exist 等接口时，首先将对应的 Watch 事件放到本地的 ZKWatchManager 中进行管理。
服务端在接收到客户端的请求后根据请求类型判断是否含有 Watch 事件，并将对应事件放到 WatchManager 中进行管理。

在事件触发的时候服务端通过节点的路径信息查询相应的 Watch 事件通知给客户端，客户端在接收到通知后，首先查询本地的 ZKWatchManager 获得对应的 Watch 信息处理回调操作。
这种设计不但实现了一个分布式环境下的观察者模式，而且通过将客户端和服务端各自处理 Watch 事件所需要的额外信息分别保存在两端，减少彼此通信的内容。大大提升了服务的处理性能。

## Zk序列化与反序列化原理

ZooKeeper 中并没有采用和 Java 一样的序列化方式，而是采用了一个 Jute 的序列解决方案作为 ZooKeeper 框架自身的序列化方式，
说到 Jute 框架，它最早作为 Hadoop 中的序列化组件。之后 Jute 从 Hadoop 中独立出来，成为一个独立的序列化解决方案。
ZooKeeper 从最开始就采用 Jute 作为其序列化解决方案，直到其最新的版本依然没有更改。

虽然 ZooKeeper 一直将 Jute 框架作为序列化解决方案，但这并不意味着 Jute 相对其他框架性能更好，反倒是 Apache Avro、Thrift 等框架在性能上优于前者。
之所以 ZooKeeper 一直采用 Jute 作为序列化解决方案，主要是新老版本的兼容等问题，这里也请你注意，也许在之后的版本中，
ZooKeeper 会选择更加高效的序列化解决方案。

## Jute在ZooKeeper中的底层实现

Binary 序列化方式，即二进制的序列化方式。正如我们前边所提到的，**采用这种方式的序列化就是将 Java 对象信息转化成二进制的文件格式**。

在 Jute 中实现 Binary 序列化方式的类是 BinaryOutputArchive。该 BinaryOutputArchive 类通过实现 OutPutArchive 接口，
在 Jute 框架采用二进制的方式实现序列化的时候，采用其作为具体的实现类。

Binary 二进制序列化方式的底层实现相对简单，只是采用将对应的 Java 对象转化成二进制字节流的方式。Binary 方式序列化的优点有很多：
无论是 Windows 操作系统、还是 Linux 操作系统或者是苹果的 macOS 操作系统，其底层都是对二进制文件进行操作，
而且所有的系统对二进制文件的编译与解析也是一样的，所有操作系统都能对二进制文件进行操作，跨平台的支持性更好。
而缺点则是会存在不同操作系统下，产生大端小端的问题。

## zk的通信协议是什么？

`基于TCP/IP协议`，**zk实现了自己的通信协议来完成客户端与服务端，服务端与服务端之间的网络通信**，zk的通信协议整体上的设计非常简单，

客户端发起连接,发送握手包进行timeout协商,协商成功后会返回一个session id和timeoout值.随后就可以进行正常通信,通信过程中要在timeout范围内发送ping包. 
zookeeper client和server之间的通信协议基本规则就是发送请求获取响应.并根据响应做不同的动作.

**发送数据格式为:**

1. 消息长度+xid+request. xid每次请求必须是唯一的.消息长度和xid同为4字节,命令长度为4字节且必须为request的开始4字节.
2. 命令是从1到11的数字表示,close的命令为-11.不同类型请求request有些差异
3. 特殊请求具有固定的xid:watch_xid固定为-1,ping_xid为-2,auth_xid固定为-4.普通请求一般从0开始每次请求累加一次xid.

**响应数据为:**

1. 消息长度+header+response.消息长度为4字节,表明header+response的总长度.
2. header为xid,zxid,err对应长度为4,8,4.response根据请求类型不同具有差别
3. 根据header里xid的区别分为watch,ping,auth,data这四种类型
4. 根据这四种类型来区分返回消息是事件,还是认证,心跳和请求数据.client并以此作出不同响应.

## 项目生产集群安装多少？安装多少zk合适？

安装奇数台,生产经验:
1. 10台服务器： 服务器： 3台 zk；
2. 20台服务器： 服务器： 5台 zk；
3. 100台服务器： 服务器： 11台 zk；
4. 200台服务器： 服务器： 11台 zk；

## zookeeper的watch的原理

分布式环境下的观察者模式:通过客服端和服务端分别创建有观察者的信息列表。客户端调用相应接口时，
首先将对应的Watch事件放到本地的ZKWatchManager中进行管理。服务端在接收到客户端的请求后根据请求类型判断是否含有Watch事件，并将对应事件放到WatchManager 中进行管理。
在事件触发的时候服务端通过节点的路径信息查询相应的Watch事件通知给客户端，客户端在接收到通知后，
首先查询本地的ZKWatchManager获得对应的Watch.信息处理回调操作。这种设计不但实现了一个分布式环境下的观察者模式，
而目通过将**客户端和服务端各自处理Watch事件所需要的额外信息分别保存在两端，减少彼此通信的内容。大大提升了服务的处理性能**。

**客户端实现过程:**

1. 标记该会话是一个带有Watch事件的请求。
2. 通过DataWatchRegistration类来保存watcher事件和节点的对应关系。
3. 客户端向服务器发送请求，将请求封装成一个Packet对象，并添加到一个等待发送队列outgoingQueue中。
4. 调用负责处理队列outgoingQueue的SendThread线程类中的readResponse方法接收服务端的回调，并在最后执行finishPacket()方法将Watch注册到ZKWatchManager，sendThread通过发送path路径和watcher为true，到server注册watch事件。

zKWatchManager保存了`Map<String,Set<watcher>> dataWatchers、Map<String,Set<watcher>>existsWatchers、Map<String,Set<watcher>> childrenWatchers`三个集合，
客户端会在dataWatchers 中会添加一个key为 path路径的本地事件

**服务端实现过程:**

1. 解析收到的请求是否带有Watch注册事件，通过FinalRequestProcessor类中的processRequest 函数实现的。当getDataRequest.getWatch()值为True时，表明该请求需要进行Watch监控注册。
2. 将对应的Watch事件存储到WatchManager，通过zks.getZKDatabase().getData函数实现,WatchManger该类中有HashMap<String,HashSet<watcher>> watchTable ,key为path,Watcher是一个客户端网络连接封装，当节点变化时会通知对应的连接(连接通过心跳保持)

**服务端触发过程:**

1. 调用WatchManager中的方法触发数据变更事件
2. 封装了一个具有会话状态、事件类型、数据节点3种属性的WatchedEvent对象。之后查询该节点注册的Watch事件，如果为空说明该节点没有注册过Watch事件。如果存在Watch事件则添加到定义的Wathcers集合中，并在WatchManager管理中删除。最后，通过调用process方法向客户端发送通知

**客户端回调过程:**

1. 使用SendThread.readResponse()方法来统一处理服务端的相应
2. 将收到的字节流反序列化转换成WatcherEvent对象。调用eventThread.queueEvent( ）方法将接收到的事件交给EventThread线程进行处理
3. 从ZKWatchManager中查询注册过的客户端Watch信息。查询到后，会将其从ZKWatchManager的管理中删除。因此客户端的Watcher机制是一次性的，触发后就会被删除+
4. 将查询到的Watcher存储到waitingEvents队列中，调用EventThread类中的run方法循环取出在waitingEvents队列中等待的 Watcher事件进行处理

## zookeeper分布式锁原理

![img.png](../.vuepress/public/interview/zookeeper/zookeeper分布式锁.png)

zk分布式锁实现原理

1. 上来直接创建一个锁节点下的一个接一个的临时顺序节点。
2. 如果自己不是第一个节点，就对自己上一个节点加监听器。
3. 只要上一个节点释放锁，自己就排到前面去了，相当于是一个排队机制。

而且用临时顺序节点，如果某个客户端创建临时顺序节点之后，自己宕机了，zk感知到那个客户端宕机，会自动删除对应的临时顺序节点，
相当于自动释放锁，或者是自动取消自己的排队。解决了惊群效应。

## zookeeper的典型场景

通过对Zookeeper中丰富的数据节点进行交叉使用，配合Watcher事件通知机制，可以非常方便的构建一系列分布式应用中会涉及的核心功能，

1. 数据发布/订阅:配置中心
2. 负载均衡:提供服务者列表
3. 命名服务:提供服务名到服务地址的映射
4. 分布式协调/通知: watch机制和临时节点，获取各节点的任务进度，通过修改节点发出通知
5. 集群管理:是否有机器退出和加入、选举master
6. 分布式锁
7. 分布式队列

第一类，在约定目录下创建临时目录节点，监听节点数目是否是要求的数目。

第二类，和分布式锁服务中的控制时序场景基本原理一致，入列有编号，出列按编号。在特定的目录下创建PERSISTENT_SEQUENTIAL节点，
创建成功时Watcher通知等待的队列，队列删除序列号最小的节点用以消费。此场景下Zookeeper的znode用于消息存储，
znode存储的数据就是消息队列中的消息内容，SEQUENTIAL序列号就是消息的编号，按序取出即可。由于创建的节点是持久化的，
所以不必担心队列消息的丢失问题。

## 请谈谈ZooKeeper对事务性的支持

ZooKeeper对于事务性的支持主要依赖于四个函数，`zoo_create_op_init，zoo_delete_op_init,zoo_set_op_init以及zoo_check_op_init`。
每一个函数都会在客户端初始化一个operation，客户端程序有义务保留这些operations。当准备好一个事务中的所有操作后，
可以使用zoo_multi来提交所有的操作，由zookeeper服务来保证这一系列操作的原子性。也就是说只要其中有一个操作失败了，
相当于此次提交的任何一个操作都没有对服务端的数据造成影响。Zoo_multi的返回值是第一个失败操作的状态信号。

## 简述zk中的观察者作用

```shell
peerType=observer
server. 1: 71ocalhost: 2181:3181:observer
```

**观察者的设计是希望能动态扩展zookeeper集群又不会降低写性能。**

**如果扩展节点是follower，则写入操作提交时需要同步的节点数会变多，导致写入性能下降，而follower又是参与投票的、也会导致投票成本增加。**

observer是一种新的节点类型，解决扩展问题的同时，不参与投票、只获取投票结果，同时也可以处理读写请求，写请求转发给leader。
负责接收leader同步过来的提交数据,observer的节点故障也不会影响集群的可用性，

跨数据中心部署。把节点分散到多个数据中心可能因为网络的延迟会极大拖慢系统。使用observer的话，更新操作都在一个单独的数据中心来处理，
并发送到其他数据中心，让其他数据中心的节点消费数据。

无法完全消除数据中心之间的网络延迟，因为observer需要把更新请求转发到另一个数据中心的leader，并处理同步消息，网络速度极慢的话也会有影响，
它的优势是为本地读请求提供快速响应。

## 简述zk和Eurka的区别？

1. zk:CP设计(强一致性)，目标是一个分布式的协调系统，用于进行资源的统一管理。当节点crash后，**需要进行leader的选举，在这个期间内，zk服务是不可用的**。
2. eureka: **AP设计(高可用)，目标是一个服务注册发现系统，专门用于微服务的服务发现注册**。Eureka各个节点都是平等的，几个节点挂掉不会影响正常节点的工作，剩余的节点依然可以提供注册和查询服务。
而Eureka的客户端在向某个Eureka注册时如果发现连接失败，会自动切换至其他节点，只要有一台Eureka还在，就能保证注册服务可用(保证可用性)，只不过查到的信息可能不是最新的(不保证强一致性)
同时当eureka的服务端发现85%以上的服务都没有心跳的话，它就会认为自己的网络出了问题，就不会从服务列表中删除这些失去心跳的服务，同时eureka的客户端也会缓存服务信息。eureka对于服务注册发现来说是非常好的选择。

## ZooKeeper节点类型？

1. 短暂（ephemeral）：客户端和服务器端断开连接后，创建的节点自己删除
2. 持久（persistent）：客户端和服务器端断开连接后，创建的节点不删除

Znode有四种形式的目录节点（默认是persistent ）

1. 持久化目录节点（PERSISTENT） 客户端与zookeeper断开连接后，该节点依旧存在
2. 持久化顺序编号目录节点（PERSISTENT_SEQUENTIAL） 客户端与zookeeper断开连接后，该节点依旧存在，只是Zookeeper给该节点名称进行顺序编号
3. 临时目录节点（EPHEMERAL） 客户端与zookeeper断开连接后，该节点被删除
4. 临时顺序编号目录节点（EPHEMERAL_SEQUENTIAL） 客户端与zookeeper断开连接后，该节点被删除，只是Zookeeper给该节点名称进行顺序编号

## 请说明ZooKeeper使用到的各个端口的作用？

1. 2888：Follower与Leader交换信息的端口
2. 3888：万一集群中的Leader服务器挂了，需要一个端口来重新进行选举，选出一个新的Leader，而这个端口就是用来执行选举时服务器相互通信的端口

## ZooKeeper使用的ZAB协议与Paxo算法的异同？

Paxos算法是分布式选举算法，Zookeeper使用的 ZAB协议（Zookeeper原子广播），两者的异同如下：
1. 相同之处：比如都有一个Leader，用来协调N个Follower的运行；Leader要等待超半数的Follower做出正确反馈之后才进行提案；二者都有一个值来代表Leader的周期。
2. 不同之处：ZAB用来构建高可用的分布式数据主备系统（Zookeeper），Paxos是用来构建分布式一致性状态机系统

## 能否为临时节点创建子节点？

ZooKeeper中不能为临时节点创建子节点，如果需要创建子节点，应该将要创建子节点的节点创建为永久性节点

## Zookeeper 和 Dubbo 的关系

**Zookeeper的作用：**

zookeeper用来**注册服务和进行负载均衡**，哪一个服务由哪一个机器来提供必需让调用者知道，简单来说就是ip地址和服务名称的对应关系。
当然也可以通过硬编码的方式把这种对应关系在调用方业务代码中实现，但是如果提供服务的机器挂掉调用者无法知晓，
如果不更改代码会继续请求挂掉的机器提供服务。zookeeper通过心跳机制可以检测挂掉的机器并将挂掉机器的ip和服务对应关系从列表中删除。
至于支持高并发，简单来说就是横向扩展，在不更改代码的情况通过添加机器来提高运算能力。通过添加新的机器向zookeeper注册服务，
服务的提供者多了能服务的客户就多了。

**dubbo：**

是管理中间层的工具，**在业务层到数据仓库间有非常多服务的接入和服务提供者需要调度**，dubbo提供一个框架解决这个问题。
注意这里的dubbo只是一个框架，至于你架子上放什么是完全取决于你的，就像一个汽车骨架，你需要配你的轮子引擎。
这个框架中要完成调度必须要有一个分布式的注册中心，储存所有服务的元数据，你可以用zk，也可以用别的，只是大家都用zk。

**zookeeper和dubbo的关系：**

Dubbo 的将注册中心进行抽象，它可以外接不同的存储媒介给注册中心提供服务，有 ZooKeeper，Memcached，Redis 等。

引入了 ZooKeeper 作为存储媒介，也就把 ZooKeeper 的特性引进来。首先是负载均衡，单注册中心的承载能力是有限的，
在流量达到一定程度的时 候就需要分流，负载均衡就是为了分流而存在的，一个 ZooKeeper 群配合相应的 Web 应用就可以很容易达到负载均衡；
资源同步，单单有负载均衡还不 够，节点之间的数据和资源需要同步，ZooKeeper 集群就天然具备有这样的功能；命名服务
，将树状结构用于维护全局的服务地址列表，服务提供者在启动 的时候，向 ZooKeeper 上的指定节点 /dubbo/${serviceName}/providers 目录下写入自己的 URL 地址，
这个操作就完成了服务的发布。 其他特性还有 Mast 选举，分布式锁等。

## ZAB协议与Raft协议比较

ZAB 是通过“一切以领导者为准”的强领导者模型和严格按照顺序处理、提交提案，来实现操作的顺序性的。主节点是基于TCP协议来广播消息的，并保证了消息接收的顺序性。出来的比较早。
Raft协议是Raft协议就是一切以领导者为准的方式，实现一系列值的共识和各节点日志的一致性。通过日志的连续性来保证消息或者说是数据的顺序性以及完整性。Raft协议中日志不仅是数据的载体，同时，日志的完整性还会影响领导者选举的结果（注：选举的因素不仅仅只有日志的完整性来保证，还有任期等其他因素）。

**领导者选举：**

1. ZAB 采用的“**见贤思齐、相互推荐**”的快速领导者选举（Fast Leader Election），节点间通过PK竞争（资本是所持有的信息）看哪个节点更适合做Leader，一个节点PK后，会将选票信息广播出去，最终选举出了大多数节点中数据最完整的节点。
2. Raft 采用的是“**一张选票、先到先得**”的自定义算法（注：里面包含了一个随机等待时间的概念，来保证最多几次选举就能完整选举过程。），这里简单说一下就是一个节点发现leader挂了，就选举自己为leader，然后通知其他节点，其他节点把选票投给第一个通知它的节点。（注：这里其实也会涉及到PK，根据数据的完整性以及任期等信息，如果通知它的节点 没有当前节点的数据完整等那么 当前节点是不会将选票投给该节点，

**以上看来，Raft 的领导者选举，需要通讯的消息数更少，选举也更快。**

**日志复制：**

Raft 和 ZAB 相同，都是以领导者的日志为准来实现日志一致，而且日志必须是连续的，也必须按照顺序提交。

1. ZAB通过`ZXid`来保证操作的顺序性。
2. Raft协议通过`Log Entry`加自己的校验来实现日志的连续性

**读操作和一致性：**

ZAB 的设计目标是操作的顺序性，在 ZooKeeper 中默认实现的是最终一致性，读操作可以在任何节点上执行；
而 Raft 的设计目标是强一致性（也就是线性一致性），所以 Raft 更灵活（可以自己配置），Raft 系统既可以提供强一致性，也可以提供最终一致性，
但是一般为了保证性能，默认提供的也是最终一致性。

**写操作：**

Raft 和 ZAB 相同，写操作都必须在领导者节点上处理。

**成员变更：**

Raft 和 ZAB 都支持成员变更，其中 ZAB 以动态配置（dynamic configuration）的方式实现的。那么当你在节点变更时，不需要重启机器，集群是一直运行的，服务也不会中断。

相比 ZAB，Raft 的设计更为简洁，比如 Raft 没有引入类似 ZAB 的成员发现和数据同步阶段，而是当节点发起选举时，递增任期编号，在选举结束后，广播心跳，直接建立领导者关系，然后向各节点同步日志，来实现数据副本的一致性。
ZAB协议的数据同步的阶段，ZAB集群式无法对外提供服务。

其实，ZAB 的成员发现，可以和领导者选举合到一起，类似 Raft，在领导者选举结束后，直接建立领导者关系，而不是再引入一个新的阶段；数据同步阶段，是一个冗余的设计，可以去除的，因为 ZAB 不是必须要先实现数据副本的一致性，才可以处理写请求，而且这个设计是没有额外的意义和价值的。
还有，ZAB 和 ZooKeeper 强耦合，你无法在实际系统中独立使用；而 Raft 的实现（比如 Hashicorp Raft）是可以独立使用的，编程友好。

## zab、raft的算法的不同

1. zab 用的是 epoch 和 count 的组合来唯一表示一个值, 而 raft 用的是 term 和 index
2. raft 协议数据只有单向地从 leader 到 follower（成为 leader 的条件之一就是拥有最新的 log），而 zab 的 zookeeper 实现中 ，一个 prospective leader 需要将自己的 log 更新为 quorum 里面最新的 log，然后才好在 synchronization 阶段将 quorum 里的其他机器的 log 都同步到一致
3. 选主投票的区别
   1. ZAB 采用的“**见贤思齐、相互推荐**”的快速领导者选举（Fast Leader Election），节点间通过PK竞争（资本是所持有的信息）看哪个节点更适合做Leader，一个节点PK后，会将选票信息广播出去，最终选举出了大多数节点中数据最完整的节点。
   2. Raft 采用的是“**一张选票、先到先得**”的自定义算法（注：里面包含了一个随机等待时间的概念，来保证最多几次选举就能完整选举过程。），这里简单说一下就是一个节点发现leader挂了，就选举自己为leader，然后通知其他节点，其他节点把选票投给第一个通知它的节点。（注：这里其实也会涉及到PK，根据数据的完整性以及任期等信息，如果通知它的节点 没有当前节点的数据完整等那么 当前节点是不会将选票投给该节点，
4. 事务操作的区别
   1. **Raft 系统中的事务消息是通过双向的 RPC 来完成的**，而 Zab 中，则是单向的，一来一回两个消息来完成的。明显 Zab 的性能更好，不需要浪费 leader 一个线程去等待 follower 完成业务操作。
   2. Zab 中 leader 发送一个 Proposal 消息给 follower，发送完成。当 follower 完成 proposal 过程时，再发送一个消息 ACK 到 leader，发送完成。leader 统计 ACK 数量过半时，触发广播 commit。

## zookeeper的脑裂问题与解决方案

脑裂（Split Brain）是指在分布式系统中，节点之间由于网络分区或其他原因无法正常通信，导致系统的一部分被分离成多个独立的子系统，各自运行而互相不受控制。在ZooKeeper中，脑裂问题可能会导致数据不一致和服务不可用。

以下是一些防范和解决ZooKeeper脑裂问题的方案：

1. **部署奇数个节点**：ZooKeeper推荐部署奇数个节点，以确保在网络分区发生时能够保持大多数节点的一致性。奇数个节点的ZooKeeper集群可以容忍少数节点的故障，提高系统的可用性。
2. **使用硬件隔离**：将ZooKeeper节点部署在不同的物理机器或虚拟机上，避免它们被部署在同一个物理机器上。这样可以降低硬件故障导致的脑裂风险。
3. **配置合适的选举算法**：ZooKeeper使用选举算法来选择领导者，确保集群中只有一个领导者。选举算法的配置和调整可以影响系统对网络分区的容忍程度。通常，使用默认配置即可，但在特殊情况下可能需要根据实际需求进行调整。
4. **配置合适的心跳和超时时间**：通过调整心跳时间和会话超时时间来减小脑裂的风险。较短的心跳时间和较短的会话超时时间可以更快地检测到节点的故障，从而更快地进行选举。
5. **使用ZooKeeper的Observer节点**：在某些情况下，可以使用ZooKeeper的Observer节点来提高系统的容错性。Observer节点参与集群的投票，但不参与领导者选举，从而提供更好的容错性。
6. **网络拓扑优化：通过优化网络拓扑**，使用更可靠的网络连接和硬件设备，可以降低网络分区发生的概率，从而减小脑裂的风险。
7. **增加投票节点**：增加ZooKeeper节点的数量可以提高系统的容错性，但需要权衡节点数量和系统的性能。

以上是一些建议和方法，用于减小ZooKeeper脑裂问题的风险。根据具体的应用场景和需求，可能需要综合考虑这些方案，并根据实际情况进行调整。

## Zookeeper通知机制实现IDRAC容器服务自动更新功能？

**怎么实现通知的？**

Zookeeper提供了数据的发布/订阅功能，多个订阅者可同时监听某一特定主题对象，当该主题对象的自身状态发生变化时(例如节点内容改变、节点下的子节点列表改变等)，会实时、主动通知所有订阅者。
客户端首先将Watcher注册到服务端，同时将Watcher对象保存到客户端的Watch管理器中。当ZooKeeper服务端监听的数据状态发生变化时，服务端会主动通知客户端，接着客户端的Watch管理器会触发相关Watcher来回调相应处理逻辑，
从而完成整体的数据发布/订阅流程。

![img.png](../.vuepress/public/cv/zookeeper-watcher.png)

1. 一次性: Watcher是一次性的，一旦被触发就会移除，再次使用时需要重新注册。
2. 客户端顺序回调: Watcher回调是顺序串行化执行的，只有回调后客户端才能看到最新的数据状态。一个Watcher回调逻辑不应该太多，以免影响别的watcher执行
3. 轻量级: WatchEvent是最小的通信单元，结构上只包含通知状态、事件类型和节点路径，并不会告诉数据节点变化前后的具体内容；
4. 时效性: Watcher只有在当前session彻底失效时才会无效，若在session有效期内快速重连成功，则watcher依然存在，仍可接收到通知；

![img.png](../.vuepress/public/cv/Watcher注册流程.png)

![img.png](../.vuepress/public/cv/Watcher通知流程.png)

```java
package com.zhuangxiaoyan.cv;

import java.util.concurrent.CountDownLatch;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

public class ZookeeperSync implements Watcher{

	private static CountDownLatch  countDownLatch  = new CountDownLatch(1);
	private static ZooKeeper zk = null;
	private static Stat stat = new Stat();
	
	public static void main(String[] args) throws Exception {
		// 节点路径
		String path = "/mynode";
		// 连接zookeeper
		zk = new ZooKeeper("10.68.155.114:2181", 5000, new ZookeeperSync());
		countDownLatch.await();
		String str = new String(zk.getData(path, true, stat));
		System.out.println("原始数据为：" + str);
		Thread.sleep(1000);
	}
	
	@Override
	public void process(WatchedEvent event) {
		if (KeeperState.SyncConnected == event.getState()) {
			System.out.println("zk连接成功通知");
			if (EventType.None == event.getType() && null == event.getPath()) {
				countDownLatch.countDown();
			} else if (event.getType() == EventType.NodeDataChanged) {
				try {
					String str = new String(zk.getData(event.getPath(), true, stat));
					System.out.println("数据已修改，新值为：" + str);
				} catch (Exception e) {
					
				}
			}
		}
	}
}
```

**Zk在IDRAC服务通知机制流程**

当某一个IDRAC下面API发生变化，在zookeeper中监听的服务的API和返回数据结果
* 开始向zookeeper中进行API和数据的注册`zkClientWatcher.createPath("/api/infra/cpu/info","{cpu_type："inter",core:2}”)`;
* 当有更新`zkClientWatcher.updateNode("/api/infra/cpu/info","{cpu_type："AMD",core:2}")`; 会触发相关服务进行服务拉取最新的API结果，获取最新返回的数据。
* 由于zookeeper的watcher市一次性的，通过判断事件的类型来判断做什么操作，如果新的API发送改变，如果同时老的数据也没有用了，删除老的节点树，创建新的节点数据，需要重新进行watcher的注册工作，如果是数据发生改变，那就不需要改变了path。
* 触发IDRAC镜像构建job。
* 触发IDRAC新的自动化测试job。
* 将测试结果发送相关开发和测试人员通知邮件相关的人员。

**Zookeeper 对节点的 watch 监听通知是永久的吗？为什么不是永久的?**

不是。官方声明：一个 Watch 事件是一个一次性的触发器，当被设置了 Watch的数据发生了改变的时候，则服务器将这个改变发送给设置了 Watch 的客户端，以便通知它们。
为什么不是永久的，举个例子，如果服务端变动频繁，而监听的客户端很多情况下，每次变动都要通知到所有的客户端
，给网络和服务器造成很大压力。一般是客户端执行 getData(“/节点 A”,true)，如果节点 A 发生了变更或删除，
客户端会得到它的watch事件，但是在之后节点 A 又发生了变更，而客户端又没有设置 watch 事件，就不再给客户端发送。
**在实际应用中，很多情况下，我们的客户端不需要知道服务端的每一次变动，我只要最新的数据即可。**