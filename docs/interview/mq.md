---
lang: zh-CN
sidebarDepth: 2
---

# MQ面试问题

## RockectMQ的原理图

![img.png](../.vuepress/public/interview/mq/RockectMQ.png)

![img.png](../.vuepress/public/interview/mq/rocketmq2.png)

## NameServer的设计原理

* https://javap.blog.csdn.net/article/details/120027043

NameServer的两大职责：路由管理、服务注册与发现。它就像微服务架构中的【注册中心】，类似于Zookeeper，但是它比Zookeeper更加的轻量级。Zookeeper是强一致性的，效率较低，
RocketMQ的架构设计决定了NameServer不需要那么复杂的功能，因此阿里设计了一款足够轻量的NameServer。

NameServer支持集群部署，但是节点之间不会有任何通信和数据同步，每个节点都是无状态的。Broker启动后，会向所有的NameServer注册自己的路由信息，
因此每一个NameServer节点都会有一份完整的数据。当某个NameServer下线了，客户端仍然可以动态感知到服务端的存在。

在NameServer的设计中，Broker信息、路由信息都是保存在内存中的，虽然也支持持久化存储，但是没什么必要。**NameServer还有一个组件KVConfigManager**，
它用来管理键值对类型的配置数据，它是会持久化存储的，**持久化目录为home/namesrv/kvConfig.json**，通过文件名就知道，它通过JSON的方式来管理数据，
每次数据的变动都会自动持久化到磁盘，服务启动后会自动调用load()方法从磁盘加载到内存。

要想启动NameServer，只需要运行bin目录下的mqnamesrv脚本。查看这个脚本文件，它的最后一行命令为

`sh ${ROCKETMQ_HOME}/bin/runserver.sh org.apache.rocketmq.namesrv.NamesrvStartup $@`

它会执行/bin/runserver.sh脚本，启动NamesrvStartup类。因此要分析NameServer的启动流程，以NamesrvStartup类的main方法为入口就好了。
NameServer是一个服务端，它需要对外提供网络服务，供客户端请求。RocketMQ的网络通信层使用Netty框架来实现，

NameServer涉及到的几个比较重要的类如下：

1. NamesrvStartup：服务启动类，帮助读取配置，创建Controller并启动服务。
2. NamesrvController：核心类，负责服务的初始化、启动和停止。
3. KVConfigManager：KV配置信息管理，支持持久化。
4. RouteInfoManager：管理路由信息。
5. NettyRemotingServer：Netty服务端，处理客户端请求。
6. DefaultRequestProcessor：客户端请求处理器。

![img.png](../.vuepress/public/interview/mq/NameServer的设计原理.png)

**NameServer是RocketMQ的名称服务，它是一个非常简单的Topic路由注册中心。它主要的职责就两个：**

1. 管理Broker，检测Broker的存活状态，服务注册与发现。
2. 为Producer和Consumer提供Topic路由服务，从而进行消息的生产和投递。

NameServer是无状态的，可以集群化部署，但是彼此之间不通信，不会同步数据，Broker会向所有的节点注册Topic路由数据，只要有一个节点存活，就不影响RocketMQ的运行。

## NameServer的详细流程

1. NameServer互相独立，彼此没有通信关系，单台NameServer挂掉，不影响其他NameServer。
2. NameServer不去连接别的机器，不主动推消息(相互独立，数据不做同步和一致性校验)。
3. 单个Broker（Master、Slave）与所有NameServer进行定时注册，以便告知NameServer自己还活着。
4. Broker每隔30秒向所有NameServer发送心跳，心跳包含了自身的topic配置信息。
5. NameServer每隔10秒，扫描所有还存活的broker连接，如果某个连接的最后更新时间与当前时间差值超过2分钟，则断开此连接，
   NameServer也会断开此broker下所有与slave的连接。同时更新topic与队列的对应关系，但不通知生产者和消费者。
6. Broker slave同步或者异步从Broker master上拷贝数据。
7. Consumer随机与一个NameServer建立长连接，如果该NameServer断开，则从NameServer列表中查找下一个进行连接。
8. Consumer主要从NameServer中根据Topic查询Broker的地址，查到就会缓存到客户端，并向提供Topic服务的Master、Slave建立长连接，且定时向Master、Slave发送心跳。
9. 如果Broker宕机，则NameServer会将其剔除，而Consumer端的定时任务MQClientInstance.this.updateTopicRouteInfoFromNameServer每30秒执行一次，
   将Topic对应的Broker地址拉取下来，此地址只有Slave地址了，此时Consumer从Slave上消费。消费者与Master和Slave都建有连接，在不同场景有不同的消费规则。
10. Producer随机与一个NameServer建立长连接，每隔30秒（此处时间可配置）从NameServer获取Topic的最新队列情况，
    如果某个Broker Master宕机，Producer最多30秒才能感知，在这个期间，发往该broker master的消息失败。Producer向提供Topic服务的Master建立长连接，且定时向Master发送心跳。
11. 生产者与所有的master连接，但不能向slave写入。客户端是先从NameServer寻址的，得到可用Broker的IP和端口信息，然后据此信息连接broker。

**NameServer在RocketMQ中的作用：**

1. NameServer用来保存活跃的broker列表，包括Master和Slave 。
2. NameServer用来保存所有topic和该topic所有队列的列表。
3. NameServer用来保存所有broker的Filter列表。
4. 命名服务器为客户端，包括生产者，消费者和命令行客户端提供最新的路由信息。

## 为什么不使用Zookeeper

RocketMQ为什么不使用ZooKeeper而自己开发NameServer？ 在服务发现领域，ZooKeeper根本就不能算是最佳的选择。

**注册中心是CP还是AP系统?**

1. 在分布式系统中，即使是对等部署的服务，因为请求到达的时间，硬件的状态，操作系统的调度，虚拟机的GC等，任何一个时间点，这些对等部署的节点状态也不可能完全一致，而流量不一致的情况下，只要注册中心在A承诺的时间内（例如1s内）将数据收敛到一致状态（即满足最终一致），
   流量将很快趋于统计学意义上的一致，所以注册中心以最终一致的模型设计在生产实践中完全可以接受。
2. 分区容忍及可用性需求分析实践中，注册中心不能因为自身的任何原因破坏服务之间本身的可连通性，这是注册中心设计应该遵循的铁律！
3. 在CAP的权衡中，注册中心的可用性比数据强一致性更宝贵，所以整体设计更应该偏向AP，而非CP，数据不一致在可接受范围，而P下舍弃A却完全违反了注册中心不能因为自身的任何原因破坏服务本身的可连通性的原则。
4. 服务规模、容量、服务联通性，当数据中心服务规模超过一定数量，作为注册中心的ZooKeeper性能堪忧。在服务发现和健康监测场景下，随着服务规模的增大，无论是应用频繁发布时的服务注册带来的写请求，还是刷毫秒级的服务健康状态带来的写请求，还是不能整个数据中心的机器或者容器皆与注册中心有长连接带来的连接压力上，ZooKeeper很快就会力不从心，而ZooKeeper的写并不是可扩展的，不可以通过加节点解决水平扩展性问题。
5. 服务健康检查，使用ZooKeeper作为服务注册中心时，服务的健康检测绑定在了ZooKeeper对于Session的健康监测上，或者说绑定在TCP长链接活性探测上了。ZK与服务提供者机器之间的TCP长链接活性探测正常的时候，该服务就是健康的么？答案当然是否定的！注册中心应该提供更丰富的健康监测方案，服务的健康与否的逻辑应该开放给服务提供方自己定义，而不是一刀切搞成了TCP活性检测！
   健康检测的一大基本设计原则就是尽可能真实的反馈服务本身的真实健康状态，否则一个不敢被服务调用者相信的健康状态判定结果还不如没有健康检测。
6. 如果注册中心（Registry）本身完全宕机了，服务调用链路应该受到影响么？不应该受到影响。服务调用（请求响应流）链路应该是弱依赖注册中心，必须仅在服务发布，机器上下线，服务扩缩容等必要时才依赖注册中心。
   这需要注册中心仔细的设计自己提供的客户端，客户端中应该有针对注册中心服务完全不可用时做容灾的手段，例如设计客户端缓存数据机制就是行之有效的手段。另外，注册中心的健康检查机制也要仔细设计以便在这种情况不会出现诸如推空等情况的出现。
   ZooKeeper的原生客户端并没有这种能力，所以利用ZooKeeper实现注册中心的时候我们一定要问自己，如果把ZooKeeper所有节点全干掉，你生产上的所有服务调用链路能不受任何影响么？

熟悉阿里巴巴技术体系的都知道，其实阿里巴巴维护了目前国内最大规模的ZooKeeper集群，整体规模有近千台的ZooKeeper服务节点。
**在粗粒度分布式锁，分布式选主，主备高可用切换等不需要高TPS支持的场景下有不可替代的作用**，而这些需求往往多集中在**大数据、离线任务等相关的业务领域**，
因为大数据领域，讲究分割数据集，并且大部分时间分任务多进程/线程并行处理这些数据集，但是总是有一些点上需要将这些任务和进程统一协调，这时候就是ZooKeeper发挥巨大作用的用武之地。
但是在交易场景交易链路上，在主业务数据存取，大规模服务发现、大规模健康监测等方面有天然的短板，应该竭力避免在这些场景下引入ZooKeeper，
在阿里巴巴的生产实践中，应用对ZooKeeper申请使用的时候要进行严格的场景、容量、SLA需求的评估。总体来说，**对于ZooKeeper，大数据使用，服务发现不用。**

## Producer消息发送流程分析

MQProducer是RocketMQ提供的生产者接口，默认实现为DefaultMQProducer，如果要发送事务消息，对应的实现类为TransactionMQProducer。

DefaultMQProducer仅仅是RocketMQ暴露给用户使用的外观类，它内部持有一个生产者实现DefaultMQProducerImpl，它才是具体的执行者。
使用这种结构的好处是，RocketMQ在后续版本可以任意更换实现类，这一切对用户而言是零感知的。

RocketMQ基于Netty实现网络传输，Producer要和NameServer、Broker交互，所以它自然也是一个Netty客户端，Producer在启动时，会同时启动Netty客户端，
然后向NameServer拉取Broker信息，向Broker发送心跳。发送消息时，从NameServer拉取Topic路由消息，轮询出一个MessageQueue，再将消息发到关联的Broker上。

![img.png](../.vuepress/public/interview/mq/Producer消息发送流程分析.png)

DefaultMQProducer是RocketMQ提供的生产者外观类，核心实现是DefaultMQProducerImpl，这么做的目的是可以随时更换生产者实现，让用户无感知。
生产者要和Broker通信，因此它也是一个Netty客户端，生产者启动的同时也会启动Netty客户端，然后通过一系列的定时任务去从NameServer拉取消息，以及和Broker发送心跳。

消息发送时，Producer会先对消息做前置校验，校验通过后去NameServer查找Topic对应的路由信息，然后从众多MessageQueue中轮询出一个，
根据MessageQueue对应的BrokerName查找Master主机地址，构建请求头SendMessageRequestHeader和RemotingCommand对象，通过Netty将请求发送给Broker。

## 同步发送、异步发送、单向发送消息

RocketMQ支持3种消息发送方式：**同步（sync）、异步（async）、单向（oneway）**。

1. 单向发送，这个就是发送之后不用接收结果的，就是你发出去一个消息，然后就返回了，就算有结果返回也不会接收了，这是站在消息生产者的角度；
2. 同步发送的话，就是发出去一个消息，这个线程要等着它返回消息发送结果，然后你这个线程再根据这个消息发送结果再做一些业务操作等等(保证消息可靠发送到达MQ)；
3. 异步发送，这个就是在你发送消息之前要给一个callback，发送的时候，你这个线程就不用等着，该干什么就干什么，然后发送结果回来的时候，
   是由其他线程调用你这个callback来处理的，你可以把这个callback看作是一个回调函数，回调方法，这个方法里面的业务逻辑就是你对这个消息发送结果的处理。 采用事件监听方式接受broker返回的确认。

## RocketMQ的broker的启动分析

![img.png](../.vuepress/public/interview/mq/broker的启动分析.png)

## Broker消息高性能存储分析

* https://javap.blog.csdn.net/article/details/120140051

RocketMQ网络通信协议被封装成Java对象**RemotingCommand**，消息发送也是一个请求，对应的请求头为SendMessageRequestHeader，
头信息里就标明了消息是由哪个Group生产的、要发到哪个Topic下、消息属性是什么等等，这里就不展开了。**总之，客户端要发送消息，必须构建RemotingCommand**，再通过Netty客户端将请求发送到Broker，


Broker服务启动的同时，会启动Netty服务端来处理客户端的请求，当对应的客户端Channel有事件时，会通过NettyServerHandler进行处理，因为是处理客户端的请求命令
，于是会调用processRequestCommand方法，根据客户端的RequestCode去匹配请求处理器NettyRequestProcessor，消息发送有两个RequestCode，
分别是10和310，对应的常量为SEND_MESSAGE和SEND_MESSAGE_V2，从名字就可以看出来只是版本不同而已，V2针对属性名做了简化，
FastJson序列化的性能会更好一些，区别不大，对应的Processor都是SendMessageProcessor。

RocketMQ的一大特点就是海量的消息积压能力，支持亿级别的消息积压，还能保证性能不受太大影响，这要归功于RocketMQ精心设计的存储系统。

要做到海量的消息积压，首先可以肯定的是，消息不能直接存储在内存中，一个是内存空间有限，其次是面对如此庞大的数据，不可能做到近实时的持久化，
断电数据就丢了是不能被接受的。所以，RocketMQ直接把消息存储到磁盘。

**「磁盘IO效率慢」的概念已经深入人心，每次消息发送都要写入磁盘，那岂不是性能极差？RocketMQ如何保证高性能呢？**

![img.png](../.vuepress/public/interview/mq/消息存储设计.png)

![img.png](../.vuepress/public/interview/mq/消息存储设计2.png)

其实，磁盘如果利用的好，它的效率比你想象的要快得多。磁盘随机写的效率确实很差，只有100KB每秒的写入速度，但是对于顺序写，在Page Cache的加持下，
它的写入速度能达到600MB每秒，这已经超过了绝大多数网卡的读写速度了，所以只要能保证顺序写，磁盘IO并不是性能瓶颈。
这里稍微提下，Page Cache是Linux系统的高速页缓存，一个页大小是4KB，它是用来提高磁盘IO效率的。当你要从磁盘读数据时，
它会从内存中分配一个Page，然后将磁盘数据读入Page Cache，当你下次再次读取时，就能命中缓存，不用再读磁盘了。
写数据也是一样，先写到Page Cache，然后将该页置为「Dirty」脏的，然后由系统统一将这些脏页数据刷盘。因此，
在Page Cache的加持下，磁盘顺序写的效率几乎等于操作内存。

RocketMQ存储消息，主要涉及到三大类文件，分别是：CommitLog、ConsumerQueue、Index。

CommitLog存储Broker上所有的消息，不管你是哪个Topic下的，全部写到CommitLog文件，因此它是完全的顺序写。

ConsumerQueue是RocketMQ用来加速消费者消费消息的索引文件，每个Topic是一个文件夹，下面再以QueueID分片存储，消息写入到CommitLog后，
还要往对应的ConsumerQueue文件写入一个索引信息，它也是顺序写的。

Index是RocketMQ用来实现消息查询的索引文件，有了它就可以通过Key和时间范围快速查询消息，同样的，消息写入到CommitLog后，也会往Index中写入索引数据，也是顺序写的。

**综上所属，RocketMQ消息存储涉及的主要文件，全是顺序写的，这便保证了Broker消息存储的高性能。**

## 内存映射与MMAP零拷贝技术

![img.png](../.vuepress/public/interview/mq/MMAP零拷贝技术.png)

以前，我们从磁盘读写数据时，均需要经过至少两次数据拷贝。
读：磁盘 > 内核缓冲区 > JVM内存。
写：JVM内存 > 内核缓冲区 磁盘。

![img.png](../.vuepress/public/interview/mq/磁盘读写数据时.png)

而内存映射技术，不管是读还是写，均只需要一次数据拷贝。
读：磁盘 > 内核缓冲区。
写：内核缓冲区 > 磁盘。

用户空间直接拿应用程序的逻辑内存地址映射到Linux系统的内核缓冲区，这样应用程序看似读写的是自己的内存，其实读写的是内核缓冲区，
数据不用在内核空间和用户空间来回拷贝了，不仅减少了内存复制的开销，还避免了因系统调用引起的软中断。

![img_1.png](../.vuepress/public/interview/mq/MQ-MMAP零拷贝技术.png)


「零拷贝」是提升IO效率的终极利器，以前，如果我们需要把磁盘中的数据发送到网络，至少需要经过4次数据拷贝：磁盘 > 内核缓冲区 > JVM > Socket缓冲区 > 网卡。

![img.png](../.vuepress/public/interview/mq/零拷贝.png)

利用内存映射，最多只需要三次数据拷贝，数据直接从内核缓冲区拷贝到Socket缓冲区就可以直接发送了。实际上，
可能连内核缓冲区拷贝到Socket缓冲区的过程都没有了，内核缓冲区和Socket缓冲区也可以建立内存映射，这样就只剩下两次数据拷贝了。

![img.png](../.vuepress/public/interview/mq/MQ-内存映射.png)

零拷贝是站在内存的角度来看，数据没有在内存中发生复制行为。数据从磁盘拷贝到内核缓冲区，再拷贝到网卡，期间数据没有在内存中复制过，所以叫「零拷贝」。

综上所述，零拷贝的核心是内存映射，**内存映射技术在Linux系统上对应的是mmap系统函数，在Java中对应的就MappedByteBuffer类**。
**mmap函数有一个缺陷，就是对映射的文件大小有限制，因此CommitLog单个文件默认为1GB。**

## 提升同步双写性能的CompletableFuture

![img.png](../.vuepress/public/interview/mq/CompletableFuture.png)

* produce发送数据Broker启动一个线程来处理阻塞等结果，返回给produce。
* CompetableFuture异步调用:不会阻塞线程。启动一个线程来处理（接收到对应的数据了)不会阻塞启动一个子线程。拿到结果对应。

## Commitlog写入时使用可重入锁还是自旋锁?

* 自旋谈(CPU CAS机制运行)不会有上下文切换。
* 可重入锁:有可能阻塞线程。发生上下文切换。

![img.png](../.vuepress/public/interview/mq/下文切换.png)

![img.png](../.vuepress/public/interview/mq/CAS机制.png)

**使用自旋锁。异步刷盘建议使用自旋锁。同步刷盘建议使用重入锁。**

## 数据读写分离之堆外内存机制

![img.png](../.vuepress/public/interview/mq/堆外内存机制.png)

## RocketMQ的同步双写机制与异步刷盘

![img.png](../.vuepress/public/interview/mq/Rocket的同步双写机制.png)

RocketMQ高性能存储的最后一个**利器就是「异步刷盘」，与之对应的就是「同步刷盘」。**

同步刷盘：消息写入Page Cache后调用系统函数fsync将数据同步到磁盘才给客户端返回ACK响应，这种方式对数据的安全性很高，但是性能会有较大影响。

异步刷盘：充分利用Page Cache的优势，只要消息写入Page Cache就给客户端返回ACK响应，RocketMQ会在后台起一个线程异步刷盘，极大的提高了性能和吞吐量。

![img.png](../.vuepress/public/interview/mq/异步刷盘.png)

**RocketMQ支持海量的消息积压，是因为它直接将消息写入到磁盘。同时，为了解决磁盘IO速度慢的问题，RocketMQ做了大量的优化。
顺序写、零拷贝、异步刷盘是RocketMQ高性能存储的三大利器，消息生产时，CommitLog、ConsumerQueue、Index都是顺序写的，
消息消费时，ConsumerQueue是顺序读的，但是CommitLog是随机读的。**

**因为mmap对文件大小有限制，所以RocketMQ的很多文件都做了定长切分处理。CommitLog单个文件定长为1G，如果写满会创建新的文件继续写。**

从整体流程上看，消息的存储并不复杂，Broker接收到客户端的请求后，从请求体中读取数据并构建内部消息对象MessageExtBrokerInner，
然后按照存储格式将消息写入到CommitLog。当然了，这中间还有大量的逻辑处理，包括：事务消息、定时消息、死信消息等等。

Broker收到消息后先写入CommitLog，后台会起一个线程ReputMessageService，对CommitLog中的消息进行重放，重放的时候进行ConsumerQueue和Index的索引构建。

![img.png](../.vuepress/public/interview/mq/Broker消息存储.png)

## ConsumeQueue构建过程分析

* https://javap.blog.csdn.net/article/details/120167005

理论上来说，RocketMQ只要有CommitLog文件就可以正常运行了，那为何还要维护ConsumeQueue文件呢？

ConsumeQueue是消费队列，引入它的目的是为了提高消费者的消费速度。毕竟RocketMQ是基于Topic主题订阅模式的，
消费者往往只关心自己订阅的消息，如果每次消费都从CommitLog文件中检索数据，无疑性能是非常差的。有了ConsumeQueue，
消费者就可以根据消息在CommitLog文件中的偏移量快速定位到消息进行消费了。

Broker会将客户端发送的消息写入CommitLog文件，持久化存储。但是整个流程并没有涉及到ConsumeQueue文件的操作，
那么ConsumeQueue文件是如何被构建的呢？

**ReputMessageService是「消息重放服务」，请允许我这么命名。Broker在启动的时候，会开启一个线程每毫秒执行一次doReput()方法。**
* 它的目的就是对写入CommitLog文件里的消息进行「重放」，它有一个属性reputFromOffset，记录的是消息重放的偏移量，MessageStore启动的时候会对其进行赋值。
* 它的工作原理是，根据重放偏移量reputFromOffset去读取CommitLog里的待重放的消息，并构建DispatchRequest对象，
* 然后将DispatchRequest对象分发出去，交给各个CommitLogDispatcher处理。

MessageStore维护了CommitLogDispatcher对象集合，目前只有三个处理器：

1. CommitLogDispatcherBuildConsumeQueue：构建ConsumeQueue索引。
2. CommitLogDispatcherBuildIndex：构建Index索引。
3. CommitLogDispatcherCalcBitMap：构建布隆过滤器，加速SQL92过滤效率。

![img.png](../.vuepress/public/interview/mq/ConsumeQueue构建.png)

ConsumeQueue是RocketMQ用来加速消费者消费效率的索引文件，它是一个逻辑消费队列，并不保存消息本身，只是一个消息索引。
索引长度为20个字节，记录了消息在CommitLog文件里的偏移量，消息长度，和消息Tag的哈希值。
Consumer消费消息时可以根据Tag哈希值快速过滤消息，然后根据偏移量快速定位到消息，再根据消息长度读取出一条完整的消息。

Broker将消息写入CommitLog后并不会马上写ConsumeQueue，而是由一个异步线程ReputMessageService将消息进行重放，
重放的过程中由CommitLogDispatcherBuildConsumeQueue将消息构建到ConsumeQueue文件，构建的频率为1毫秒一次，
几乎是近实时的，不用担心消费会延迟。

## Index构建过程分析

Broker会把Producer发送的消息写入到CommitLog，理论上来说，RocketMQ只要有CommitLog文件就可以正常运行了。
构建额外的ConsumeQueue是为了加速消费者的消费效率，构建Index文件的目的又是什么呢？

Index文件是否存在，都不影响RocketMQ生产者和消费者的正常运行，它的目的仅仅是提高消息查询的效率。
如果我们要根据Key或时间段查询消息，通过CommitLog去检索无疑性能是非常差的，通过空间换时间，往CommitLog写入消息后，
再往Index文件写入一份索引数据，就可以实现快速查询了。

和ConsumeQueue的构建过程一样，消息写入CommitLog后并不会立马写Index，
而是由异步线程ReputMessageService重放消息时进行构建的。


**索引设计**

索引文件存储路径为$HOME/store/index/{fileName}，fileName以文件创建时的时间戳命名，单个IndexFile是定长的，
大小约400M，一个IndexFile可以保存2000万个索引，IndexFile底层存储结构为哈希+链表的结构，借鉴了HashMap的设计。

**IndexFile**

索引文件，对应的类是org.apache.rocketmq.store.index.IndexFile。索引文件的构成：

![img.png](../.vuepress/public/interview/mq/IndexFile.png)

**IndexHeader**

![img.png](../.vuepress/public/interview/mq/IndexHeader.png)

**索引条目**

单个索引条目是定长的，为20个字节。构成如下：

![img.png](../.vuepress/public/interview/mq/索引条目.png)

链接下一个索引的指针默认值为0，当遇到哈希碰撞时，采用头插法，哈希槽指向最新的索引数据，指针链接下一个索引。
为什么采用头插法？因为对于RocketMQ来说，关心的总是最新的消息。

![img.png](../.vuepress/public/interview/mq/Index构建过程的时序图.png)

Index是RocketMQ用来根据Key和时间范围检索消息的索引文件，它的存在与否并不影响RocketMQ生产者和消费者的正常运行，
如果没有消息检索的需求，可以选择关闭索引功能。

和ConsumeQueue一样，Index索引的构建也是通过**ReputMessageService线程重放消息来完成的**。消息重放时，
根据消息Key计算哈希值，对哈希槽数量进行取模得到槽位下标，采用头插法插入索引数据。

单个索引条目为20个字节，分别是4字节哈希码、8字节消息偏移量、4字节存盘时间差、4字节Next指针。

## Consumer消息拉取和消费流程分析

* https://javap.blog.csdn.net/article/details/120229506

MQConsumer是RocketMQ提供的消费者接口，从接口定义上可以看到，它主要的功能是订阅感兴趣的Topic、注册消息监听器、启动生产者开始消费消息。

消费者获取消息的模式有两种：推模式和拉模式，对应的类分别是DefaultMQPushConsumer和DefaultMQPullConsumer，需要注意的是，**在4.9.0版本，DefaultMQPullConsumer已经被废弃了**。

Push模式下，由Broker接收到消息后主动推送给消费者，实时性较高，但是会增加Broker的压力。
Pull模式下，由消费者主动从Broker拉取消息，主动权在消费者，这种方式更灵活，消费者可以根据自己的消费能力拉取适量的消息。

实际上，Push模式也是通过Pull的方式实现的，消息统一由消费者主动拉取，那如何保证消息的实时性呢？

Consumer和Broker会建立长连接，一旦分配到MessageQueue，就会立马构建PullRequest去拉取消息，在不触发流控的情况下，
不管有没有拉取到新的消息，Consumer都会立即再次拉取，这样就保证了消息消费的实时性。

**如果Broker长时间没有新的消息，Consumer一直拉取，岂不是空转CPU浪费资源？**

Consumer在拉取消息时，会携带参数suspendTimeoutMillis，它表示Broker在没有新的消息时，阻塞等待的时间，默认是15秒。如果没有消息，
Broker等待15秒再返回结果，避免客户端频繁拉取。如果15秒内有新的消息了，立马返回，保证消息消费的时效性。

**DefaultMQPushConsumer**

RocketMQ暴露给开发者使用的基于Push模式的默认生产者类，和DefaultMQProducer一样，它也仅仅是一个外观类，基本没有业务逻辑，几乎所有操作都转交给生产者实现类DefaultMQPushConsumerImpl完成。这么做的好处是RocketMQ屏蔽了内部实现，方便在后续的版本中随时更换实现类，而用户无感知。


**DefaultMQPushConsumerImpl**

默认的基于Push模式的消费者实现类，拥有消费者的所有功能，例如：拉取消息、执行钩子函数、消费者重平衡等等。


**PullAPIWrapper**

调用拉取消息API的包装类，它是Consumer拉取消息的核心类，它有一个方法特别重要pullKernelImpl，是拉取消息的核心方法。它会根据拉取的MessageQueue去查找对应的Broker，然后构建拉取消息请求头PullMessageRequestHeader发送到Broker，然后执行拉取回调，在回调里会通知消费者消费拉取到的消息。


**OffsetStore**

OffsetStore是RocketMQ提供的，用来帮助Consumer管理消费位点(消费进度)的接口，它有两个实现类：LocalFileOffsetStore和RemoteBrokerOffsetStore，
从名字就可以看出来**，一个是将消费进度存储在本地，一个是将消费进度存储在Broker上。LocalFileOffsetStore会将消费进度持久化到本地磁盘，Consumer启动后会从指定目录读取文件，恢复消费进度。**
**RemoteBrokerOffsetStore将消费进度交给Broker管理，Consumer不会存储到文件，没有意义，但是消费消息时会暂存消费进度在内存，然后在拉取消息时上报消费进度，由Broker负责存储。**

什么场景下需要将消费进度存储在本地呢？这和RocketMQ消息消费模式有关，RocketMQ支持两种消息消费模式：**集群消费和广播消费**。
一个ConsumerGroup下可以有多个消费者实例，集群模式下，消息只会投递给其中一个Consumer实例消费，而广播模式下，消息会投递给每个Consumer实例。

![img.png](../.vuepress/public/interview/mq/集群模式消费.png)

![img.png](../.vuepress/public/interview/mq/广播模式消费.png)

综上所述，集群模式下，消费进度由Broker管理，使用RemoteBrokerOffsetStore。广播模式下，因为消息需要被每个Consumer实例消费，每个实例消费的进度是不一样的，因此由实例自己存储消费进度，使用LocalFileOffsetStore。

**ConsumeMessageService**

消费消息的服务，**客户端拉取到消息后，是需要有线程去消费的，因此它是一个线程池，线程数由consumeThreadMin和consumeThreadMax设置，默认线程数为20**。

一个是由当前线程直接消费消息，另一个是提交消费请求ConsumeRequest由线程池去负责调度，一般情况下使用的还是后者。

RocketMQ提供了两个实现类，分别是ConsumeMessageConcurrentlyService和ConsumeMessageOrderlyService，前者用来并发消费消息，后者用来消费有序消息，本文只分析前者。

**PullMessageService**

消息拉取服务，负责从Broker拉取消息，然后提交给ConsumeMessageService消费。它也是一个线程，它的run方法是一个死循环，通过监听阻塞队列来判断是否需要拉取消息。
阻塞队列里存放的就是PullRequest对象，当Consumer实例上线后，会做一次负载均衡，从众多MessageQueue中给自己完成分配，当有新的MessageQueue被分配给自己，
就会创建PullRequest对象提交到阻塞队列，然后PullMessageService就会开始拉取消息，在拉取完成的回调函数中，不管有没有拉取到新的消息，在不触发流控的情况下，都会一直拉取。

消费者实现比生产者实现要复杂的多。整个Consumer的运行可以大致分为四个过程：

1. 消费者启动
2. 消费者组负载均衡
3. 消息的拉取
4. 消息的消费

![img.png](../.vuepress/public/interview/mq/Consumer消息拉取和消费流程分析.png)

Consumer要订阅自己感兴趣的Topic，支持通过子表达式过滤消息，子表达式的类型有两种：TAG和SQL92。不管是哪种表达式，最终都会被解析成订阅关系SubscriptionData对象存储到Map中。

如果使用TAG的方式，会计算出Tag哈希值，Broker在ConsumeQueue索引中记录了Tag哈希，这样就可以根据Tag哈希快速过滤消息了。

订阅完Topic，就是注册消息监听MessageListener，就是一个赋值操作，跳过。以上操作执行完，Consumer就可以启动了，接下来才是重头戏。

1. 外观类启动的时候，会启动消费者实现类DefaultMQPushConsumerImpl，我们直接看它就好。启动主要做了以下事情：
   1. 校验消费者配置
   2. 拷贝订阅关系
   3. 创建MQClientInstance
   4. 设置RebalanceImpl
   5. 创建PullAPIWrapper，消息拉取核心类
   6. 加载消费进度(Local)
   7. 启动ConsumeMessageService
   8. 启动MQClientInstance
   9. 拉取订阅的Topic路由信息
   10. SQL表达式上传到Broker编译
   11. 给Broker发心跳，通知其他Consumer重平衡
   12. 自己重平衡,拉取消息

2. checkConfig方法，会在Consumer启动前做一系列的校验，确保服务满足启动条件，校验的事项有：
   1. 校验GroupName
   2. 校验消费模式:集群/广播
   3. 校验ConsumeFromWhere
   4. 校验开始消费的指定时间
   5. 校验AllocateMessageQueueStrategy
   6. 校验订阅关系
   7. 校验是否注册消息监听
   8. 校验消费线程数
   9. 校验单次拉取的最大消息数
   10. 校验单次消费的最大消息数

**启动前校验通过，说明配置没有问题，具备启动的基本条件。**

3. copySubscription方法会拷贝订阅关系到RebalanceImpl，Consumer在重平衡时需要用到，除了拷贝给定的Topic订阅关系，Consumer还会自动订阅ConsumerGroup的重试队列。

4. 创建客户端实例MQClientInstance，消息拉取核心对象PullAPIWrapper。

5. 前置操作完成后，就可以启动客户端实例了。MQClientInstance启动主要做了以下事情：
   1. 发请求获取NameServerAddr
   2. 启动Netty客户端
   3. 启动各种定时任务
   4. 启动消息拉取服务
   5. 启动重均衡服务
   
客户端如果没有指定NameServer地址，RocketMQ会读取环境变量rocketmq.namesrv.domain，它的期望值是一个URL链接，每隔2分钟发一个请求更新NameServer地址。
在集群环境下，NameServer机器数和IP都是不固定的，通过配置中心下发比硬编码更灵活。

启动重平衡服务RebalanceService，也是一个单独的线程，默认会每隔20秒重新做一次负载均衡，给Consumer重新分配MessageQueue。
例如，TopicA下有4个MessageQueue，此时只有一个消费者实例订阅了，那么这4个MessageQueue都会分配给它消费。过了一会儿，
新的消费者实例上线，此时会做一次重平衡，重新分配，因为有两个消费者实例了，因此每个实例会分配2个MessageQueue。

6. 相关服务启动完成后，Consumer会自动向NameServer拉取订阅的Topic路由信息。

新的Consumer实例启动上线了，会向Broker发送心跳，Broker接收到心跳后，会发命令NOTIFY_CONSUMER_IDS_CHANGED给Group下其它消费者，要求它们重新做一次负载均衡，重新分配MessageQueue。

Consumer的心跳除了告诉Broker自己还活着，还做了一些其他事情：
   1. 如果是新的Consumer实例，通知其他Consumer重新负载均衡。
   2. 告诉Broker自己的订阅关系。

## Consumer负载均衡

在RocketMQ的设计理念中，不管是Producer往队列里发消息，还是Consumer从队列里消费消息，**负载均衡都是在客户端完成的。**

有三种情况会触发Consumer进行重平衡：

1. Consumer服务启动，自身进行一次重平衡。
2. 新的Consumer上下线，Broker主动通知其它Consumer重平衡。
3. RebalanceService每隔20秒触发一次重平衡。

![img.png](../.vuepress/public/interview/mq/Consumer负载均衡.png)

客户端重平衡时，会遍历所有Consumer，根据订阅的Topic进行重平衡，所谓的「重平衡」就是将Topic下的MessageQueue重新分配给Consumer消费，分配的算法策略对应接口AllocateMessageQueueStrategy。

根据Topic重平衡的流程是这样的，先获取Topic下所有的MessageQueue，然后获取订阅Topic的所有客户端ID集合，对队列集合和客户端集合排序，根据分配策略给当前Consumer分配MessageQueue。

新的Consumer上线，其它Consumer需要将自己负责消费的MessageQueue分配一点给它。旧的Consumer下线，分配给它的MessageQueue无法被消费了，需要重新分配给其它Consumer。这两种情况，Broker都要及时通知Group下其它所有Consumer执行重平衡服务。

新的Consumer上线是通过心跳的方式通知Broker的，旧的Consumer下线是Broker通过监听Channel关闭事件来感知的，无论何种方式，Broker的处理方式都是一样的，给Group下所有Consumer发送命令NOTIFY_CONSUMER_IDS_CHANGED，Consumer接受到Broker的请求后，会执行重平衡服务。

![img.png](../.vuepress/public/interview/mq/平衡服务.png)

## Consumer消息拉取

新的Consumer实例启动后，它会主动执行一次重平衡操作，给自己分配MessageQueue。此时，它的队列集合是空的，
因此被分配到的MessageQueue一定是新的，那么它就会构建PullRequest去拉取这些新分配的MessageQueue里的消息。

![img.png](../.vuepress/public/interview/mq/Consumer消息拉取的时序图.png)

PullRequest会转交给消费者实现类处理，在拉取消息之前，进行必要的流控处理，流控是用来保护消费者的，当消费者消费能力不够时，拉取速度太快会导致大量消息积压，很可能内存溢出。以下情况会被流控：

1. 消费者是否被暂停？暂停就过1秒再拉取。
2. Queue缓存的消息数是够超过1000？
3. Queue缓存的消息字节数是否超过100MB？
4. 缓存的消息位点差是否超过2000？

一旦触发流控，默认会过50ms再尝试拉取，这个时间是可配置的。

没有触发流控，则开始消息拉取，Consumer在拉取消息时，会顺便上报消费位点CommitOffset。

## rocketmq消息消费

Consumer拉取到消息后，自然就是消费了，何时会触发消息的消费动作呢？在消息拉取时，会创建PullCallback对象，消息拉取完成后会触发对应的回调，在回调方法里如果拉取到了消息会触发消息消费流程。

![img.png](../.vuepress/public/interview/mq/Consumer消息消费的时序图.png)


RocketMQ消费者的实现要比生产者复杂的多，Consumer的大致工作流程再梳理一下。

新的Consumer实例启动后，会发送心跳给Broker，Broker会下发命令给Group下其它的Consumer实例进行重平衡，当前Consumer自身也会立即执行重平衡，
这样Topic下的MessageQueue就会被重新分配。对于被移除的MessageQueue，Consumer会上报消费位点，然后停止消费其消息。
对于新分配的MessageQueue，Consumer会创建PullRequest对象开始拉取消息，只要拉取了一次，后面就会一直拉取，
直到MessageQueue被剔除。PullCallback是拉取回调，在回调里会对拉取结果做判断，如果拉取到了新的消息，
就会提交ConsumeRequest对象到消费者线程池，线程池开始消费消息。消费成功则记录消费位点，然后定时上报给Broker。
消费失败，则重新发回给Broker，如果发回失败，则5秒后再次提交到消费者线程池等待再次消费。

## Broker消息投递源码分析

Consumer消息拉取流程，Consumer服务启动会做一次重平衡操作，重新分配MessageQueue，对于新分配的MessageQueue会构建对应的PullRequest去拉取消息，此时PullMessageService线程会被唤醒，调用PullAPIWrapper的pullKernelImpl方法开始拉取消息。

拉取消息对应的RequestCode为PULL_MESSAGE，Broker服务接收到Consumer的请求后，会根据RequestCode去查找对应的处理器PullMessageProcessor，然后调用其processRequest方法开始处理请求。

**PullMessageProcessor**

用来处理Consumer消息拉取请求的处理器，在处理请求前它会做一些基础校验，例如：Broker、Topic是否有读权限，服务是否正常运行等等。基础校验通过后，再开始解析请求头，构建SubscriptionData订阅关系，创建MessageFilter过滤消息，从MessageStore检索消息，保存Consumer消费位点，返回结果。

**TopicConfigManager**

Topic配置信息管理器，它用来管理Broker上所有的Topic信息，例如：Topic下的读写队列个数、权限、消息过滤类型、是否是顺序消息等。会定时持久化到store/config/topics.json文件，Broker启动时会读取磁盘文件进行恢复。

**MessageFilter**

消息过滤器，RocketMQ允许Consumer在订阅Topic时给定一个子表达式来过滤消息，表达式类型可以是TAG或SQL92语法。MessageFilter接口有两个方法用来匹配消息是否需要投递给Consumer，isMatchedByConsumeQueue方法在读取ConsumeQueue索引时即可根据TagsHash快速匹配，当然存在哈希碰撞的概率，Consumer会再做一次Tag字符串的匹配。isMatchedByCommitLog方法通过读取CommitLog文件来进行匹配，因为SQL92语法可以根据消息属性来过滤消息，而消息属性是存储在CommitLog中的，因此必须通过该方法来判断。

**DefaultMessageStore**

默认的消息仓库，RocketMQ将消息存储到磁盘，涉及的文件有：CommitLog、ConsumeQueue、Index，这些文件均通过MessageStore进行维护管理。在处理Consumer的拉取请求时，MessageStore会先根据Topic和queueId定位到ConsumeQueue，然后根据拉取位点Offset定位到具体的索引项，索引项的前8个字节记录的是消息在CommitLog文件中的物理偏移量，根据该偏移量即可快速定位到具体的消息。

**ConsumerOffsetManager**

消费者消费位点管理器，用来存储消费者的消费进度，会定时持久化到store/config/consumerOffset.json文件，Broker启动时同样会读取文件恢复数据。Consumer在拉取消息时，会在请求头带上自己的消费位点commitOffset，Broker处理拉取请求时会顺带记录Consumer的消费进度。

![img.png](../.vuepress/public/interview/mq/Broker消息投递.png)

Netty服务端在接收到客户端的数据包后，将字节序列编码为RemotingCommand对象，然后根据RequestCode去匹配对应的处理器，这里是PullMessageProcessor，然后调用方法processRequest开始处理请求。

1. 消息拉取请求的处理过程大致如下：
   1. 解析请求头，创建响应
   2. 校验Broker、Topic读权限
   3. 解析订阅关系，创建MessageFilter
   4. 从MessageStore获取消息结果
   5. 保存Consumer消费位点
   6. 返回结果
2. 知道了Consumer的消息过滤规则，就可以创建MessageFilter了。MessageFilter只是接口

   如果仅使用Tag过滤，方法一即可满足，效率最高，通过ConsumeQueue文件中的索引就可快速判断，不用再读取CommitLog文件。如果使用SQL92语法过滤，因为支持对属性进行过滤，而消息属性是存储在CommitLog文件里的，因此不得不再次读取CommitLog文件才能进行判断，效率较低。

3. 一切准备就绪，通过MessageStore开始检索消息，主要流程如下：

1. 定位到ConsumeQueue
2. 校验拉取的Offset
3. 根据Offset定位ByteBuffer
4. MessageFilter过滤(TagsHash)
5. CommitLog读取消息
6. MessageFilter过滤(SQL92)
7. 返回消息列表

Broker在接收到Consumer的消息拉取请求后，先对自身和对应Topic做权限校验，确保有读取权限，然后根据拉取的Topic和queueId定位到ConsumeQueue文件，
根据拉取位点计算物理偏移量，根据偏移量从具体的ConsumeQueue文件中截取对应的映射文件缓冲区，循环读取索引项，先根据TagsHash进行快速过滤，
然后根据Offset去CommitLog文件读取消息，再根据SQL92语法进行过滤，只有被MessageFilter成功匹配的数据才会返回给客户端。
**在返回消息给客户端之前，Broker还会对Consumer上报的消费位点进行存储。**

## RocketMQ定时消息实现原理分析

定时消息(延迟消息)是RocketMQ比较有用的特性之一，定时消息被发送到Broker后，不会马上投递给Consumer，而是等待特定的时间，然后再投递消费。
应用场景举例：用户下单后，系统锁定库存，如果用户在15分钟内未付款，系统自动取消订单，释放库存让其他用户有购买的机会。这种场景通过延迟消息就可以很轻松的实现。

延迟消息并不支持用户指定任意时间，而是通过设置延迟级别来指定的。RocketMQ最多支持18个延迟级别，每个延迟级别对应的延迟时间可以通过配置messageDelayLevel自定义，默认值为“1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h”。该配置属于Broker，对所有Topic有效！默认的level值为0，代表非延迟消息，超过18按最大值18计算。

如果Broker接收到的是延迟消息，会改写消息Topic为SCHEDULE_TOPIC_XXXX，改写queueId为延迟级别level-1，这样在构建ConsumeQueue的时候，
消息就不会被构建到目标消费队列，消费者暂时也就无法消费这条消息了。然后由ScheduleMessageService服务，定时去扫描延迟队列里的消息，
完成延迟消息的交付。如果消息的交付时间到了，会构建新的Message对象，恢复原来的Topic、queueId等属性，重新写回CommitLog，之后Consumer就可以正常消费这条数据了。

**简单点说，就是Broker先替Producer将这条消息暂存到统一的「延迟队列」中，然后定时去扫描这个队列，将需要交付的消息重新写回到CommitLog。**

![img.png](../.vuepress/public/interview/mq/Broker处理延时消息.png)

RocketMQ对延迟消息的实现原理是，Broker默认会有一个延迟消息专属的Topic，下面有18个队列，每个延迟级别对应一个队列。
如果Broker接收到的是延迟消息，会改写消息的Topic和queueId，将消息暂时统一写入延迟队列中，然后由ScheduleMessageService线程对延迟队进行扫描，
将到期需要交付的消息从CommitLog中读出来，然后恢复消息原本的Topic和queueId等属性，重新写回CommitLog，然后Consumer就可以正常消费了。

## RocketMQ定时消息相关组件

1. DefaultMessageStore： 默认的消息仓库，RocketMQ将消息存储到磁盘，涉及的文件有：CommitLog、ConsumeQueue、Index，这些文件均通过MessageStore进行维护管理，例如：消息写入CommitLog、索引写入ConsumeQueue等。
2. CommitLog： CommitLog用来存储消息主体和其元数据，虽然RocketMQ是基于Topic主题订阅模式的，但是对于Broker而言，所有消息全部写入CommitLog，不关心Topic，因此CommitLog是完全顺序写的。
3. ScheduleMessageService： 定时消息服务，用来处理延迟消息，将需要交付的消息重新写回CommitLog。它内部有一个Timer定时器，通过提交延时任务的方式来工作。
4. DeliverDelayedMessageTimerTask： 交付延时消息的定时任务，它继承自TimerTask，可以提交到Timer调度执行。当ScheduleMessageService需要处理延时消息时，就会创建该对象提交到Timer，当消息还未到交付时间时，会计算倒计时，然后再重新提交一个延时任务。

![img.png](../.vuepress/public/interview/mq/RocketMQ定时消息相关组件.png)

在构建DispatchRequest对象时，有一个点需要注意，如果是延迟消息，**TagsHash存储的不再是Tag字符串的哈希值，而是存储消息的交付时间戳，**
ScheduleMessageService线程会根据这个时间戳判断消息是否需要交付。

然后每隔10秒会对延迟队列的处理进度做一次持久化。

RocketMQ对延迟消息的实现原理是，Broker默认会有一个延迟消息专属的Topic，下面有18个队列，每个延迟级别对应一个队列。
如果Broker接收到的是延迟消息，会改写消息的Topic和queueId，将消息暂时统一写入延迟队列中，然后由ScheduleMessageService线程对延迟队进行扫描，
将到期需要交付的消息从CommitLog中读出来，然后恢复消息原本的Topic和queueId等属性，重新写回CommitLog，然后Consumer就可以正常消费了。

## 定时消息机制

定时消息和延时消息本质相同，都是服务端根据消息设置的定时时间在某一固定时刻将消息投递给消费者消费。
在分布式定时调度触发、任务超时处理等场景，需要实现精准、可靠的定时事件触发。

在分布式定时调度场景下，需要实现各类精度的定时任务，例如每天5点执行文件清理，每隔2分钟触发一次消息推送等需求。传统基于数据库的定时调度方案在分布式场景下，性能不高，实现复杂。
基于 Apache RocketMQ 的定时消息可以封装出多种类型的定时触发器。

![img.png](../.vuepress/public/interview/mq/任务超时处理.png)

以电商交易场景为例，订单下单后暂未支付，此时不可以直接关闭订单，而是需要等待一段时间后才能关闭订单。使用 Apache RocketMQ 定时消息可以实现超时任务的检查触发。

**基于定时消息的超时任务处理具备如下优势：**

1. 精度高、开发门槛低：基于消息通知方式不存在定时阶梯间隔。可以轻松实现任意精度事件触发，无需业务去重。
2. 高性能可扩展：传统的数据库扫描方式较为复杂，需要频繁调用接口扫描，容易产生性能瓶颈。 Apache RocketMQ 的定时消息具有高并发和水平扩展的能力。

定时消息是 Apache RocketMQ 提供的一种高级消息类型，消息被发送至服务端后，在指定时间后才能被消费者消费。通过设置一定的定时时间可以实现分布式场景的延时调度触发效果。

**定时时间设置原则**

* Apache RocketMQ 定时消息设置的定时时间是一个预期触发的系统时间戳，延时时间也需要转换成当前系统时间后的某一个时间戳，而不是一段延时时长。
* 定时时间的格式为毫秒级的Unix时间戳，您需要将要设置的时刻转换成时间戳形式。具体方式，请参见Unix时间戳转换工具。
* 定时时间必须设置在定时时长范围内，超过范围则定时不生效，服务端会立即投递消息。
* 定时时长最大值默认为24小时，不支持自定义修改，更多信息，请参见参数限制。
* 定时时间必须设置为当前时间之后，若设置到当前时间之前，则定时不生效，服务端会立即投递消息。

## 定时消息生命周期

![img.png](../.vuepress/public/interview/mq/定时消息生命周期.png)

1. 初始化：消息被生产者构建并完成初始化，待发送到服务端的状态。
2. 定时中：消息被发送到服务端，和普通消息不同的是，服务端不会直接构建消息索引，而是会将定时消息单独存储在定时存储系统中，等待定时时刻到达。
3. 待消费：定时时刻到达后，服务端将消息重新写入普通存储引擎，对下游消费者可见，等待消费者消费的状态。
4. 消费中：消息被消费者获取，并按照消费者本地的业务逻辑进行处理的过程。 此时服务端会等待消费者完成消费并提交消费结果，如果一定时间后没有收到消费者的响应，Apache RocketMQ会对消息进行重试处理。具体信息，请参见消费重试。
5. 消费提交：消费者完成消费处理，并向服务端提交消费结果，服务端标记当前消息已经被处理（包括消费成功和失败）。 Apache RocketMQ 默认支持保留所有消息，此时消息数据并不会立即被删除，只是逻辑标记已消费。消息在保存时间到期或存储空间不足被删除前，消费者仍然可以回溯消息重新消费。
6. 消息删除：Apache RocketMQ按照消息保存机制滚动清理最早的消息数据，将消息从物理文件中删除。更多信息，请参见消息存储和清理机制。

**消息类型一致性**： 定时消息仅支持在 MessageType为Delay 的主题内使用，即定时消息只能发送至类型为定时消息的主题中，发送的消息的类型必须和主题的类型一致。

**定时精度约束**： Apache RocketMQ 定时消息的定时时长参数精确到毫秒级，但是默认精度为1000ms，即定时消息为秒级精度。
Apache RocketMQ 定时消息的状态支持持久化存储，系统由于故障重启后，仍支持按照原来设置的定时时间触发消息投递。**若存储系统异常重启，可能会导致定时消息投递出现一定延迟**。

## RocketMQ事务消息实现原理分析

![img_1.png](../.vuepress/public/interview/mq/RocketMQ事务流程.png)

**事务消息处理流程**

1. 生产者将消息发送至Apache RocketMQ服务端。
2. Apache RocketMQ服务端将消息持久化成功之后，向生产者返回Ack确认消息已经发送成功，此时消息被标记为"暂不能投递"，这种状态下的消息即为半事务消息。
3. 生产者开始执行本地事务逻辑。
4. 生产者根据本地事务执行结果向服务端提交二次确认结果（Commit或是Rollback），服务端收到确认结果后处理逻辑如下：
   * 二次确认结果为Commit：服务端将半事务消息标记为可投递，并投递给消费者。
   * 二次确认结果为Rollback：服务端将回滚事务，不会将半事务消息投递给消费者。
5. **在断网或者是生产者应用重启的特殊情况下，若服务端未收到发送者提交的二次确认结果，或服务端收到的二次确认结果为Unknown未知状态，经过固定时间后，
服务端将对消息生产者即生产者集群中任一生产者实例发起消息回查。 说明服务端回查的间隔时间和最大回查次数，请参见参数限制**。
6. 生产者收到消息回查后，需要检查对应消息的本地事务执行的最终结果。
7. 生产者根据检查到的本地事务的最终状态再次提交二次确认，服务端仍按照步骤4对半事务消息进行处理。

RocketMQ采用2PC的思想，实现了Producer发送「事务消息」。事务消息的提交分为两个阶段，阶段一，Producer发送半事务(Half)消息到Broker，
Broker存储消息，然后响应消息写入结果，此时消息对Consumer是不可见的。阶段二，Producer根据消息发送结果做对应的处理，
如果消息发送成功，则开始执行本地事务，并提交事务状态给Broker，事务状态有三种，对应的Broker动作为：

1. COMMIT_MESSAGE：事务提交，消息对Consumer可见。
2. ROLLBACK_MESSAGE：事务回滚，丢弃消息。
3. UNKNOW：事务未知，暂不处理，稍后进行回查。

**消息回查是对二阶段的一个补偿机制，因为很可能本地事务执行完了，但是提交事务状态失败。Broker会启动TransactionalMessageCheckService线程，
定时会Half消息进行回查，将消息重新发送给Producer检查本地事务。同时，为了避免消息无限制的回查，
默认最大回查次数为15，超过15次Broker将丢弃该消息，扔到一个特殊的队列中。**

整个事务消息的处理流程，可以分为以下五个步骤：

1. Half消息发送
2. Half消息存储
3. 提交事务状态
4. 处理事务状态
5. 事务回查

![img.png](../.vuepress/public/interview/mq/Half消息发送.png)

## RocketMQ使用限制

**消息类型一致性**: 事务消息仅支持在 MessageType 为 Transaction 的主题内使用，即事务消息只能发送至类型为事务消息的主题中，发送的消息的类型必须和主题的类型一致。

**消费事务性**: Apache RocketMQ 事务消息保证本地主分支事务和下游消息发送事务的一致性，但不保证消息消费结果和上游事务的一致性。因此需要下游业务分支自行保证消息正确处理，建议消费端做好消费重试，如果有短暂失败可以利用重试机制保证最终处理成功。

**中间状态可见性**: Apache RocketMQ 事务消息为最终一致性，即在消息提交到下游消费端处理完成之前，下游分支和上游事务之间的状态会不一致。因此，事务消息仅适合接受异步执行的事务场景。

**事务超时机制**: Apache RocketMQ 事务消息的生命周期存在超时机制，即半事务消息被生产者发送服务端后，如果在指定时间内服务端无法确认提交或者回滚状态，则消息默认会被回滚。事务超时时间，请参见参数限制。

## RocketMQ事务消息生命周期

* 初始化：半事务消息被生产者构建并完成初始化，待发送到服务端的状态。
* 事务待提交：半事务消息被发送到服务端，和普通消息不同，并不会直接被服务端持久化，而是会被单独存储到事务存储系统中，等待第二阶段本地事务返回执行结果后再提交。此时消息对下游消费者不可见。
* 消息回滚：第二阶段如果事务执行结果明确为回滚，服务端会将半事务消息回滚，该事务消息流程终止。
* 提交待消费：第二阶段如果事务执行结果明确为提交，服务端会将半事务消息重新存储到普通存储系统中，此时消息对下游消费者可见，等待被消费者获取并消费。
* 消费中：消息被消费者获取，并按照消费者本地的业务逻辑进行处理的过程。 此时服务端会等待消费者完成消费并提交消费结果，如果一定时间后没有收到消费者的响应，Apache RocketMQ会对消息进行重试处理。具体信息，请参见消费重试。
* 消费提交：消费者完成消费处理，并向服务端提交消费结果，服务端标记当前消息已经被处理（包括消费成功和失败）。 Apache RocketMQ默认支持保留所有消息，此时消息数据并不会立即被删除，只是逻辑标记已消费。消息在保存时间到期或存储空间不足被删除前，消费者仍然可以回溯消息重新消费。
* 消息删除：Apache RocketMQ按照消息保存机制滚动清理最早的消息数据，将消息从物理文件中删除。更多信息，请参见消息存储和清理机制。

## RocketMQ事务回查

![img.png](../.vuepress/public/interview/mq/事务回查.png)

Half消息写入成功，可能因为种种原因没有收到Producer的事务状态提交请求。此时，Broker会主动发起事务回查请求给Producer，以决定最终将消息Commit还是Rollback。

Half消息最终状态有没有被确认，是通过Op队列里的消息判断的。Broker服务启动时，会开启TransactionalMessageCheckService线程，每隔60秒进行一次消息回查。为了避免消息被无限次的回查，RocketMQ通过transactionCheckMax属性设置消息回查的最大次数，默认是15次。

RocketMQ实现事务消息的原理和实现延迟消息的原理类似，**都是通过改写Topic和queueId，暂时将消息先写入一个对Consumer不可见的队列中**，
然后等待Producer执行本地事务，提交事务状态后再决定将Half消息Commit或者Rollback。同时，**可能因为服务宕机或网络抖动等原因，
Broker没有收到Producer的事务状态提交请求，为了对二阶段进行补偿，Broker会主动对未确认的Half消息进行事务回查，判断消息的最终状态是否确认，
是通过Op队列实现的，Half消息一旦确认事务状态，就会往Op队列中写入一条消息，消息内容是Half消息所在ConsumeQueue的偏移量。**

## RocketMQ顺序消息实现原理分析

顺序消息是RocketMQ的特性之一，它可以让Consumer消费消息的顺序严格按照消息的发送顺序来进行。
例如：一条订单产生的三条消息：订单创建、订单付款、订单完成。消费时要按照这个顺序依次消费才有意义，但是不同的订单之间这些消息是可以并行消费的。

顺序消息可以分为全局有序和分区有序，绝大部分场景下，分区有序就已经能够满足需求了，

全局有序：某个Topic下所有的消息都是有序的，所有消息按照严格的先进先出的顺序进行生产和消费，**要求Topic下只能有一个分区队列**，
**且Consumer只能有一个线程消费，适用对性能要求不高的场景**。

分区有序：某个Topic下，所有消息根据ShardingKey进行分区，相同ShardingKey的消息必须被发送到同一个分区队列，
因为队列本身是可以保证先进先出的，此时只要保证Consumer同一个队列单线程消费即可。

RocketMQ里的分区队列MessageQueue本身是能保证FIFO的，正常情况下不能顺序消费消息主要有两个原因：

1. Producer发送消息到MessageQueue时是轮询发送的，消息被发送到不同的分区队列，就不能保证FIFO了。
2. Consumer默认是多线程并发消费同一个MessageQueue的，即使消息是顺序到达的，也不能保证消息顺序消费。

综上所述，RocketMQ要想实现顺序消息，**核心就是Producer同步发送，确保一组顺序消息被发送到同一个分区队列，然后Consumer确保同一个队列只被一个线程消费**。

**MessageQueueSelector**

分区队列选择器，它是一个接口，只有一个select方法，根据ShardingKey从Topic下所有的分区队列中，选择一个目标队列进行消息发送，
必须确保相同ShardingKey选择的是同一个分区队列，常见作法是对队列数取模。

**RebalanceLockManager**

Consumer重平衡操作时，Broker维护的全局锁管理器。Consumer在重平衡时，会开始拉取新分配的MessageQueue里的消息，但是如果是顺序消息，
在拉取消息前，必须向Broker竞争队列锁成功才能拉取。因为，此时MessageQueue很可能还在被其它Consumer实例消费，消费位点还没有上报，
直接拉取会导致消息重复消费、消费顺序错乱。

RebalanceLockManager维护了一个ConcurrentMap容器，里面存放了所有MessageQueue对应的LockEntry对象，
LockEntry记录了MessageQueue锁的持有者客户端ID和最后的更新时间戳，以此来判断MessageQueue的锁状态和锁超时。

**ConsumeMessageOrderlyService**

消费顺序消息服务类，与之对应的还有ConsumeMessageConcurrentlyService消费并发消息服务类，
它俩最大的区别就是ConsumeMessageOrderlyService在获取MessageQueue里的消息并消费之前，会对MessageQueue加锁
，确保同一时间单个MessageQueue最多只会被一个线程消费，因为MessageQueue里的消息是有序的，只要消费有序就能保证最终有序。

**MessageQueueLock**

Consumer用来维护MessageQueue对应的本地锁对象，使用ConcurrentHashMap来管理。确保同一个MessageQueue同一时间最多只会被一个线程消费，
因此线程消费前必须先竞争队列本地锁。通过synchronized关键字来保证同步，因此锁对象就是一个Object对象。

![img.png](../.vuepress/public/interview/mq/Producer发送顺序消息.png)

核心在于重写MessageQueueSelector类，将相同ShardingKey消息发送到同一队列即可。

**Tips：必须使用同步发送，异步/单向发送，无法保证消息被有序写入队列。**

## RocketMQ 消费有序

相较于发送有序，消费有序就复杂的多。下面是Consumer保证消费有序的时序图：

![img.png](../.vuepress/public/interview/mq/MQ-消费分析.png)

顺序消费需要Producer、Broker、Consumer三者一起配合才能正常工作。首先，Producer需要确保相同ShardingKey的消息被发送到同一分区队列中，
因为队列本身是能保证FIFO的，这是基础。然后，Broker需要维护全局MessageQueue的锁状态，Consumer拉取消息前，必须保证竞争锁队列成功，
否则就会导致同一MessageQueue里的消息被多个Consumer实例消费，造成消息重复消费和顺序错乱。最后，Consumer在消费MessageQueue的消息前，
必须确保竞争MessageQueue本地锁成功，同一个MessageQueue同一时间最多只能被一个线程消费。

## RocketMQ批量发送

批量发送消息能显著提高传递小消息的性能。限制是这些批量消息应该有相同的topic，而且不能是延时消息。此外，**这一批消息的总大小不应超过4MB**，如果超过可以有2种处理方案：

1. 将消息进行切割成多个小于4M的内容进行发送
2. 修改4M的限制改成更大
    * 可以设置Producer的maxMessageSize属性
    * 修改配置文件中的maxMessageSize属性
    * 对于消费者而言Consumer的MessageListenerConcurrently监听接口的consumeMessage()方法的第一个参数为消息列 表，但默认情况下每次只能消费一条消息，可以通过：Consumer的pullBatchSize属性设置消息拉取数量(默认32)，可以通过设置consumeMessageBatchMaxSize属性设置消息一次消费数量(默认1)。

[注意]：pullBatchSize 和 consumeMessageBatchMaxSize并不是设置越大越好，一次拉取数据量太大会导致长时间等待，性能降低。而且消息处理失败同一批消息都会失败，然后进行重试，导致消费时长增加。增加没必要的重试次数。

## rocketmq消费模式

消费组之间有两种消费模式：**集群模式**和**广播模式**。

1. 在集群模式下，同一主题下的消息只能被消费组内的某一个消费者处理，一条消息会被 1 个消费组内的 N 个消费者消费 1 次。
2. 在广播模式下，同一主题下的消息将会被消费组内的所有消费者处理一次，一条消息会被 1 个消费组内的 N 个消费者消费 N 次。
3. **如果消息消费是集群模式，那么消息进度保存在Broker上; 如果是广播模式，那么消息消费进度存储在Consumer端本地**。

## rocketMQ生产者端要保证幂等性?

1. RocketMQ支持消息查询的功能，只要去RocketMQ查询一下是否已经发送过该条消息就可以了，不存在则发送，存在则不发送，**但是RocketMQ消息查询的性能不是特别好，如果是在高并发的场景下，每条消息在发送到RocketMQ时都去查询一下，可能会影响接口的性能。**
2. 引入redis，在发送消息到RocketMQ成功之后，向redis中插入一条数据，如果发生重试，则先去redis中查询一下该条消息是否已经发送过了，**但是存在的话就不重复发送消息了.在一些极端的场景下，redis也无法保证消息发送成功之后，就一定能写入redis成功，比如写入消息成功而redis此时宕机，那么再次查询redis判断消息是否已经发送过，是无法得到正确结果的**

生产者的这两种幂等性方案都可以实现，但是都存在一定的缺陷

消息的消费，最后都对应的是数据库的操作，只要在消息消费的时候，判断一下数据库中是否已经消费过了这条消息，就可以保证幂等性了，例如使用唯一索引，保证一条消息只被消费一次。

![img.png](../.vuepress/public/interview/mq/rocketmq幂等性.png)

消息重复消费是一个非常常见的问题，在很多系统调用频繁的场景下，都可能会出现超时重试的情况，还有就是系统频繁迭代，经常重启系统更新的场景，也会出现消息重复消费

生产者端发送重复的消息到RocketMQ中其实问题不大，消息只是在RocketMQ中重复了，并没有影响到系统的数据，我们只需要在最后修改数据库的时候，保证好幂等性即可

## rocketmq消费者怎么拉取消息

整体流程包括：
1. 消费者启动。主要包括订阅Topic、初始化消息进度。
2. 消费者发送拉取请求。主要查询路由表找到目标Broker发送请求。
3. Broker查找并返回消息。根据订阅关系Subscription和 消息进度 进行消息过滤和匹配，然后返回消息。
4. 消费者接收并处理消息。

消息服务器与消费者之间有两种消息传送方式：**推模式和拉模式**。

拉模式是消费者主动向消息服务器请求拉取消息。推模式是消息到达消息服务器后，由服务器主动推送给消息消费者。在 RocketMQ 中，Consumer端的两种消费模式（Push/Pull）底层其实都是基于拉模式来获取消息的。
具体实现方式是，消息拉取线程从服务器拉取一批消息后，将其提交给消息消费线程池，并立即继续向服务器尝试拉取消息，以保持消息的连续性。

**那如果拉取消息时，Broker端暂时没有新消息可以返回怎么办？会一直无脑发送拉取请求吗？**

* RocketMQ默认会开启长轮询机制，这个机制能够平衡轮询压力与新消息的实时性 ：
* 消费者发送拉取请求到Broker，如果没有新消息，Broker会暂时 挂起 请求不返回
* Broker每隔5s检查一次挂起的请求，是否有满足条件的新消息，如果有就返回，如果没有就继续挂起，直到超时返回
* 如果在挂起的过程中，有满足条件的新消息写入commitLog，也会立即返回新消息

**消费者怎么知道去哪里拉取消息？**

消费端的负载均衡是指将Broker端中多个队列queue按照某种算法分配给同一个消费组中的不同消费者，负载均衡是客户端开始消费的起点。

**RocketMQ「队列粒度」的负载均衡的核心设计理念是：**

1. 消费队列在同一时间只允许被同一消费组内的一个消费者消费
2. 一个消费者能同时消费多个消息队列

**负载均衡基本流程：**
1. Consumer启动后，它就会通过定时任务向所有Broker实例发送心跳包（包含消费分组名称、订阅关系集合、消息通信模式和客户端id等信息），Broker会缓存这些信息。
2. Consumer每隔10ms从Nameserver获取Topic与队列queue的路由信息，缓存本地
3. 每隔20s，Consumer端会请求Broekr获取该消费组下消费者Id列表，然后根据Topic下的队列queue、消费组下消费者Id进行排序，计算出待拉取的队列queue
4. 根据新算出的本地应该消费队列queue，重新计算本地队列消费任务。


特别注意，无论是消息粒度负载均衡策略还是队列粒度负载均衡策略，**在消费者上线或下线、服务端扩缩容等场景下，都会触发短暂的重新负载均衡动作。
此时可能会存在短暂的负载不一致情况，出现少量消息重复的现象。因此，需要在下游消费逻辑中做好消息幂等去重处理**。

## 怎么保证消息消费不丢失？其实思路是比较直接的，

就是 「消息确认机制」和「失败重试机制」

1. 消费者从RocketMQ拉取消息后，需要返回"CONSUME_SUCCESS"来表示业务方已经正常完成消费。只有返回"CONSUME_SUCCESS"才算作消费完成。这就是消费时的「消息确认机制」。
2. 如果返回"CONSUME_LATER"，则会按照不同的消息延迟级别进行再次消费，延迟级别从秒到小时不等，最长延迟时间为2个小时后再次尝试消费。这就是消费时的「失败重试机制」。
3. 重试消息会被存入名为 "%RETRY%+消费组名称" 的Topic中，原始主题Topic会存入属性中。然后会基于定时任务机制，在到期时将任务再次拉取出来。
4. 另外，RocketMQ跟kafka不同的是，天然支持了死信队列机制。如果在尝试消费的过程中达到了最大重试次数（通常为16次），仍然无法成功消费，则消息将被发送到死信队列，以确保消息存储的可靠性。后续业务可以根据死信队列，来做相关补偿措施。

## 怎么保证消息消费不重复？

其实思路也很直接，就是不保证不重复。

**所有消息队列的设计，都是不保证消息消费不重复的。所以使用消息队列时，要特别注意，如果有唯一性要求，必须做好消费端的幂等设计。**

## RocketMQ 5.0 解锁任意时间延迟消息能力

过去，阿里云商用的 RocketMQ 支持任意时间延迟消息, 但是开源 4.x 版本的不支持. 只有默认 18 个 level 的延迟消息,
分别是 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h。然而，最新版本5.0中，
RocketMQ引入了任意时间延迟消息的灵活性。它的实现是基于时间轮算法，并具有可调整的精度参数。

RocketMQ 4.x 版本的延迟消息是通过简单的方式实现的。如果发现消息是延迟的，则会记录原始的主题，然后将消息的主题修改为 SCHEDULE_TOPIC_XXXX，
并投递到延迟队列中。Broker具有定时器，将定期消费延迟队列中的消息，到期的消息将重新投递到原始主题中。

采用了时间轮算法. 时间轮是一种算法, 用来管理和调度固定时间间隔内的事件。它将固定时间间隔分成若干个时间槽，并将事件映射到相应的时间槽上。
在时间轮中，每个时间槽都会按顺序处理它所映射的事件，从而实现对固定时间间隔内的事件的调度.

**RocketMQ 中的时间轮数据是通过 mmap 技术持久化到本地文件中**的。这种方法可以保证时间轮数据的完整性和高效存储。
在系统重启或崩溃后，数据可以通过 mmap 快速加载到内存中，从而继续执行下一步操作。

默认时间轮的精度是 1000 毫秒，也就是一秒，时间轮的槽的数量为 TIMER_WHEEL_TTL_DAY（7）乘 DAY_SECS（24 * 3600）。
因此，使用默认精度，时间轮最多可以存储 7 天的延迟消息。

如果延迟消息超过 7 天怎么处理? 将消息放入时间轮的关键逻辑在源码 TimerMessageStore 类的 doEnqueue 方法中.

该方法主要执行以下步骤：

1. 先对消息的到期时间进行判断，如果消息到期时间与当前时间差值超过了时间轮定义的翻滚窗口（timerRollWindowSlots）的时间长度，则需要将该消息放入下一个时间轮；
2. 然后通过时间轮的 getSlot 方法，根据消息的到期时间，确定该消息应该放入的 slot；
3. 使用 ByteBuffer 对象 timerLogBuffer 记录该消息的一些信息，包括偏移量，大小等；
4. 通过 timerLog.append 方法将消息的信息写入日志；
5. 最后，通过 timerWheel.putSlot 将该消息放入时间轮的对应 slot 中。

```java
public boolean doEnqueue(long offsetPy, int sizePy, long delayedTime, MessageExt messageExt) {
LOGGER.debug("Do enqueue [{}] [{}]", new Timestamp(delayedTime), messageExt);
//copy the value first, avoid concurrent problem
long tmpWriteTimeMs = currWriteTimeMs;
boolean needRoll = delayedTime - tmpWriteTimeMs >= timerRollWindowSlots * precisionMs;
int magic = MAGIC_DEFAULT;
if (needRoll) {
magic = magic | MAGIC_ROLL;
if (delayedTime - tmpWriteTimeMs - timerRollWindowSlots * precisionMs < timerRollWindowSlots / 3 * precisionMs) {
//give enough time to next roll
delayedTime = tmpWriteTimeMs + (timerRollWindowSlots / 2) * precisionMs;
} else {
delayedTime = tmpWriteTimeMs + timerRollWindowSlots * precisionMs;
}
}
boolean isDelete = messageExt.getProperty(TIMER_DELETE_UNIQKEY) != null;
if (isDelete) {
magic = magic | MAGIC_DELETE;
}
String realTopic = messageExt.getProperty(MessageConst.PROPERTY_REAL_TOPIC);
Slot slot = timerWheel.getSlot(delayedTime);
ByteBuffer tmpBuffer = timerLogBuffer;
tmpBuffer.clear();
tmpBuffer.putInt(TimerLog.UNIT_SIZE); //size
tmpBuffer.putLong(slot.lastPos); //prev pos
tmpBuffer.putInt(magic); //magic
tmpBuffer.putLong(tmpWriteTimeMs); //currWriteTime
tmpBuffer.putInt((int) (delayedTime - tmpWriteTimeMs)); //delayTime
tmpBuffer.putLong(offsetPy); //offset
tmpBuffer.putInt(sizePy); //size
tmpBuffer.putInt(hashTopicForMetrics(realTopic)); //hashcode of real topic
tmpBuffer.putLong(0); //reserved value, just set to 0 now
long ret = timerLog.append(tmpBuffer.array(), 0, TimerLog.UNIT_SIZE);
if (-1 != ret) {
// If it's a delete message, then slot's total num -1
// TODO: check if the delete msg is in the same slot with "the msg to be deleted".
timerWheel.putSlot(delayedTime, slot.firstPos == -1 ? ret : slot.firstPos, ret,
isDelete ? slot.num - 1 : slot.num + 1, slot.magic);
addMetric(messageExt, isDelete ? -1 : 1);
}
return -1 != ret;
}
```

可以看到超过了 7 天, 就会触发翻滚窗口, 减少消息的延迟时间, 重新投递到队列中, 等下一次从队列中拿出时, 重复以上的逻辑. 可能会经过多轮的翻滚, 最终会少于 7 天.

根据应用场景对精度的要求, 还有是否接受多次翻滚时间轮导致的重复投递损耗, 来决定 precisionMs 参数的选择. 一般来说, 对于对时间精度要求较高的场景, 
可以选择更小的 precisionMs 参数, 这样可以避免重复投递的损耗, 但会消耗更多的系统资源. 如果不在意重复投递的损耗, 可以选择更大的 precisionMs 参数,
这样可以减小系统资源的消耗.

## MQ的作用

* 异步处理 - 相比于传统的串行、并行方式，提高了系统吞吐量。
* 应用解耦 - 系统间通过消息通信，不用关心其他系统的处理。
* 流量削锋 - 可以通过消息队列长度控制请求量；可以缓解短时间内的高并发请求。
* 日志处理 - 解决大量日志传输。
* 消息通讯 - 消息队列一般都内置了高效的通信机制，因此也可以用在纯的消息通讯。比如实现点对点消息队列，或者聊天室等。

## 解耦、异步、削峰是什么？

- **解耦**：A 系统发送数据到 BCD 三个系统，通过接口调用发送。如果 E 系统也要这个数据呢？那如果 C 系统现在不需要了呢？A
系统负责人几乎崩溃…A 系统跟其它各种乱七八糟的系统严重耦合，A 系统产生一条比较关键的数据，很多系统都需要 A 系统将这个数据发送过来。
如果使用 MQ，A 系统产生一条数据，发送到 MQ 里面去，哪个系统需要数据自己去 MQ 里面消费。如果新系统需要数据，
直接从 MQ 里消费即可；如果某个系统不需要这条数据了，就取消对 MQ 消息的消费即可。这样下来，A 系统压根儿不需要去考虑要给谁发送数据，
不需要维护这个代码，也不需要考虑人家是否调用成功、失败超时等情况。就是一个系统或者一个模块，调用了多个系统或者模块，
互相之间的调用很复杂，维护起来很麻烦。但是其实这个调用是不需要直接同步调用接口的，如果用 MQ 给它异步化解耦。

- **异步**：A 系统接收一个请求，需要在自己本地写库，还需要在 BCD 三个系统写库，自己本地写库要 3ms，
BCD 三个系统分别写库要 300ms、450ms、200ms。最终请求总延时是 3 + 300 + 450 + 200 = 953ms，接近 1s，
用户感觉搞个什么东西，慢死了慢死了。用户通过浏览器发起请求。如果使用 MQ，那么 A 系统连续发送 3 条消息到 MQ 队列中，
假如耗时 5ms，A 系统从接受一个请求到返回响应给用户，总时长是 3 + 5 = 8ms。

- **削峰**：减少高峰时期对服务器压力。

## 延迟队列、优先级队列

延迟队列指的是存储对应的延迟消息，消息被发送以后，并不想让消费者立刻拿到消息，而是等待特定时间后，消费者才能拿到这个消息进行消费。

RabbitMQ 本身是没有延迟队列的，要实现延迟消息，一般有两种方式：

* 通过 RabbitMQ 本身队列的特性来实现，需要使用 RabbitMQ 的死信交换机（Exchange）和消息的存活时间 TTL（Time To Live）。
* 在 RabbitMQ 3.5.7 及以上的版本提供了一个插件（rabbitmq-delayed-message-exchange）来实现延迟队列功能。同时，插件依赖 Erlang/OPT 18.0 及以上。

也就是说，AMQP 协议以及 RabbitMQ 本身没有直接支持延迟队列的功能，但是可以通过 TTL 和 DLX 模拟出延迟队列的功能。

RabbitMQ 自 V3.5.0 有优先级队列实现，优先级高的队列会先被消费。可以通过x-max-priority参数来实现优先级队列。不过，当消费速度大于生产速度且 Broker 没有堆积的情况下，优先级显得没有意义。

## 死信队列原理

DLX，全称为 Dead-Letter-Exchange，死信交换器，死信邮箱。当消息在一个队列中变成死信 (dead message) 之后，
它能被重新被发送到另一个交换器中，这个交换器就是 DLX，绑定 DLX 的队列就称之为死信队列。

导致的死信的几种原因：
* 消息被拒（Basic.Reject /Basic.Nack) 且 requeue = false。
* 消息 TTL 过期。
* 队列满了，无法再添加

## 消息队列有什么缺点

* **系统可用性降低**：本来系统运行好好的，现在你非要加入个消息队列进去，那消息队列挂了，你的系统不是呵呵了。因此，系统可用性会降低；
* **系统复杂度提高**：加入了消息队列，要多考虑很多方面的问题，比如：一致性问题、如何保证消息不被重复消费、如何保证消息可靠性传输等。因此，需要考虑的东西更多，复杂性增大。
* **一致性问题**：A 系统处理完了直接返回成功了，人都以为你这个请求就成功了；但是问题是，要是 BCD 三个系统那里，BD 两个系统写库成功了，结果 C 系统写库失败了，咋整？你这数据就不一致了。

所以消息队列实际是一种非常复杂的架构，你引入它有很多好处，但是也得针对它带来的坏处做各种额外的技术方案和架构来规避掉，做好之后，你会发现，
妈呀，系统复杂度提升了一个数量级，也许是复杂了 10 倍。但是关键时刻，用，还是得用的。

## MQ出现顺序错乱的场景

**为什么要保证顺序**: 消息队列中的若干消息如果是对同一个数据进行操作，这些操作具有前后的关系，必须要按前后的顺序执行，否则就会造成数据异常。
举例：比如通过mysql binlog进行两个数据库的数据同步，由于对数据库的数据操作是具有顺序性的，如果操作顺序搞反，就会造成不可估量的错误。
比如数据库对一条数据依次进行了 插入->更新->删除操作，这个顺序必须是这样，如果在同步过程中，消息的顺序变成了 删除->插入->更新，
那么原本应该被删除的数据，就没有被删除，造成数据的不一致问题。

**出现顺序错乱的场景**

rabbitmq ①一个queue，有多个consumer去消费，这样就会造成顺序的错误，consumer从MQ里面读取数据是有序的，但是每个consumer的执行时间是不固定的，
无法保证先读到消息的consumer一定先完成操作，这样就会出现消息并没有按照顺序执行，造成数据顺序错误。

![img.png](../.vuepress/public/interview/others/mq-order01.png)

一个queue对应一个consumer，但是consumer里面进行了多线程消费，这样也会造成消息消费顺序错误。

![img.png](../.vuepress/public/interview/others/mq-order02.png)

kafka ①kafka一个topic，一个partition，一个consumer，但是consumer内部进行多线程消费，这样数据也会出现顺序错乱问题。

![img.png](../.vuepress/public/interview/others/mq-order03.png)

②具有顺序的数据写入到了不同的partition里面，不同的消费者去消费，但是每个consumer的执行时间是不固定的，无法保证先读到消息的consumer一定先完成操作，这样就会出现消息并没有按照顺序执行，造成数据顺序错误。

![img.png](../.vuepress/public/interview/others/mq-order04.png)

## RabbitMQ基本概念

* Broker： 简单来说就是消息队列服务器实体
* Exchange： 消息交换机，它指定消息按什么规则，路由到哪个队列
* Queue： 消息队列载体，每个消息都会被投入到一个或多个队列
* Binding： 绑定，它的作用就是把exchange和queue按照路由规则绑定起来
* Routing Key： 路由关键字，exchange根据这个关键字进行消息投递
* VHost： vhost 可以理解为虚拟 broker ，即 mini-RabbitMQ server。其内部均含有独立的 queue、exchange 和 binding 等，但最最重要的是，其拥有独立的权限系统，可以做到 vhost 范围的用户控制。当然，从 RabbitMQ 的全局角度，vhost 可以作为不同权限隔离的手段（一个典型的例子就是不同的应用可以跑在不同的 vhost 中）。
* Producer： 消息生产者，就是投递消息的程序
* Consumer： 消息消费者，就是接受消息的程序
* Channel： 消息通道，在客户端的每个连接里，可建立多个channel，每个channel代表一个会话任务

由**Exchange、Queue、RoutingKey**三个才能决定一个从Exchange到Queue的唯一的线路。

## RabbitMQ的工作模式

**simple模式（即最简单的收发模式）**

<img :src="$withBase('/interview/mq04.png')" alt="mq04">

消息产生消息，将消息放入队列

* 消息的消费者(consumer) 监听 消息队列,如果队列中有消息,就消费掉,消息被拿走后,自动从队列中删除(隐患 消息可能没有被消费者正确处理,已经从队列中消失了,
* 造成消息的丢失，这里可以设置成手动的ack,但如果设置成手动ack，处理完后要及时发送ack消息给队列，否则会造成内存溢出)。

**work工作模式(资源的竞争)**

<img :src="$withBase('/interview/mq05.png')" alt="mq05">

* 消息产生者将消息放入队列消费者可以有多个,消费者1,消费者2同时监听同一个队列,消息被消费。C1 C2共同争抢当前的消息队列内容,
  谁先拿到谁负责消费消息(隐患：高并发情况下,默认会产生某一个消息被多个消费者共同使用,可以设置一个开关(syncronize) 保证一条消息只能被一个消费者使用)。

**publish/subscribe发布订阅(共享资源)**

<img :src="$withBase('/interview/mq06.png')" alt="mq06">

* 每个消费者监听自己的队列；
* 生产者将消息发给broker，由交换机将消息转发到绑定此交换机的每个队列，每个绑定交换机的队列都将接收到消息。

**routing路由模式**

<img :src="$withBase('/interview/mq08.png')" alt="mq08">

* 消息生产者将消息发送给交换机按照路由判断,路由是字符串(info) 当前产生的消息携带路由字符(对象的方法),交换机根据路由的key,只能匹配上路由key对应的消息队列,对应的消费者才能消费消息;
* 根据业务功能定义路由字符串
* 从系统的代码逻辑中获取对应的功能字符串,将消息任务扔到对应的队列中。
* 业务场景:error 通知;EXCEPTION;错误通知的功能;传统意义的错误通知;客户通知;利用key路由,可以将程序中的错误封装成消息传入到消息队列中,开发者可以自定义消费者,实时接收错误;

**topic 主题模式(路由模式的一种)**

<img :src="$withBase('/interview/mq07.png')" alt="mq07">

* 星号井号代表通配符
* 星号代表多个单词,井号代表一个单词
* 路由功能添加模糊匹配
* 消息产生者产生消息,把消息交给交换机
* 交换机根据key的规则模糊匹配到对应的队列,由队列的监听消费者接收消息消费

（在我的理解看来就是routing查询的一种模糊匹配，就类似sql的模糊查询方式）

## 消息怎么路由？

消息提供方->路由->一至多个队列消息发布到交换器时，消息将拥有一个路由键（routing key），在消息创建时设定。通过队列路由键，
可以把队列绑定到交换器上。消息到达交换器后，RabbitMQ 会将消息的路由键与队列的路由键进行匹配（针对不同的交换器有不同的路由规则）；

常用的交换器主要分为一下三种：
* fanout：如果交换器收到消息，将会广播到所有绑定的队列上
* direct：如果路由键完全匹配，消息就被投递到相应的队列
* topic：可以使来自不同源头的消息能够到达同一个队列。 使用 topic 交换器时，可以使用通配符

## rabbitmq消息基于什么传输

**由于 TCP 连接的创建和销毁开销较大，且并发数受系统资源限制，会造成性能瓶颈。RabbitMQ 使用信道的方式来传输数据。信道是建立在真实的 TCP 连接内的虚拟连接，且每条 TCP 连接上的信道数量没有限制。**

## RabbitMQ消息的可靠传输？

消息到 MQ 的过程中搞丢，MQ 自己搞丢，MQ 到消费过程中搞丢。
* 生产者到 RabbitMQ：事务机制和 Confirm 机制，注意：事务机制和 Confirm 机制是互斥的，两者不能共存，会导致 RabbitMQ 报错。
* RabbitMQ 自身：持久化、集群、普通模式、镜像模式。
* RabbitMQ 到消费者：basicAck 机制、死信队列、消息补偿机制。

**发送方确认模式**
* 将信道设置成 confirm 模式（发送方确认模式），则所有在信道上发布的消息都会被指派一个唯一的 ID。
* 一旦消息被投递到目的队列后，或者消息被写入磁盘后（可持久化的消息），信道会发送一个确认给生产者（包含消息唯一 ID）。
* 如果 RabbitMQ 发生内部错误从而导致消息丢失，会发送一条 nack（notacknowledged，未确认）消息。
* 发送方确认模式是异步的，生产者应用程序在等待确认的同时，可以继续发送消息。当确认消息到达生产者应用程序，生产者应用程序的回调方法就会被触发来处理确认消息。

**接收方确认机制**
* 消费者接收每一条消息后都必须进行确认（消息接收和消息确认是两个不同操作）。只有消费者确认了消息，RabbitMQ 才能安全地把消息从队列中删除。
* 这里并没有用到超时机制，RabbitMQ 仅通过 Consumer 的连接中断来确认是否需要重新发送消息。也就是说，只要连接不中断，RabbitMQ 给了 Consumer 足够长的时间来处理消息。保证数据的最终一致性；

**下面罗列几种特殊情况**

如果消费者接收到消息，在确认之前断开了连接或取消订阅，RabbitMQ 会认为消息没有被分发，然后重新分发给下一个订阅的消费者。（**可能存在消息重复消费的隐患，需要去重**）
如果消费者接收到消息却没有确认消息，连接也未断开，则 RabbitMQ 认为该消费者繁忙，将不会给该消费者分发更多的消息。

**生产者丢失消息:confirm模式**

从生产者弄丢数据这个角度来看，RabbitMQ提供transaction和confirm模式来确保生产者不丢消息；

* transaction机制就是说：发送消息前，开启事务（channel.txSelect()）,然后发送消息，如果发送过程中出现什么异常，事务就会回滚（channel.txRollback()）,如果发送成功则提交事务（channel.txCommit()）。然而，这种方式有个缺点：吞吐量下降；
* confirm模式用的居多：一旦channel进入confirm模式，所有在该信道上发布的消息都将会被指派一个唯一的ID（从1开始），一旦消息被投递到所有匹配的队列之后；
* rabbitMQ就会发送一个ACK给生产者（包含消息的唯一ID），这就使得生产者知道消息已经正确到达目的队列了；
* 如果rabbitMQ没能处理该消息，则会发送一个Nack消息给你，你可以进行重试操作。

**消息队列丢数据：消息持久化**

处理消息队列丢数据的情况，一般是开启持久化磁盘的配置。
这个持久化配置可以和confirm机制配合使用，你可以在消息持久化磁盘后，再给生产者发送一个Ack信号。这样，
如果消息持久化磁盘之前，rabbitMQ阵亡了，那么生产者收不到Ack信号，生产者会自动重发。

那么如何持久化呢？这里顺便说一下吧，其实也很容易，就下面两步
1. 将queue的持久化标识durable设置为true,则代表是一个持久的队列
2. 发送消息的时候将deliveryMode=2
这样设置以后，即使rabbitMQ挂了，重启后也能恢复数据。

**消费者丢失消息:手动确认消息**：

* 消费者丢数据一般是因为采用了自动确认消息模式，改为手动确认消息即可！
* 消费者在收到消息之后，处理消息之前，会自动回复RabbitMQ已收到消息；
* 如果这时处理消息失败，就会丢失该消息；

**解决方案：处理消息成功后，手动回复确认消息。**

## rabbitmq持久化(交换器持久化、队列持久化和消息的持久化)

消息的可靠性是RabbitMQ的一大特色，那么RabbitMQ是如何保证消息可靠性的呢？答案就是消息持久化。
持久化可以防止在异常情况下丢失数据。RabbitMQ的持久化分为三个部分：**交换器持久化、队列持久化和消息的持久化**。

交换器持久化可以通过在声明队列时将durable参数设置为true。如果交换器不设置持久化，那么在RabbitMQ服务重启之后，
相关的交换器元数据会丢失，不过消息不会丢失，只是不能将消息发送到这个交换器了。

队列的持久化能保证其本身的元数据不会因异常情况而丢失，但是不能保证内部所存储的消息不会丢失。要确保消息不会丢失，
需要将其设置为持久化。队列的持久化可以通过在声明队列时将durable参数设置为true。

设置了队列和消息的持久化，当RabbitMQ服务重启之后，消息依然存在。如果只设置队列持久化或者消息持久化，重启之后消息都会消失。

当然，也可以将所有的消息都设置为持久化，但是这样做会影响RabbitMQ的性能，因为磁盘的写入速度比内存的写入要慢得多。

对于可靠性不是那么高的消息可以不采用持久化处理以提高整体的吞吐量。鱼和熊掌不可兼得，关键在于选择和取舍。
在实际中，需要根据实际情况在可靠性和吞吐量之间做一个权衡。

## MQ的message都使用持久化机制？

1. 首先，必然导致性能的下降，因为写磁盘比写 RAM 慢的多，message 的吞吐量可能有 10 倍的差距。
2. 其次，message 的持久化机制用在 RabbitMQ 的内置 cluster 方案时会出现“坑爹”问题。矛盾点在于，
若 message 设置了 persistent 属性，但 queue 未设置 durable 属性，那么当该 queue 的 owner node 出现异常后，
在未重建该 queue 前，发往该 queue 的 message 将被 blackholed ；若 message 设置了 persistent 属性，
同时 queue 也设置了 durable 属性，那么当 queue 的 owner node 异常且无法重启的情况下，则该 queue 无法在其他 node 上重建，
只能等待其 owner node 重启后，才能恢复该 queue 的使用，而在这段时间内发送给该 queue 的 message 将被 blackholed 。
3. 所以，是否要对 message 进行持久化，需要综合考虑性能需要，以及可能遇到的问题。若想达到 100,000 条/秒以上的消息吞吐量（单 RabbitMQ 服务器），
则要么使用其他的方式来确保 message 的可靠 delivery ，要么使用非常快速的存储系统以支持全持久化（例如使用 SSD）。另外一种处理原则是：仅对关键消息作持久化处理（根据业务重要程度），且应该保证关键消息的量不会导致性能瓶颈。

## RabbitMQ高可用的

RabbitMQ 是比较有代表性的，因为是基于主从（非分布式）做高可用性的，我们就以 RabbitMQ 为例子讲解第一种 MQ 的高可用性怎么实现。
RabbitMQ 有三种模式：**单机模式、普通集群模式、镜像集群模式**。

- **单机模式**: Demo/test 级别的，一般就是你本地启动了玩玩儿的?，没人生产用单机模式。

- **普通集群模式**：意思就是在多台机器上启动多个 RabbitMQ 实例，每个机器启动一个。你创建的 queue，只会放在一个 RabbitMQ 实例上，
但是每个实例都同步 queue 的元数据（元数据可以认为是 queue 的一些配置信息，通过元数据，可以找到 queue 所在实例）。
你消费的时候，实际上如果连接到了另外一个实例，那么那个实例会从 queue 所在实例上拉取数据过来。这方案主要是提高吞吐量的，
就是说让集群中多个节点来服务某个 queue 的读写操作。

- **镜像集群模式**: 这种模式，才是所谓的 RabbitMQ 的高可用模式。跟普通集群模式不一样的是，在镜像集群模式下，
你创建的 queue，无论元数据还是 queue 里的消息都会存在于多个实例上，就是说，每个 RabbitMQ 节点都有这个 queue 的一个完整镜像，
包含 queue 的全部数据的意思。然后每次你写消息到 queue 的时候，都会自动把消息同步到多个实例的 queue 上。
RabbitMQ 有很好的管理控制台，就是在后台新增一个策略，这个策略是镜像集群模式的策略，指定的时候是可以要求数据同步到所有节点的，
也可以要求同步到指定数量的节点，再次创建 queue 的时候，应用这个策略，就会自动将数据同步到其他的节点上去了。

这样的好处在于，你任何一个机器宕机了，没事儿，其它机器（节点）还包含了这个 queue 的完整数据，别的 consumer 都可以到其它节点上去消费数据。
坏处在于，第一，这个性能开销也太大了吧，消息需要同步到所有机器上，导致网络带宽压力和消耗很重！RabbitMQ 一个 queue 的数据都是放在一个节点里的，
镜像集群下，也是每个节点都放这个 queue 的完整数据。

## RabbitMQ如何实现高可用?

在分布式架构下，高可用是最基础的设计。也就是说，一旦依赖的某个服务出现故障，不能影响业务的正常执行。RabbitMQ提供了两种集群模式：**普通集群模式、镜像集群模式**

**普通集群**

![img.png](../.vuepress/public/interview/others/rabbitmq-cluster01.png)

这种集群模式下，**各个节点只同步元数据，不同步队列中的消息**。其中元数据包含队列的名称、交换机名称及属性、交换机与队列的绑定关系等。
当我们发送消息和消费消息的时候，不管请求发送到RabbitMQ集群的哪个节点。最终都会通过元数据定位到队列所在的节点去存储以及拉取数据。
很显然，**这种集群方式并不能保证Queue的高可用，因为一旦Queue所在的节点挂了，那么这个Queue的消息就没办法访问了。它的好处是通过多个节点分担了流量的压力，提升了消息的吞吐能力**。

**镜像集群**

它和普通集群的区别在于，镜像集群中Queue的数据会在RabbitMQ集群的每个节点存储一份。一旦任意一个节点发生故障，其他节点仍然可以继续提供服务。所以这种集群模式实现了真正意义上的高可用。

![img.png](../.vuepress/public/interview/others/rabbitmq-cluster02.png)

最后，在镜像集群的模式下，我们可以通过Keepalived+HAProxy来实现RabbitMQ集群的负载均衡。其中：

HAProxy是一个能支持四层和七层的负载均衡器，可以实现对RabbitMQ集群的负载均衡
同时为了避免HAProxy的单点故障，可以再增加Keepalived实现HAProxy的主备，如果HAProxy主节点出现故障那么备份节点就会接管主节点提供服务。

![img.png](../.vuepress/public/interview/others/rabbitmq-cluster03.png)

**RabbitMQ高可用实现方式有两种**

* 第一种是普通集群模式，在这种模式下，一个Queue的消息只会存在集群的一个节点上，集群里面的其他节点会同步Queue所在节点的元数据，
  消息在生产和消费的时候，不管请求发送到集群的哪个节点，最终都会路由到Queue所在节点上去存储和拉取消息。
  这种方式并不能保证Queue的高可用性，但是它可以提升RabbitMQ的消息吞吐能力

* 第二种是镜像集群，也就是集群里面的每个节点都会存储Queue的数据副本。意味着每次生产消息的时候，都需要把消息内容同步给集群中的其他节点。
  这种方式能够保证Queue的高可用性，但是集群副本之间的同步会带来性能的损耗。另外，由于每个节点都保存了副本，所以我们还可以通过HAProxy实现负载均衡。

## RabbitMQ的消息如何实现路由？

RabbitMQ是一个基于AMQP协议实现的分布式消息中间件。AMQP的具体工作机制是，生产者把消息发送到RabbitMQ Broker上的Exchange交换机上。
Exchange交换机把收到的消息根据路由规则发给绑定的队列（Queue）。最后再把消息投递给订阅了这个队列的消费者，从而完成消息的异步通讯。

![img.png](../.vuepress/public/interview/others/rabbimq001.png)

其中，Exchange是一个消息交换机，它里面定义了消息路由的规则，也就是这个消息路由到那个队列。然后Queue表示消息的载体，每个消息可以根据路由规则路由到一个或者多个队列里面。

而关于消息的路由机制，核心的组件是Exchange。它负责接收生产者的消息然后把消息路由到消息队列，而消息的路由规则由ExchangeType和Binding决定。

Binding表示建立Queue和Exchange之间的绑定关系，每一个绑定关系会存在一个BindingKey。通过这种方式相当于在Exchange中建立了一个路由关系表。

![img.png](../.vuepress/public/interview/others/rabbimq002.png)

生产者发送消息的时候，需要声明一个routingKey（路由键），Exchange拿到routingKey之后，根据RoutingKey和路由表里面的BindingKey进行匹配，而匹配的规则是通过ExchangeType来决定的。

![img.png](../.vuepress/public/interview/others/rabbimq003.png)

在RabbitMQ中，有三种类型的`Exchange：direct ，fanout和topic`。
* `direct`： 完整匹配方式，也就是Routing key和Binding Key完全一致，相当于点对点的发送。
* `fanout`： 广播机制，这种方式不会基于Routing key来匹配，而是把消息广播给绑定到当前Exchange上的所有队列上。
* `topic`： 正则表达式匹配，根据Routing Key使用正则表达式进行匹配，符合匹配规则的Queue都会收到这个消息

## Kafka的特点与使用场景

Kafka的特点

* 高吞吐量、低延迟：kafka每秒可以处理几十万条消息，它的延迟最低只有几毫秒，每个主题可以分多个分区, 消费组对分区进行消费操作；
* 可扩展性：kafka集群支持热扩展；
* 持久性、可靠性：消息被持久化到本地磁盘，并且支持数据备份防止数据丢失；
* 容错性：允许集群中节点失败（若副本数量为n,则允许n-1个节点失败）；
* 高并发：支持数千个客户端同时读写

使用场景

* 日志收集：一个公司可以用Kafka可以收集各种服务的log，通过kafka以统一接口服务的方式开放给各种consumer，例如Hadoop、Hbase、Solr等；
* 消息系统：解耦和生产者和消费者、缓存消息等；
* 用户活动跟踪：Kafka经常被用来记录web用户或者app用户的各种活动，如浏览网页、搜索、点击等活动，这些活动信息被各个服务器发布到kafka的topic中，然后订阅者通过订阅这些topic来做实时的监控分析，或者装载到Hadoop、数据仓库中做离线分析和挖掘；
* 运营指标：Kafka也经常用来记录运营监控数据。包括收集各种分布式应用的数据，生产各种操作的集中反馈，比如报警和报告；
* 流式处理：比如spark streaming和storm；

## kafa的核心概念

<img :src="$withBase('/interview/kafka.png')" alt="kafka">

**Producer**： 生产者即数据的发布者，该角色将消息发布到Kafka的topic中。broker接收到生产者发送的消息后，broker将该消息追加到当前用于追加数据的segment文件中。
生产者发送的消息，存储到一个partition中，生产者也可以指定数据存储的partition。

**Consumer**： 消费者可以从broker中读取数据。消费者可以消费多个topic中的数据。

**Topic**： 在Kafka中，使用一个类别属性来划分数据的所属类，划分数据的这个类称为topic。如果把Kafka看做为一个数据库，topic可以理解为数据库中的一张表，topic的名字即为表名。

**Partition**：topic中的数据分割为一个或多个partition。每个topic至少有一个partition。每个partition中的数据使用多个segment文件存储。
partition中的数据是有序的，partition间的数据丢失了数据的顺序。如果topic有多个partition，消费数据时就不能保证数据的顺序。在需要严格保证消息的消费顺序的场景下，需要将partition数目设为1。

**Partition offset**

每条消息都有一个当前Partition下唯一的64字节的offset，它指明了这条消息的起始位置。

**Replicas of partition**

副本是一个分区的备份。副本不会被消费者消费，副本只用于防止数据丢失，即消费者不从为follower的partition中消费数据，
而是从为leader的partition中读取数据。副本之间是一主多从的关系。

**Broker**

Kafka 集群包含一个或多个服务器，服务器节点称为broker。broker存储topic的数据。如果某topic有N个partition，
集群有N个broker，那么每个broker存储该topic的一个partition。如果某topic有N个partition，集群有(N+M)个broker，
那么其中有N个broker存储该topic的一个partition，剩下的M个broker不存储该topic的partition数据。
如果某topic有N个partition，集群中broker数目少于N个，那么 一个broker存储该topic的一个或多个partition。
在实际生产环境中，尽量避免这种情况的发生，这种情况容易导致Kafka集群数据不均衡。

**Leader**

每个partition有多个副本，其中有且仅有一个作为Leader，Leader是当前负责数据的读写的partition

**Follower**

Follower跟随Leader，所有写请求都通过Leader路由，数据变更会广播给所有Follower，Follower与Leader保持数据同步。
如果Leader失效，则从Follower中选举出一个新的Leader。当Follower与Leader挂掉、卡住或者同步太慢，
leader会把这个follower从“in sync replicas”（ISR）列表中删除，重新创建一个Follower。

**Zookeeper**

Zookeeper负责维护和协调broker。当Kafka系统中新增了broker或者某个broker发生故障失效时，由ZooKeeper通知生产者和消费者。
生产者和消费者依据Zookeeper的broker状态信息与broker协调数据的发布和订阅任务。

**AR(Assigned Replicas)**

分区中所有的副本统称为AR。

**ISR(In-Sync Replicas)**

所有与Leader部分保持一定程度的副（包括Leader副本在内）本组成ISR。

**OSR(Out-of-Sync-Replicas)**

与Leader副本同步滞后过多的副本。

**HW(High Watermark)**

高水位，标识了一个特定的offset，消费者只能拉取到这个offset之前的消息。

**LEO(Log End Offset)**

即日志末端位移(log end offset)，记录了该副本底层日志(log)中下一条消息的位移值。注意是下一条消息！也就是说，
如果LEO=10，那么表示该副本保存了10条消息，位移值范围是[0,9]。

## Kafka如何保证消息消费的顺序性？

**kafka为什么会存在无序消费**

首先，在kafka的架构里面，用到了Partition分区机制来实现消息的物理存储，在同一个topic下面，可以维护多个partition来实现消息的分片。

![img.png](../.vuepress/public/interview/others/kafka01.png)

生产者在发送消息的时候，会根据消息的key进行取模，来决定把当前消息存储到哪个partition里面。并且消息是按照先后顺序有序存储到partition里面的。

![img.png](../.vuepress/public/interview/others/kafka02.png)

在这种情况下，假设有一个topic存在三个partition，而消息正好被路由到三个独立的partition里面。
然后消费端有三个消费者通过balance机制分别指派了对应消费分区。因为消费者是完全独立的网络节点，
所有可能会出现，消息的消费顺序不是按照发送顺序来实现的，从而导致乱序的问题。

![img.png](../.vuepress/public/interview/others/kafka03.png)

**kafka如何保证有序消费**

针对这个问题，一般的解决办法就是自定义消息分区路由的算法，然后把指定的key都发送到同一个Partition里面。
接着指定一个消费者专门来消费某个分区的数据，这样就能保证消息的顺序消费了。

![img.png](../.vuepress/public/interview/others/kafka04.png)

另外，有些设计方案里面，在消费端会采用异步线程的方式来消费数据来提高消息的处理效率，那这种情况下，
因为每个线程的消息处理效率是不同的，所以即便是采用单个分区的存储和消费也可能会出现无序问题，
针对这个问题的解决办法就是在消费者这边使用一个阻塞队列，把获取到的消息先保存到阻塞队列里面，然后异步线程从阻塞队列里面去获取消息来消费。

## 什么是 ISR，为什么需要引入ISR

首先，发送到Kafka Broker上的消息，最终是以Partition的物理形态来存储到磁盘上的。
而Kafka为了保证Parititon的可靠性，提供了Paritition的副本机制，然后在这些Partition副本集里面。存在Leader Partition和Flollower Partition。

生产者发送过来的消息，会先存到Leader Partition里面，然后再把消息复制到Follower Partition，
这样设计的好处就是一旦Leader Partition所在的节点挂了，可以重新从剩余的Partition副本里面选举出新的Leader。
然后消费者可以继续从新的Leader Partition里面获取未消费的数据。

![img.png](../.vuepress/public/interview/others/ISR.png)

在Partition多副本设计的方案里面，有两个很关键的需求。
* 副本数据的同步
* 新Leader的选举

这两个需求都需要涉及到网络通信，Kafka为了避免网络通信延迟带来的性能问题，以及尽可能的保证新选举出来的Leader Partition里面的数据是最新的，所以设计了ISR这样一个方案。
ISR全称是 in-sync replica，它是一个集合列表，里面保存的是和Leader Parition节点数据最接近的Follower Partition,如果某个Follower Partition里面的数据落后Leader太多，就会被剔除ISR列表。
简单来说，ISR列表里面的节点，同步的数据一定是最新的，所以后续的Leader选举，只需要从ISR列表里面筛选就行了。

所以，我认为引入ISR这个方案的原因有两个
1. 尽可能的保证数据同步的效率，因为同步效率不高的节点都会被踢出ISR列表。
2. 避免数据的丢失，因为ISR里面的节点数据是和Leader副本最接近的。

## Kafka怎么避免重复消费

首先Kafka Broker上存储的消息，都有一个Offset标记。然后kafka的消费者是通过offSet标记来维护当前已经消费的数据，
每消费一批数据，Kafka Broker就会更新OffSet的值，避免重复消费。

![img.png](../.vuepress/public/interview/others/Kafka001.png)

默认情况下，**消息消费完以后，会自动提交Offset的值，避免重复消费**。Kafka消费端的自动提交逻辑有一个默认的5秒间隔，也就是说在5秒之后的下一次向Broker拉取消息的时候提交。
所以在**Consumer消费的过程中，应用程序被强制kill掉或者宕机，可能会导致Offset没提交，从而产生重复提交的问题**。

除此之外，还有另外一种情况也会出现重复消费。在Kafka里面有一个Partition Balance机制，就是把多个Partition均衡的分配给多个消费者。
Consumer端会从分配的Partition里面去消费消息，**如果Consumer在默认的5分钟内没办法处理完这一批消息**。**就会触发Kafka的Rebalance机制，从而导致Offset自动提交失败。**
**而在重新Rebalance之后，Consumer还是会从之前没提交的Offset位置开始消费，也会导致消息重复消费的问题**。

![img.png](../.vuepress/public/interview/others/Kafka002.png)

基于这样的背景下，我认为解决重复消费消息问题的方法有几个。
* **提高消费端的处理性能避免触发Balance，比如可以用异步的方式来处理消息，缩短单个消息消费的市场。或者还可以调整消息处理的超时时间。还可以减少一次性从Broker上拉取数据的条数**。
* **可以针对消息生成md5然后保存到mysql或者redis里面，在处理消息之前先去mysql或者redis里面判断是否已经消费过。这个方案其实就是利用幂等性的思想。**

## Kafka的零拷贝原理?

在实际应用中，如果我们需要把磁盘中的某个文件内容发送到远程服务器上，如图

![img.png](../.vuepress/public/interview/others/0copy.png)

那么它必须要经过几个拷贝的过程：

1. 从磁盘中读取目标文件内容拷贝到内核缓冲区
2. CPU控制器再把内核缓冲区的数据赋值到用户空间的缓冲区中
3. 接着在应用程序中，调用write()方法，把用户空间缓冲区中的数据拷贝到内核下的Socket Buffer中。
4. 最后，把在内核模式下的SocketBuffer中的数据赋值到网卡缓冲区（NIC Buffer)
5. 网卡缓冲区再把数据传输到目标服务器上。

在这个过程中我们可以发现，数据从磁盘到最终发送出去，要经历4次拷贝，而在这四次拷贝过程中，有两次拷贝是浪费的，分别是：
1. 从内核空间赋值到用户空间
2. 从用户空间再次复制到内核空间

除此之外，由于用户空间和内核空间的切换会带来CPU的上线文切换，对于CPU性能也会造成性能影响。

而零拷贝，就是把这两次多于的拷贝省略掉，应用程序可以直接把磁盘中的数据从内核中直接传输给Socket，而不需要再经过应用程序所在的用户空间，如下图所示。

![img.png](../.vuepress/public/interview/others/0copy02.png)

零拷贝通过DMA（Direct Memory Access）技术把文件内容复制到内核空间中的Read Buffer，接着把包含数据位置和长度信息的文件描述符加载到Socket Buffer中，DMA引擎直接可以把数据从内核空间中传递给网卡设备。
在这个流程中，数据只经历了两次拷贝就发送到了网卡中，并且减少了2次cpu的上下文切换，对于效率有非常大的提高。
所以，所谓零拷贝，并不是完全没有数据赋值，只是相对于用户空间来说，不再需要进行数据拷贝。对于前面说的整个流程来说，零拷贝只是减少了不必要的拷贝次数而已。

在程序中如何实现零拷贝呢？
1. 在Linux中，零拷贝技术依赖于底层的sendfile()方法实现
2. 在Java中，FileChannal.transferTo() 方法的底层实现就是 sendfile() 方法。
3. 除此之外，还有一个 mmap 的文件映射机制,它的原理是：将磁盘文件映射到内存, 用户通过修改内存就能修改磁盘文件。使用这种方式可以获取很大的I/O提升，省去了用户空间到内核空间复制的开销。

## MQ的技术选型

**Kafka**

Kafka 是 LinkedIn 开源的一个分布式流式处理平台，已经成为 Apache 顶级项目，早期被用来用于处理海量的日志，
后面才慢慢发展成了一款功能全面的高性能消息队列。Kafka 是一个分布式系统，由通过高性能 TCP 网络协议进行通信的服务器和客户端组成，
可以部署在在本地和云环境中的裸机硬件、虚拟机和容器上。

* 消息队列：发布和订阅消息流，这个功能类似于消息队列，这也是 Kafka 也被归类为消息队列的原因。
* 容错的持久方式存储记录消息流：Kafka 会把消息持久化到磁盘，有效避免了消息丢失的风险。
* 流式处理平台：在消息发布的时候进行处理，Kafka 提供了一个完整的流式处理类库。

**RocketMQ**

RocketMQ 是阿里开源的一款云原生“消息、事件、流”实时数据处理平台，借鉴了 Kafka，已经成为 Apache 顶级项目。
RocketMQ 的核心特性（摘自 RocketMQ 官网）：

* 云原生：生与云，长与云，无限弹性扩缩，K8s 友好
* 高吞吐：万亿级吞吐保证，同时满足微服务与大数据场景。
* 流处理：提供轻量、高扩展、高性能和丰富功能的流计算引擎。
* 金融级：金融级的稳定性，广泛用于交易核心链路。
* 架构极简：零外部依赖，Shared-nothing 架构。
* 生态友好：无缝对接微服务、实时计算、数据湖等周边生态。

**RabbitMQ**

RabbitMQ 是采用 Erlang 语言实现 AMQP(Advanced Message Queuing Protocol，高级消息队列协议）的消息中间件，
它最初起源于金融系统，用于在分布式系统中存储转发消息。RabbitMQ 发展到今天，被越来越多的人认可，
这和它在易用性、扩展性、可靠性和高可用性等方面的卓著表现是分不开的。RabbitMQ 的具体特点可以概括为以下几点：

- 可靠性： RabbitMQ 使用一些机制来保证消息的可靠性，如持久化、传输确认及发布确认等。
- 灵活的路由： 在消息进入队列之前，通过交换器来路由消息。对于典型的路由功能，RabbitMQ 己经提供了一些内置的交换器来实现。
  针对更复杂的路由功能，可以将多个交换器绑定在一起，也可以通过插件机制来实现自己的交换器。这个后面会在我们讲 RabbitMQ 核心概念的时候详细介绍到。
- 扩展性： 多个 RabbitMQ 节点可以组成一个集群，也可以根据实际业务情况动态地扩展集群中节点。
- 高可用性： 队列可以在集群中的机器上设置镜像，使得在部分节点出现问题的情况下队列仍然可用。
- 支持多种协议： RabbitMQ 除了原生支持 AMQP 协议，还支持 STOMP、MQTT 等多种消息中间件协议。
- 多语言客户端： RabbitMQ 几乎支持所有常用语言，比如 Java、Python、Ruby、PHP、C#、JavaScript 等。
- 易用的管理界面： RabbitMQ 提供了一个易用的用户界面，使得用户可以监控和管理消息、集群中的节点等。在安装 RabbitMQ 的时候会介绍到，安装好 RabbitMQ 就自带管理界面。
- 插件机制： RabbitMQ 提供了许多插件，以实现从多方面进行扩展，当然也可以编写自己的插件。感觉这个有点类似 Dubbo 的 SPI 机制

**Pulsar**

Pulsar 是下一代云原生分布式消息流平台，最初由 Yahoo 开发 ，已经成为 Apache 顶级项目。Pulsar 集消息、存储、轻量化函数式计算为一体，
采用计算与存储分离架构设计，支持多租户、持久化存储、多机房跨区域数据复制，具有强一致性、高吞吐、低延时及高可扩展性等流数据存储特性，
被看作是云原生时代实时消息流传输、存储和计算最佳解决方案。Pulsar 的关键特性如下（摘自官网）：

- 下一代云原生分布式消息流平台。
- Pulsar 的单个实例原生支持多个集群，可跨机房在集群间无缝地完成消息复制。
- 极低的发布延迟和端到端延迟。
- 可无缝扩展到超过一百万个 topic。简单的客户端 API，支持 Java、Go、Python 和 C++。
- 主题的多种订阅模式（独占、共享和故障转移）。
- 通过 Apache BookKeeper 提供的持久化消息存储机制保证消息传递 。
- 由轻量级的 serverless 计算框架 Pulsar Functions 实现流原生的数据处理。
- 基于 Pulsar Functions 的 serverless connector 框架 Pulsar IO 使得数据更易移入、移出 Apache Pulsar。
- 分层式存储可在数据陈旧时，将数据从热存储卸载到冷/长期存储（如 S3、GCS）中。

| 对比方向        |    概要         |
|:--------------:|:---------------------------------------------:|
| 吞吐量     | 万级的 ActiveMQ 和 RabbitMQ 的吞吐量（ActiveMQ 的性能最差）要比十万级甚至是百万级的 RocketMQ 和 Kafka 低一个数量级。 |
| 可用性     | 都可以实现高可用。ActiveMQ 和 RabbitMQ 都是基于主从架构实现高可用性。RocketMQ 基于分布式架构。 Kafka 也是分布式的，一个数据多个副本，少数机器宕机，不会丢失数据，不会导致不可用  |
| 时效性 |   RabbitMQ 基于 Erlang 开发，所以并发能力很强，性能极其好，延时很低，达到微秒级，其他几个都是 ms 级。  |
| 功能支持 |  Pulsar 的功能更全面，支持多租户、多种消费模式和持久性模式等功能，是下一代云原生分布式消息流平台。  |
| 消息丢失 |  ActiveMQ 和 RabbitMQ 丢失的可能性非常低， Kafka、RocketMQ 和 Pulsar 理论上可以做到 0 丢失。 |

## 如何设计MQ软件

比如说这个消息队列系统，我们从以下几个角度来考虑一下：

1. 首先这个 mq 得支持可伸缩性吧，就是需要的时候快速扩容，就可以增加吞吐量和容量，那怎么搞？设计个分布式的系统呗，
   参照一下 kafka 的设计理念，broker -> topic -> partition，每个 partition 放一个机器，就存一部分数据。
   如果现在资源不够了，简单啊，给 topic 增加 partition，然后做数据迁移，增加机器，不就可以存放更多数据，提供更高的吞吐量了？
2. 其次你得考虑一下这个 mq 的数据要不要落地磁盘吧？那肯定要了，落磁盘才能保证别进程挂了数据就丢了。
   那落磁盘的时候怎么落啊？顺序写，这样就没有磁盘随机读写的寻址开销，磁盘顺序读写的性能是很高的，这就是 kafka 的思路。
3. 其次你考虑一下你的mq 的可用性啊？这个事儿，具体参考之前可用性那个环节讲解的 kafka 的高可用保障机制。
   多副本 -> leader & follower -> broker 挂了重新选举 leader 即可对外服务。
4. 能不能支持数据0丢失啊？可以呀，有点复杂的。

## 百万消息堆积,如何快速解决?

**解决消息堆积的原因**
1. 新上线的消费者功能有BUG，消息无法被消费
2. 消费者实例宕机或因网络问题暂时无法同Broker建立连接。
3. 生产者短时间内推送大量消息至Broker，消费者消费能力不足

**解决消息堆积的方案**

**事前处理**

在我们系统上线之前应该我们对大致的流量应该有一个预估，并提前进行压测.

**事中处理：**

1. 临时扩容: **同时增加消费者实例数量、增加消息队列数量、**
2. 增加临时消费者：**如果我们消息队列内存不允许的情况下，我们可以增加一个临时消费者实例(只做插入操作，将消息保存到数据库中，保证消息不丢失)**
3. 业务降级，最低限度让系统还能正常运转，服务重要业务和核心业务：a．减少发送方发送的数据量 b．减轻消费者业务逻辑,提高消费速度。

**事后处理**

1. 优化我们消费者代码，提高消费者消费速度
2. 并行处理，将消息分配到多个处理线程中同时处理，从而提供系统的处理速度和性能.

**为了防止我们消息堆积问题的产生**

1. 我们要做好上线前的对流量的预估和提前的压测
2. 对于新增活动或者其他会导致我们短时间流量飙升的，需要提前增加我们的消费者实例数量和消息队列数量
3. 通过服务降级来保证重要业务和核心业务。
4. 提高我们的消费者能力（策略:丢弃(消费者有API获取的)，加强消费速度或者增加并行);

## MQ消息持续积压

1. 消息积压处理办法：临时紧急扩容：

2. 先修复 consumer 的问题，确保其恢复消费速度，然后将现有 cnosumer 都停掉。新建一个 topic，partition 是原来的 10 倍，临时建立好原先 10 倍的 queue 数量。
然后写一个临时的分发数据的 consumer 程序，这个程序部署上去消费积压的数据，消费之后不做耗时的处理，直接均匀轮询写入临时建立好的 10 倍数量的 queue。
接着临时征用 10 倍的机器来部署 consumer，每一批 consumer 消费一个临时 queue 的数据。这种做法相当于是临时将 queue 资源和 consumer 资源扩大 10 倍，以正常的 10 倍速度来消费数据。
等快速消费完积压数据之后，得恢复原先部署的架构，重新用原先的 consumer 机器来消费消息。
3. MQ中消息失效：假设你用的是 RabbitMQ，RabbtiMQ 是可以设置过期时间的，也就是 TTL。如果消息在 queue 中积压超过一定的时间就会被 RabbitMQ 给清理掉，
这个数据就没了。那这就是第二个坑了。这就不是说数据会大量积压在 mq 里，而是**大量的数据会直接搞丢**。
我们可以采取一个方案，就是批量重导，这个我们之前线上也有类似的场景干过。就是大量积压的时候，我们当时就直接丢弃数据了，
然后等过了高峰期以后，比如大家一起喝咖啡熬夜到晚上12点以后，用户都睡觉了。这个时候我们就开始写程序，将丢失的那批数据，
写个临时程序，一点一点的查出来，然后重新灌入 mq 里面去，把白天丢的数据给他补回来。也只能是这样了。
假设 1 万个订单积压在 mq 里面 ，没有处理，其中 1000 个订单都丢了，你只能手动写程序把那 1000 个订单给查出来，手动发到 mq 里去再补一次。
4. mq消息队列块满了：如果消息积压在 mq 里，你很长时间都没有处理掉，此时导致 mq 都快写满了，咋办？这个还有别的办法吗？没有，
谁让你第一个方案执行的太慢了，你临时写程序，接入数据来消费，消费一个丢弃一个，都不要了，快速消费掉所有的消息。然后走第二个方案，到了晚上再补数据吧。

## MQ消息的重复问题（幂等性问题）

1. 造成消息重复的根本原因是：网络不可达。
2. 所以解决这个问题的办法就是绕过这个问题。那么问题就变成了：如果消费端收到两条一样的消息，应该怎样处理？
3. 消费端处理消息的业务逻辑保持幂等性。只要保持幂等性，不管来多少条重复消息，最后处理的结果都一样。
4. 保证每条消息都有唯一编号且保证消息处理成功与去重表的日志同时出现。利用一张日志表来记录已经处理成功的消息的 ID，如果新到的消息 ID 已经在日志表中，那么就不再处理这条消息。

## 保证消息的消费顺序

rabbitmq ①拆分多个queue，每个queue一个consumer，就是多一些queue而已，确实是麻烦点；这样也会造成吞吐量下降，可以在消费者内部采用多线程的方式取消费。

![img.png](../.vuepress/public/interview/others/mq-order05.png)

②或者就一个queue但是对应一个consumer，然后这个consumer内部用内存队列做排队，然后分发给底层不同的worker来处理

![img.png](../.vuepress/public/interview/others/mq-order06.png)

kafka ①确保同一个消息发送到同一个partition，一个topic，一个partition，一个consumer，内部单线程消费。

![img.png](../.vuepress/public/interview/others/mq-order07.png)

写N个内存queue，然后N个线程分别消费一个内存queue即可

![img.png](../.vuepress/public/interview/others/mq-order08.png)

## 定时消息的设计方案

* https://zhuanlan.zhihu.com/p/649047840

1. 代理实现
如果是定时消息，将消息发送到代理服务（这个是一个独立的服务，需要自己开发，定时地把消息发送出去），当然了消息用什么来保存呢？
可以是数据库，redis等等，不过我推荐采用文件，因为消息可能会比较多，需要保存一定的时间，可以考虑RocksDB，这个效率非常的高效。

2. 时间轮和delay-file实现

![img.png](../.vuepress/public/interview/mq/时间轮和delay-file实现.png)

上图是通过RocketMQ源码分析一个实现原理方案示意图。

分为两个部分：
   1. 消息的写入
   2. 消息的Schedule
* 在写入CommitLog之前，如果是延迟消息，按照每10分钟写入delayfile文件，对于快到时间执行的，直接写入时间轮，并且写入delayfile，时间轮每秒钟执行，如果时间到了，就执行队列中的任务，写入commitlog文件中，commitlog会自动写入 comsumqueue中，然后客户端就能消费到了。
* 为什么delayfile文件保存10分钟的数据呢，考虑到时间轮不能太多任务在里面，保存10分钟内的数据，也不会占用太多的内存

3. 时间轮和秒级文件实现

这种方案比较简单实现，通过秒级时间，建立对应的文件夹，只要相同的时间超时的消息，就在同一个目录，通过msgid保证文件不重复，
等到了时间后，就扫描对应的文件夹的文件，发送到队列中，写入commitlog即可。当然了，我们可以先缓冲一定时间的文件夹文件，这样效率高一些。

4. 基于rocketmq 默认支持18个等级来改造

rocketmq开源版本支持18个等级的延迟方案，如果要支持任意时间延时，可以基于rocketmq已有的level来改造，传入一个时间，计算出最接近的等级，投递出去，当消息到达时候，
判断是否到了时间，如果没有，继续计算出最接近的等级投递，一直到超时时间为止。**这个方案有个缺陷，在没有到达超时时间，同一个消息会出现多次投递的情况。**

![img.png](../.vuepress/public/interview/mq/延迟消息设计3.png)

## RocketMQ和Kafka区别？

1. **适用场景**： Kafka适合日志处理、RocketMQ适合业务处理。结论：平手，根据具体业务定夺。
2. **性能**：kafka单机写入TPS号称在百万条/秒；RocketMQ大约在10万条/秒；结论：追求性能的话，kafka单机性能更高。
3. **可靠性**：**RocketMQ支持异步/同步刷盘；异步/同步 Replication；Kafka使用异步刷盘方式，异步Replication；结论：RocketMQ所支持的同步方式提升了数据的可靠性。**
4. **实时性**：**均支持pull长轮询，RocketMQ消息实时性更好，RocketMQ胜出。**
5. **支持的队列数**：Kafka单机超过64个队列/分区，消息发送性能降低严重；**RocketMQ单机支持最高5万个队列，性能稳定；**
6. **消息顺序性**：**Kafka某些配置下，支持消息顺序，但是一台Broker宕机后，就会产生消息乱序；RocketMQ支持严格的消息顺序，在顺序消费场景下，一台Broker宕机后，发送消息会失败，但是不会乱序。**
7. **消费失败重试机制**： Kafka消费失败不支持重试，**RocketMQ消费失败支持定时重试，每次重试间隔时间顺延**
8. **定时/延时消息**：Kafka不支持定时消息；**RocketMQ支持定时消息**
9. **分布式事务消息**： Kafka不支持分布式事务消息，**RocketMQ支持分布式事务消息**
10. **消息查询机制**：Kafka不支持消息查询，**RocketMQ支持根据Message Id查询消息，也支持根据消息内容查询消息**
11. **消息回溯**：Kafka理论上可以按照Offset来回溯消息，**RocketMQ支持按照时间来回溯消息，精度毫秒，例如从一天之前的某分某秒开始重新消费消息**







## 博文参考

* https://javap.blog.csdn.net/article/details/120027043