---
lang: zh-CN
sidebarDepth: 2
---

# 设计模式知识

## 适配器模式

适配器模式是一种结构型设计模式。适配器模式的思想是：把一个类的接口变换成客户端所期待的另一种接口，从而使原本因接口不匹配而无法在一起工作的两个类能够在一起工作。
用电器来打个比喻：有一个电器的插头是三脚的，而现有的插座是两孔的，要使插头插上插座，我们需要一个插头转换器，这个转换器即是适配器。

适配器模式涉及3个角色：
1. 源（Adaptee）：需要被适配的对象或类型，相当于插头。
2. 适配器（Adapter）：连接目标和源的中间对象，相当于插头转换器。
3. 目标（Target）：期待得到的目标，相当于插座。

适配器模式包括3种形式：类适配器模式、对象适配器模式、接口适配器模式（或又称作缺省适配器模式）。

**优点**
1. 更好的复用性：系统需要使用现有的类，而此类的接口不符合系统的需要。那么通过适配器模式就可以让这些功能得到更好的复用。
2. 更好的扩展性：在实现适配器功能的时候，可以扩展自己源的行为（增加方法），从而自然地扩展系统的功能。

**缺点**

1. 会导致系统紊乱：滥用适配器，会让系统变得非常零乱。例如，明明看到调用的是A接口，其实内部被适配成了B接口的实现，一个系统如果太多出现这种情况，无异于一场灾难。因此如果不是很有必要，可以不使用适配器，而是直接对系统进行重构。

```java
public class AadpterPattern {
    public static void mian(String[] args) {
        new Aadpter(new Speaker()).translate();
    }
}

class Speaker {
    public String speak() {
        return "china";
    }
}

interface Translator {
    public String translate();
}

// 适配器类
class Aadpter implements Translator {
    private Speaker speaker;

    public Aadpter(Speaker speaker) {
        this.speaker = speaker;
    }
    
    public String translate() {
        String result=speaker.speak();
        // 理解 翻译 手语
        return result;
    }
}
```


## 策略模式

定义一组算法，将每个算法都封装起来，并且使它们之间可以互换。策略模式让算法独立于使用它的客户而变化，也称为政策模式(Policy)

![img.png](../.vuepress/public/blog/Policy.png)

```java
public class AadpterPattern {
    public static void mian(String[] args) {
        ThreadPoolExecuteor executeor=new ThreadPoolExecuteor("线程的拒绝策略的4中，需要指定的拒绝的策略");
    }
}
```








## 博文参考
- [设计模式视频讲解](https://www.bilibili.com/video/BV1vT4y1E7g6/?spm_id_from=pageDriver&vd_source=1032b663a8d7733cb9dbbbf8a2188d94)