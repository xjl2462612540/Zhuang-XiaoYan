# SpringCloudAlibaba微服务

Spring Cloud Alibaba致力于提供微服务开发的一站式解决方案，它是Spring Cloud组件被植入Alibaba元素之后的产物。
利用Spring Cloud Alibaba，可以快速搭建微服务架构并完成技术升级。中小企业如果需要快速落地业务中台和技术中台，并向数字化业务转型，
那Spring Cloud Alibaba绝对是一个“神器”。Spring Cloud Alibaba是Spring Cloud的子项目，好吧，这两项目一开始的定位就是父子关系。
看来官方还是没打算颠覆Spring Cloud的架构思想，只是想做一次能力的增强和扩展。

## 一、Spring Cloud Alibaba组件

<img :src="$withBase('/interview/Spring-Cloud-01.png')" alt="Spring-Cloud-01">

我们先看看Spring Cloud Alibaba有哪些能力？Spring Cloud Alibaba 的组件功能既有免费版本，也有收费版本。
* Sentinel：以“流”为切入点，在流量控制、并发性、容错降级和负载保护等方面提供解决方案，以保护服务的稳定性。
* Nacos：一个具备动态服务发现和分布式配置等功能的管理平台，主要用于构建云原生应用程序。
* RocketMQ：一个高性能、高可用、高吞吐量的金融级消息中间件。Spring Cloud Alibaba 将RocketMQ 定制化封装，开发人员可“开箱即用”。
* Dubbo：一个基于Java的高性能开源RPC框架。
* Seata：一个高性能且易于使用的分布式事务解决方案，可用于微服务架构。
* 阿里云OSS（阿里云对象存储服务）：一种加密的安全云存储服务，可以存储、处理和访问来自世界任何地方的大量数据。
* 阿里云SchedulerX：一款分布式任务调度产品，支持定期任务和在指定时间点触发任务。
* 阿里云SMS：一种覆盖全球的消息服务，提供便捷、高效和智能的通信功能，可帮助企业快速联系其客户

我们再来看看，没有Spring Cloud Alibaba之前，我们如何使用Spring Cloud进行微服务开发。

* Spring Cloud支持多种注册中心，比如Eureka、ZooKeeper、Consul等。
如果软件开发人员需要采用Eureka作为注册中心，则需要搭建一个Eureka Server集群，用于管理注册中心服务的元数据，然后应用服务需要接入Eureka，
就需要使用对应的注解将服务提供者以及服务订阅者注册到Eureka注册中心。同理ZooKeeper和Consul也是采用同样的方式，使用对应注册中心的注解完成服务的注册和订阅。

* Spring Cloud为了方便软件开发人员快速的接入不同的注册中心，统一使用注解@EnableDiscoveryClient+对应注册中心的Starter组件。
当然Eureka还是沿用老的使用方式@EnableEurekaClient+对应注册中心的Starter组件，主要是由于Spring Cloud已经停止了对Eureka的维护。
 
* 好吧问题来了，Spring Cloud已经将ZooKeeper和Consul的使用方式统一起来，软件开发人员非常愉快的将应用接入Spring Cloud，
但是目前市面上又出了一个新的注册中心，比如Nacos，它的性能非常高，并且支持CP和AP模式，但是Spring Cloud不支持。

* 我们再来看看在没有Spring Cloud Alibaba之前，我们如何使用Nacos。Nacos是一款既支持分布式注册中心和分布式配置中心的神器。
Nacos官方提供了很多接入模式，比如Spring Framework、Spring Boot等，但是其底层本质上是依赖Nacos提供的SDK,比如Nacos Client。

* 如果采用SpringFramework+NacosClient（比如nacos-spring-context），则需要开发人员自己维护NacosNamingServce和
NacosConfigService实例对象，也就是说开发人员需要自己依赖Nacos Client做二次开发，成本非常大。
 
* 如果采用Spring Boot+Nacos Starter组件（比如nacos-discovery-spring-boot-starter），
则开发人员可以高效的接入Nacos配置中心，并且可以使用Spring Boot提供的各种Starter组件。

## 二、Spring Cloud Alibaba的核心架构思想

<img :src="$withBase('/interview/Spring-Cloud-02.png')" alt="Spring-Cloud-02">

<img :src="$withBase('/interview/Spring-Cloud-03.png')" alt="Spring-Cloud-03">