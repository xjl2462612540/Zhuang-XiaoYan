---
lang: zh-CN
sidebarDepth: 2
---

# 非技术面试自我介绍

面试官，你好，我叫庄小焱，21年毕业于上海大学，研究生专业是机械自动化(计算机视觉方向)、本科专业是软件工程。<br>

2021年-2023年工作与DELL-EMC云计算部门担任软件开发工程师，

其中在两年中主参与了DELL-EMC云计算产品中InCloud云计算平台与Vxrail私有云操作系统的开发，

在InCloud云计算平台主要负责计算产品的微服务开发，主要包括产品信息的数据查询与展示，产品的构建，产品的上架和下架功能以及云计算产品配置与升级服务。

在Vxrail私有云操作系统主要负责硬件抽象层的微服务开发，负责IDRAC服务技术选型与服务拆分，开发虚拟IDRAC服务，利用Zookeeper通知实现IDRAC容器服务自动更新功能。

在技术栈上个人比较擅擅长JAVA技术栈，同时自己工作之余通过PMP项目管理认证，自己也是一名技术博客网站博主，六年有github,CSDN技术博文的编写经历。

积极的参与社区的相关工作。个人在性能上积极阳光，工作上比较细致和严格，有较强的抗压能能力。

以上就是我的个人基本介绍。谢谢！