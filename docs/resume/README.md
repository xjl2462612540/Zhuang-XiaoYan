# 庄小焱个人简历

<hr>

## 基本信息

* 姓&emsp;&emsp;名：庄小焱 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp; 年&ensp;龄：27
* 意向岗位：后端软件开发工程师&emsp;&emsp;&emsp;&emsp;&emsp;手&ensp;机：18279148786
* 地&emsp;&emsp;址：上海市宝山区 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&ensp;&ensp;  邮 箱：18279148786@163.com

<hr>

## 工作经历

* 某云计算公司 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2021.08~2023.08 &emsp;&emsp;&emsp;&emsp; 云计算部门&emsp;&emsp;&emsp;&emsp;&emsp;后端软件开发工程师

<hr>

## 教育经历

* 上海大学&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2018.09~2021.08&emsp;&emsp;&emsp;&emsp; 计算机视觉专业&emsp;&emsp;&emsp;&emsp;&emsp; 硕士研究生
* 江西理工大学&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2014.09~2018.09&emsp;&emsp;&emsp;&emsp; 软件工程专业&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 大学本科

<hr>

## 专业技能

* **熟悉掌握常用的链表、堆、栈、二叉树、Hash表等数据结构**.
* **擅长JAVA语言,阅读过`JDK`中`HashMap`、`ConcurrentHashMap`等集合类源码**.
* **精通JVM内存模型、熟悉`GC`回收算法、类的加载原理、十大垃圾回收器原理、拥有JVM调优实战经验**.
* **精通MYSQL底层数据结构、存储引擎、数据库事务、数据库锁、索引、`MVCC`工作原理等原理，拥有MYSQL慢查询优化经验.**
* **精通JUC并发编程，熟练的使用`Voilate`、`Synchronized`、`ReentrantLock`、`AQS`、`Automic原子类`、`CyclicBarrier`等并发工具类.**
* **精通Redis底层数据结构、Redis线程模型、网络模型,熟悉Redis的持久化与键值过期策略、Redis缓存失效和解决方案,熟悉Redis数据迁移原理、Redis的主从、哨兵机制、集群原理.**
* **熟悉23种软件设计模式,并在项目中熟练使用单例、观察者、代理、工厂等设计模式.**
* **熟悉Spring的源码,`BeanFactory`，`Bean生命周期`流程与原理，能根据业务自定义扩展Spring.**
* **熟练掌握`Spring`、`Mybatis`、`Springcloud`、`Dubbo`、`Zookeeper`主流开发框架.**
* **熟悉掌握分布式事务,`XA`,`TCC`,`Sega`,`Seata`、主导项目基于`RocketMQ`事务消息选型以及落地.**
* **熟悉分布式原理,`Redis`、`Zookeeper`分布式锁原理，并在项目使用`Redis分布式锁`解决并发问题.**
* **熟悉RocketMQ原理**
* **熟悉Docker原理、在项目使用Docker构建相关微服务、并使用Docker构建测试微服务.**

<hr>

## 项目经历

1. 某云计算公司 &emsp;&emsp;&emsp;&emsp; 2022.09-2023.10 &emsp;&emsp;&emsp; Cloud-Platform系统 &emsp;&emsp;&emsp;&emsp; 后端开发工程师
    * 项目技术: spring-cloud、spring-boot、mybatis、mysql、nginx、redis、MQ、docker、virtualization等
    * 个人主要工作：
      * 解决线上JVM参数设置不当导致GC时间过长，解决因扩大缓存内存导致大量的GC问题，优化业务缓存数据过期时间。
      * 利用 Vmware 虚拟化技术构建VM,Vxaril,Raven 等虚拟产品，提高物理硬件利用率，节约3倍硬件资源。
      * 使用 Springboot 构建资源申请服务、集群生产服务，实现虚拟资源统一管理与访问，各种集群类型的产品上架。
      * 利用 Redis 分布式锁解决资源超分配问题，实现系统 QPS 1 万+。
      * 利用 MQ 实现订单分布式事务，解决库存脏数据问题，实现资源精确管理。
    * [Cloud-Platform系统项目设计文档](https://zhuang-xiaoyan.github.io/zhuangxiaoyan/project/cloud-platform.html)

2. 个人开源项目 &emsp;&emsp;&emsp; 2021.09-2023.06 &emsp;&emsp;&emsp; Athena-Mall系统 &emsp;&emsp;&emsp;&emsp;&emsp; 后端开发工程师
   * 技术栈: spring-cloud、mysql、nacos、nginx、redis、micro-service、docker、kubesphere
   * 主要工作: 
   * 项目设计文档
     * [vue-mall-cloud设计文档](https://zhuang-xiaoyan.github.io/zhuangxiaoyan/project/athena-mall-cloud.html)
     * [vue-mall-front设计文档](https://zhuang-xiaoyan.github.io/zhuangxiaoyan/project/athena-mall-front.html)
     * [vue-mall-admin设计文档](https://zhuang-xiaoyan.github.io/zhuangxiaoyan/project/athena-admin-front.html)
   * 项目仓库
     * [vue-mall-cloud仓库](https://github.com/Zhuang-XiaoYan/Athena-Mall-Cloud)
     * [vue-mall-front仓库](https://github.com/Zhuang-XiaoYan/Athena-Mall-Front)
     * [vue-mall-admin仓库](https://github.com/Zhuang-XiaoYan/Athena-Mall-Admin)

<hr>

## 技能证书

<img :src="$withBase('/resume/PMP.png')" alt="test-arch01" width="600" height="400">

<img :src="$withBase('/resume/CSND.png')" alt="test-arch01" width="600" height="400">

<img :src="$withBase('/resume/aliyun.png')" alt="test-arch01" width="600" height="400">

<hr>

## 个人账号

* [庄小焱个人博客](https://zhuang-xiaoyan.github.io/zhuangxiaoyan/)
* [庄小焱GitHub](https://github.com/Zhuang-XiaoYan)
* [庄小焱Gitee](https://gitee.com/xjl2462612540)
* [庄小焱CSDN](https://blog.csdn.net/weixin_41605937)

<hr>

## 关于作者

大家好，我是程序员庄小焱，目前就职于某云计算研发中心，PMP项目管理专家、CSDN博文专家. 本人在系统架构、
容器化技术、大数据、机器学习领域不断学习,同时我将在博客中持续不断分享自己的学习知识和相关技术解决方案,
欢迎大家和我交流学习,欢迎大家关注我的博客。

备注：「个人简单介绍」+ 交流，围观朋友圈，做点赞之交（备注没有自我介绍不通过哦）

<img :src="$withBase('/resume/weixin.png')" alt="weixin" width="600" height="600">

<hr>
